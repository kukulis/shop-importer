update tmp_products t
join ps_product p on t.nomnr=p.supplier_reference
set t.id_product=p.id_product;

update tmp_products_unique t
  join ps_product p on t.nomnr=p.supplier_reference
set t.id_product=p.id_product;