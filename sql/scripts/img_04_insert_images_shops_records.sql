insert ignore into ps_image_shop (
id_product, id_image, id_shop, cover)
select id_product, id_image, 1, cover
from ps_image;

-- pagrindinio paveikslėlio nustatymas
update ps_image_shop pis
    join
    ( select max(id_image) as max_id_image, max(cover) as max_cover, id_product, id_shop from ps_image_shop
      group by id_product, id_shop ) cover_max
    on pis.id_image = cover_max.max_id_image and pis.id_shop = cover_max.id_shop
set pis.cover = 1
where max_cover is null;
