
update tmp_tag t join ps_lang l on t.iso_code COLLATE utf8mb4_general_ci  = l.iso_code
set t.id_lang=l.id_lang;
# update tmp_tag t join ps_lang l on t.iso_code = l.iso_code
# set t.id_lang=l.id_lang;
-- select * from ps_lang;

-- insert p_tag

insert into ps_tag (id_lang, name)
select  t.id_lang, t.tag_name from tmp_tag t
left join ps_tag pt on t.id_lang=pt.id_lang and t.tag_name=pt.name
where pt.id_tag is null;

-- update tag_ids
update tmp_tag t
    join ps_tag pt on t.id_lang=pt.id_lang and t.tag_name=pt.name
set t.id_tag = pt.id_tag;

-- update id_langs on tmp_product_tag
update tmp_product_tag t join ps_lang l on t.iso_code COLLATE utf8mb4_general_ci = l.iso_code
set t.id_lang=l.id_lang;

-- update tag_ids on tmp_product_tag
update  tmp_product_tag pt
    join tmp_tag tt on pt.tag_name = tt.tag_name and pt.iso_code=tt.iso_code
    set pt.id_tag = tt.id_tag;

# select * from tmp_product_tag pt
#     join tmp_tag tt on pt.tag_name = tt.tag_name and pt.iso_code=tt.iso_code

-- update products ids
update tmp_product_tag t
    join ps_product pp on t.nomnr = pp.supplier_reference
set t.id_product = pp.id_product;

-- inserting products tags
INSERT IGNORE INTO ps_product_tag (id_product, id_tag, id_lang)
SELECT id_product, id_tag, id_lang from tmp_product_tag;
