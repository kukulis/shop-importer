update tmp_attributes ta
    join ps_attribute_group_lang pagl
        on ta.group_name  COLLATE utf8mb4_general_ci = pagl.name and ta.id_lang = pagl.id_lang
set ta.id_attribute_group = pagl.id_attribute_group;

# select * from tmp_attributes