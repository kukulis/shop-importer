update ps_product_lang pl
  join tmp_products t on pl.id_product=t.id_product
                     and pl.id_shop = t.id_shop
                     and pl.id_lang = t.id_lang
  set
  pl.description        = t.description,
  pl.description_short  = t.description_short,
  pl.link_rewrite       = t.link_rewrite,
  pl.meta_description   = t.meta_description,
  pl.meta_keywords      = t.meta_keywords,
  pl.meta_title         = t.meta_title,
  pl.name               = t.name,
  pl.available_now      = t.available_now,
  pl.available_later    = t.available_later,
  pl.delivery_in_stock  = t.delivery_in_stock,
  pl.delivery_out_stock = t.delivery_out_stock;

