-- dabar reikia į tmp_feature sudėti id_fature_value vertes
-- čia sudedam tik vienai kalbai
update tmp_feature tf
    join tmp_feature_value tfv
    on tf.id_feature = tfv.id_feature
        and tf.iso_code='lt'
        and tf.feature_value = tfv.value
set tf.id_feature_value = tfv.id_feature_value;

-- Ką daryti su kitomis kalbomis?
-- Kaip surišti kad ta pati vertė yra ta pati abiejose kalbose?
-- Vienintelis būdas - rišti per produkto id, priimant prielaidą, kad jeigu
-- skirtingose kalbose kažkokios savybės vertės priskirtos vienam produktui,
-- vadinasi yra ta pati savybė tik su skirtingais vertimais tose skirtingose kalbose.
update tmp_feature tf1
join tmp_feature tf2
on tf1.id_product = tf2.id_product and tf1.id_feature=tf2.id_feature
set tf1.id_feature_value = tf2.id_feature_value
where tf2.iso_code = 'lt' and tf1.iso_code != 'lt';

-- select * from tmp_feature where id_feature_value is null
# select  * from tmp_feature tf1
# join tmp_feature tf2
# on tf1.id_product = tf2.id_product and tf1.id_feature=tf2.id_feature
#     where tf2.iso_code = 'lt' and tf1.iso_code != 'lt'
# and tf1.id_feature_value is null;