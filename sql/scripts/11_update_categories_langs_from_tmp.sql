update ps_category_lang cl
join tmp_categories tc
on cl.id_category = tc.id_category
and cl.id_shop = tc.id_shop
and cl.id_lang = tc.id_lang
set
 -- cl.name              = tc.name,        -- šito nedarom, nes pavadinimus redaguosis admine
 -- cl.description       = tc.description, -- šito nedarom, nes aprašymus redaguosis admine
--  cl.link_rewrite      = tc.link_rewrite, -- linkus irgi redaguos web adminas, reiktu tik tuščius užpildyti
  cl.meta_title        = tc.meta_title,
  cl.meta_keywords     = tc.meta_keywords,
  cl.meta_description  = tc.meta_description;