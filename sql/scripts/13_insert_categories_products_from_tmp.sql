insert ignore into ps_category_product
select t.id_category,
       t.id_product,
       ifnull(t.position,0) position
from tmp_categories_products t
  where id_category is not null and id_product is not null;

-- delete from ps_category_product;