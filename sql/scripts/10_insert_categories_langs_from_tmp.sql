insert ignore into ps_category_lang (
    id_category,
    id_shop,
    id_lang,
    name,
    description,
    link_rewrite,
    meta_title,
    meta_keywords,
    meta_description )
select
  id_category,
  id_shop,
  id_lang,
  name,
  description,
  link_rewrite,
  meta_title,
  meta_keywords,
  meta_description from tmp_categories where id_category is not null;
