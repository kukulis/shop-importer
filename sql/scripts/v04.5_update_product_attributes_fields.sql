update ps_product_attribute pa
join tmp_product_attribute tpa on pa.id_product_attribute = tpa.id_product_attribute
set pa.weight = tpa.weight,
    pa.available_date = tpa.available_date,
    pa.location = tpa.location,
    pa.ecotax = tpa.ecotax,
    pa.isbn = tpa.isbn,
    pa.low_stock_alert = tpa.low_stock_alert,
    pa.low_stock_threshold = tpa.low_stock_threshold,
    pa.supplier_reference = tpa.supplier_reference;
