# lentelė analogiška ps_attributes, tik pridėtas laukas attribute_code, kurio nėra ps_attribute lentelėje
# analogiškai padaryta ir su features, žr.: *_insert_new_feature_values.sql

create table if not exists auto_attributes (
    id_attribute int primary key auto_increment,
    id_attribute_group int,
    color varchar(32),
    position int,
    attribute_code varchar(32)
);


# pirma suterpiam egzistuojančius įrašus iš ps_attribute,
# kad pasislinktų id_attribute, ir kad būtų žinoma kokios reikšmės yra.

#nurezetinam id auto increment
truncate auto_attributes;

#darom įterpimus esančių duomenų
insert into auto_attributes
    (id_attribute, id_attribute_group, color, position, attribute_code)
select a.id_attribute, a.id_attribute_group, a.color, a.position,
       substring(pal.name, 0, 32)
from ps_attribute a
join ps_attribute_lang pal
    on a.id_attribute = pal.id_attribute
join ps_lang pl on pal.id_lang = pl.id_lang and pl.iso_code='en'; -- čia kalba ta pati kaip ir a00_insert_tmp_attributes.sql pagrindinė kalba

-- select * from auto_attributes;

insert into auto_attributes
(id_attribute_group, color, position, attribute_code)
select ta.id_attribute_group, ta.attribute_code,
       1, -- pozicija 1 kol kas nesugalvoju kaip skaičiuoti iš eilės
       ta.attribute_code
from tmp_attributes ta  left join auto_attributes aa
    on aa.attribute_code = ta.attribute_code
    and aa.id_attribute_group = ta.id_attribute_group
    join ps_lang pl on ta.id_lang = pl.id_lang
    where pl.iso_code = 'en' and aa.id_attribute is null;
-- čia kalba ta pati kaip ir a00_insert_tmp_attributes.sql pagrindinė kalba
# select * from tmp_attributes
#
# select * from auto_attributes
#
# select * from ps_attribute