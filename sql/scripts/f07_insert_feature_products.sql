insert ignore into ps_feature_product (id_feature, id_product, id_feature_value)
select tf.id_feature, tf.id_product, tf.id_feature_value from tmp_feature tf
where tf.id_feature is not null
and tf.id_product is not null
and tf.id_feature_value is not null;
