insert ignore into ps_image
(id_product,
  position,
  sketis_image_id,
  cover
 )
select
  t.id_product,
  t.position,
  t.sketis_image_id,
  t.cover
from tmp_images t left join ps_image i
  on t.sketis_image_id=i.sketis_image_id
where i.id_image is null and t.id_product is not null;

-- alia klaidos pranešimai
-- turėtų eiti į logą
-- šitas niekad neturėtų atsitikti, gal reiks ištrinti
select 'Klaida: ' as err, i.sketis_image_id as existing_sketis_image_id, i.id_image as existing_id_image, ti.id_image comming_id_image from ps_image i
join tmp_images ti on i.sketis_image_id = ti.sketis_image_id
where i.id_image != ti.id_image;


/*
select * from ps_image i
 join tmp_images t on i.id_image = t.id_image
    where t.sketis_image_id = i.sketis_image_id
and i.id_product != t.id_product
*/

-- perskiriam paveikslėlius naujoms prekėms.
update ps_image i
    join tmp_images t on i.id_image = t.id_image
set i.id_product = t.id_product, i.cover=t.cover
where t.sketis_image_id = i.sketis_image_id
  and i.id_product != t.id_product and t.sketis_image_id is not null;


