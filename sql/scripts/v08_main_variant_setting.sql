update ps_product_attribute pa join
    (
        select max(id_product_attribute) max_id_product_attribute,
               max(default_on) max_default_on,
               id_product
        from ps_product_attribute
        group  by id_product ) default_on_max on pa.id_product_attribute = default_on_max.max_id_product_attribute
set pa.default_on=1
where max_default_on is null;


update ps_product_attribute_shop pas
join ( select max(id_product_attribute) max_id_product_attribute,
       max(default_on) max_default_on,
       id_product,
       id_shop
from ps_product_attribute_shop
group  by id_product, id_shop ) default_on_max_shop
    on pas.id_product_attribute = default_on_max_shop.max_id_product_attribute
    and pas.id_shop=default_on_max_shop.id_shop
    set pas.default_on=1
where max_default_on is null;

