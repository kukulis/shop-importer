
-- drop table tmp_product_attribute;

create table if not exists tmp_product_attribute (
    id_product_attribute int,
    parent_reference varchar(32),
	id_product int unsigned not null,
	reference varchar(64) not null primary key,
	supplier_reference varchar(64) null,
	location varchar(64) null,
	ean13 varchar(13) null,
	isbn varchar(32) null,
	upc varchar(12) null,
	wholesale_price decimal(20,6) default 0.000000 not null,
	price decimal(20,6) default 0.000000 not null,
	ecotax decimal(17,6) default 0.000000 not null,
	quantity int(10) default 0 not null,
	weight decimal(20,6) default 0.000000 not null,
	unit_price_impact decimal(20,6) default 0.000000 not null,
	default_on tinyint(1) unsigned null,
	minimal_quantity int unsigned default 1 not null,
	low_stock_threshold int(10) null,
	low_stock_alert tinyint(1) default 0 not null,
	available_date date null
);

truncate tmp_product_attribute;
--
-- create index id_product_id_product_attribute
-- 	on ps_product_attribute (id_product_attribute, id_product);
--
-- create index product_attribute_product
-- 	on ps_product_attribute (id_product);
--
-- create index reference
-- 	on ps_product_attribute (reference);
--
-- create index supplier_reference
-- 	on ps_product_attribute (supplier_reference);

insert into tmp_product_attribute
    (
     parent_reference,
     id_product,
     reference,
     supplier_reference,
     location,
     ean13,
     isbn,
     upc,
     wholesale_price,
     price,
     ecotax,
     quantity,
     weight,
     unit_price_impact,
     default_on,
     minimal_quantity,
     low_stock_threshold,
     low_stock_alert,
     available_date)
select
       parent,
0, -- id_product
nomnr,
'',
'',
'',
'',
'', -- upc
0 , -- wholesale_price
0 , -- price
0 , -- ecotax
0 , -- quantity
weight,
0, -- unit_price_impact
null, -- default_on
1 , -- minimal_quantity
null , -- low_stock_threshold
0 , -- low_stock_alert
null -- available_date
from tmp_products_unique where parent is not null and parent != ''
on duplicate key update weight=values(weight);

# select * from tmp_product_attribute;

# select * from tmp_products_unique