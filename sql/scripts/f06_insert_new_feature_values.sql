# id_feature_value raktinis laukas generuojamas lentelėje tmp_feature_value per 'auto_increment' opciją.
# ankstesniame f*.sql skripte
insert ignore into ps_feature_value (id_feature_value, id_feature, custom )
select tfv.id_feature_value, tfv.id_feature, tfv.custom
  from tmp_feature_value tfv;

-- čia reikia imti iš tmp_feature,
-- nes tmp_feature_value atitinka ps_feature_value,
-- o tmp_feature_value_lang atitinka tmp_feature
insert ignore into ps_feature_value_lang(id_feature_value, id_lang, value)
 select tf.id_feature_value, tf.id_language, tf.feature_value from tmp_feature tf
where tf.id_feature_value is not null;
