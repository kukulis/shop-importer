update tmp_images t
  join ps_image i on t.sketis_image_id = i.sketis_image_id
  set t.id_image = i.id_image;

# select * from tmp_images where id_image is null;
#
# select count(1) from tmp_images;
# -- 11511
#
# select count(1) from ps_image
#
# select * from tmp_images
#
# select * from ps_image where sketis_image_id='1319080-26713715-4'
#
#
# select * from ps_image where sketis_image_id='1319080-26713716-4'
#
# select * from tmp_images where sketis_image_id in (
#   '1319080-26713715-4',
#   '1319080-26713716-4'
#   )