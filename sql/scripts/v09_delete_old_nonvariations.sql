-- trinam prekes, kurios prieš tai buvo ne variantai, o dabar pasidarė variantai

create or replace  table tmp_old_products (
    id_product int
);

-- ne variantai yra ps_product lentelėje, kur reference yra tas pats
-- kaip importuojamų variantų reference.
-- Importuojami variantai yra tmp_product_attribute lentelėje.
insert into tmp_old_products ( id_product )
select p.id_product
from ps_product p
    join tmp_product_attribute tpa on p.reference = tpa.reference;

-- select * from tmp_old_products;

delete from ps_product where id_product in (select id_product from tmp_old_products);
delete from ps_product_lang where id_product in (select id_product from tmp_old_products);
delete from ps_product_shop where id_product in (select id_product from tmp_old_products);
delete from ps_stock_available where id_product in (select id_product from tmp_old_products);

-- ištrinkim ir iš tmp_images lentelės
delete from tmp_images where id_product in (select id_product from tmp_old_products);
