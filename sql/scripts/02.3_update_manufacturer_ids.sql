update tmp_products t 
join ps_manufacturer m 
on t.brand = m.name
set t.id_manufacturer = m.id_manufacturer;


update tmp_products_unique t
    join ps_manufacturer m
    on t.brand = m.name
set t.id_manufacturer = m.id_manufacturer;
