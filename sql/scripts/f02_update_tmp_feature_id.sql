# priskiriame kalbos id pagal kalbos kodą (iso_code)

update tmp_feature tf
join ps_lang pl on tf.iso_code collate utf8mb4_general_ci = pl.iso_code
set tf.id_language = pl.id_lang;

# priskiriame esančių id_feature iš ps_feature_lang,
update tmp_feature tf
  join ps_feature_lang fl
    on fl.name = tf.feature_name and fl.id_lang = tf.id_language
set tf.id_feature= fl.id_feature;
