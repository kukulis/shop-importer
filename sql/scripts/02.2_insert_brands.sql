insert ignore into tmp_brands (name)
  select brand from tmp_products where brand is not null;

insert ignore into ps_manufacturer
(name, date_add, date_upd, active )
  select t.name, now() , now(), 1
  from tmp_brands t
  left join ps_manufacturer m on t.name=m.name
  where
    m.id_manufacturer is null;

