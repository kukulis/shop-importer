create or replace table old_image_ids (
    id_image int
);

-- seni paveikslėliai tai tie, kurie buvo anksčiau importuoti (turi sketis_image_id )
-- bet dabar jau nebeimportuojami, t.y. nėra atitikmens tmp_images lentelėje
insert into old_image_ids (id_image)
select i.id_image from ps_image i left join tmp_images ti on i.sketis_image_id = ti.sketis_image_id
where i.sketis_image_id is not null and ti.sketis_image_id is null;

delete from ps_image where id_image in ( select id_image from old_image_ids);

delete from ps_image_shop where id_image in ( select id_image from old_image_ids);
delete from ps_image_lang  where id_image in ( select id_image from old_image_ids);