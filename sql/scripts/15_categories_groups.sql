# insert ignore into ps_category_group
# select id_category, 1 from ps_category;
#
# insert ignore into ps_category_group
#   select id_category, 2 from ps_category;
#
# insert ignore into ps_category_group
#   select id_category, 3 from ps_category;

-- priskiriam ketegorijas visoms grupėms
insert ignore into ps_category_group select c.id_category, g.id_group from ps_category c,
                                      ps_group g;
