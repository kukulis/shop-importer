-- a00_insert_tmp_attributes.sql padaryta lentelė tmp_attributes_combinations


-- select * from tmp_attributes_combinations

update tmp_attributes_combinations c
   join ps_attribute_group_lang pagl
    on c.group_name = pagl.name COLLATE utf8mb4_general_ci
   join ps_lang l on pagl.id_lang = l.id_lang
     set c.id_attribute_group = pagl.id_attribute_group
   where l.iso_code='en';

update tmp_attributes_combinations c
  join auto_attributes aa on c.attribute_code = aa.attribute_code
       and c.id_attribute_group = aa.id_attribute_group
  set c.id_attribute = aa.id_attribute;

update tmp_attributes_combinations c
  join ps_product_attribute pa on c.nomnr = pa.supplier_reference
  set c.id_product_attribute = pa.id_product_attribute;


