create table if not exists tmp_attributes(
   attribute_name varchar(64),
   attribute_code varchar(64),
   group_name varchar(64),
   iso_code varchar(3),
   id_attribute int,
   id_attribute_group int, -- calculated one
   id_lang int,
   unique key ( attribute_code, group_name, iso_code)
);

-- tegul anglų kalba pagrindinė, tuomet ta pati spalva yra ir pavadinimas ir kodas
-- visur kodas tegul būna to atributo pavadinimas anglų kalboje.

insert ignore into tmp_attributes (attribute_name, attribute_code, group_name, iso_code)
select spalva, spalva, 'Colour', iso_code from tmp_products
where iso_code='en' and spalva is not null and spalva != '' and parent is not null and parent != '';

insert ignore into tmp_attributes (attribute_name, attribute_code, group_name, iso_code)
select t.spalva, t2.spalva, 'Spalva', t.iso_code from tmp_products t
join tmp_products t2 on t.nomnr=t2.nomnr
where t.iso_code='lt' and t.spalva is not null and t.spalva != '' and t.parent is not null and t.parent != ''
  and t2.iso_code ='en' and t2.spalva is not null and t2.spalva != '';


insert ignore into tmp_attributes (attribute_name, attribute_code, group_name, iso_code)
select kiekis, kiekis, 'Size', iso_code from tmp_products
where iso_code='en' and kiekis is not null and kiekis != '' and parent is not null and parent != '';

insert ignore into tmp_attributes (attribute_name, attribute_code, group_name, iso_code)
select t.kiekis, t2.kiekis, 'Dydis', t.iso_code from tmp_products t
 join tmp_products t2 on t.nomnr=t2.nomnr
where t.iso_code='lt' and t.kiekis is not null and t.kiekis != '' and t.parent is not null and t.parent != ''
      and t2.iso_code='en' and  t2.kiekis is not null and t2.kiekis != '';

insert ignore into tmp_attributes (attribute_name,  attribute_code, group_name, iso_code)
select var_name, var_name, 'Variant', iso_code from tmp_products
where iso_code='en' and var_name is not null and var_name != '' and parent is not null and parent != '';

insert ignore into tmp_attributes (attribute_name, attribute_code, group_name, iso_code)
select t.var_name, t2.var_name, 'Variantas', t.iso_code from tmp_products t
  join tmp_products t2 on t.nomnr=t2.nomnr
  where t.iso_code='lt' and t.var_name is not null and t.var_name != '' and t.parent is not null and t.parent != ''
  and  t2.iso_code='en' and t2.var_name is not null and t2.var_name != '';


-- čia pat padarykim atributų lentelę su prekių nomnr


create table if not exists tmp_attributes_combinations(
     attribute_code varchar(64),
     group_name varchar(64),
     nomnr varchar(32),

     id_attribute int,
     id_attribute_group int, -- calculated one
     id_product_attribute int,
     unique key ( attribute_code, group_name, nomnr)
);

insert ignore into tmp_attributes_combinations (attribute_code, group_name, nomnr)
select spalva, 'Colour', nomnr from tmp_products
where iso_code='en' and spalva is not null and spalva != '' and parent is not null and parent != '';

insert ignore into tmp_attributes_combinations (attribute_code, group_name, nomnr)
select kiekis, 'Size', nomnr from tmp_products
where iso_code='en' and kiekis is not null and kiekis != '' and parent is not null and parent != '';

insert ignore into tmp_attributes_combinations (attribute_code, group_name, nomnr)
select var_name, 'Variant', nomnr from tmp_products
where iso_code='en' and var_name is not null and var_name != '' and parent is not null and parent != '';

-- select * from tmp_attributes_combinations

