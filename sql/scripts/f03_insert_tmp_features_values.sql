# kaip atsekti kad tas pats featuras dviejose kalbose yra tikrai tas pats?

truncate  tmp_feature_value;

# Imam iš vienos kalbos, dėl to, kad tmp_feature_value labiausiai atitinka, ps_feature_value, kur
# yra vienas įrašas visoms kalboms.
# Tuo tarpu čia imam iš ps_feature_value_lang, kur vienam id_feature_value tenka kelios reikšmės
# - po vieną kiekvienai kalbai. Imam iš ps_feature_value, dėl to kad tik čia yra laukas 'value'.

-- insert existing values form ps_feature_value_lang
insert into tmp_feature_value (id_feature_value, id_feature, custom, value)
select fvl.id_feature_value, fv.id_feature, fv.custom, fvl.value
  from ps_feature_value_lang fvl
  join ps_feature_value fv on fvl.id_feature_value = fv.id_feature_value
  join ps_lang pl on fvl.id_lang = pl.id_lang
where pl.iso_code='lt' and fv.id_feature != 0;


# tmp_feature labiausiai atitinka ps_feature_value_lang lentelę, todėl čia bus vienai savybei
# kelios vertės - po vieną kiekvienai kalbai.
# todėl reikia imti vienos pasirinktos kalbos vertes.

-- insert new values from tmp_feature
insert ignore into tmp_feature_value
  (id_feature, custom, value)
  select tf.id_feature, 0, trim(tf.feature_value)  from tmp_feature tf
where tf.feature_value is not null and length( trim(tf.feature_value) ) > 0 and tf.iso_code='lt';




