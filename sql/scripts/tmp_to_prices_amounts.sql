
-- update products ids
update tmp_sketis_prices_amounts ta
  join ps_product p on ta.nomnr=p.supplier_reference
set ta.id_product = p.id_product;

-- update parent ids from tmp_product_attribute
 update tmp_sketis_prices_amounts ta
join tmp_product_attribute tpa on ta.nomnr = tpa.supplier_reference
set ta.id_product = tpa.id_product,
    ta.id_product_attribute = tpa.id_product_attribute;


-- update prices into ps_product
update ps_product p
  join tmp_sketis_prices_amounts t on p.id_product=t.id_product
set p.price = t.price, p.wholesale_price = t.wholesale_price, p.quantity=t.amount;

-- update prices and amount into ps_product_shop

update ps_product_shop ps
  join tmp_sketis_prices_amounts t on ps.id_product=t.id_product
set ps.price = t.price, ps.wholesale_price = t.wholesale_price;

-- select * from tmp_sketis_prices_amounts;
--
# ps_stock_available
insert into ps_stock_available
  (id_product, id_product_attribute, id_shop, id_shop_group, quantity)
select t.id_product, t.id_product_attribute, 1, 0, t.amount from tmp_sketis_prices_amounts t
  left join  ps_stock_available sa on t.id_product=sa.id_product
where sa.id_stock_available is null and t.id_product is not null;

insert into ps_stock_available
(id_product, id_product_attribute, id_shop, id_shop_group, quantity)
select t.id_product, t.id_product_attribute, 1, 0, t.amount
from tmp_sketis_prices_amounts t
         left join  ps_stock_available sa on t.id_product=sa.id_product
    and ( t.id_product_attribute = sa.id_product_attribute or t.id_product_attribute is null and sa.id_product_attribute=0)
where sa.id_stock_available is null
  and t.id_product is not null;


update ps_stock_available ps
join tmp_sketis_prices_amounts t on ps.id_product=t.id_product
        and ( t.id_product_attribute = ps.id_product_attribute or t.id_product_attribute is null and ps.id_product_attribute=0)
set ps.quantity=t.amount;

# -- zero unmentioned tmp products
# vien šito nepakanka, gali būti per ankstesnį importą likusi prekė , kurios dabartiniame importe nebereikia
# todėl yra padarytas skriptas 00_zero_tmp_amounts.sql

-- čia kai niekas neateina iš likučių
-- Pridėjau prie WHERE p.supplier_reference REGEXP '^E_[[:digit:]]{7}$'
--  kad neišjungtų rankiniu būdu sukeltus produktus :(
update ps_product p left join
  tmp_sketis_prices_amounts t on p.id_product=t.id_product
set p.quantity=0,
    p.visibility='none'
where t.id_product is null and p.supplier_reference REGEXP '^E_[[:digit:]]{7}$' ;

-- čia, kai ateina 0
-- Jeigu bus noro rodyti prekę su 0 likučių, tai reiks šitą pašalinti
update ps_product p left join
    tmp_sketis_prices_amounts t on p.id_product=t.id_product
set p.quantity=0,
    p.visibility=@ZERO_PRODUCTS_DISPLAY
where t.amount=0;


-- atslepiam kai likutis ne 0
update ps_product p join
    tmp_sketis_prices_amounts t on p.id_product=t.id_product
set p.visibility='both'
where t.amount > 0;

-- šį matomumą perkeliam į ps_product_shop lentelę

update ps_product_shop s
join ps_product pp on s.id_product = pp.id_product and s.id_shop=1
set s.visibility=pp.visibility
where s.visibility != pp.visibility;


update ps_stock_available a left join tmp_sketis_prices_amounts t on a.id_product=t.id_product
set a.quantity=0
where t.id_product is null;

-- variantų kiekių sumos tėvinėms prekėms
insert into ps_stock_available (
id_product,
id_product_attribute,
id_shop,
id_shop_group,
quantity,
physical_quantity,
reserved_quantity,
depends_on_stock,
out_of_stock
)
select
       id_product,
       0,
       1,
       0,
       parent_quantity,
       0,
       0,
       0,
       0
    from
(
    select sum(quantity) parent_quantity, id_product from ps_stock_available
    where id_product_attribute != 0 and id_product_attribute is not null
    group by id_product ) pqs
on duplicate key update quantity=values(quantity);

-- dabar updeitinam su tuo pačiu kiekiu ps_products lentelę

update ps_product p
join ps_stock_available pa on p.id_product=pa.id_product and pa.id_product_attribute = 0
set p.quantity=pa.quantity;


-- select * from ps_stock_available where id_product_attribute=0 and id_product in ( 20,63,66)
-- select * from ps_product_attribute
-- select * from ps_product_attribute_shop
-- select * from ps_product_attribute where id_product=63;

# select * from tmp_sketis_prices_amounts
#
# select * from ps_product where id_product=63;
# Mantas nesupranta kam čia daroma a.price=tspa.price-pp.price
# Tiekėjų kaina (savikaina už kurią mes gauname) turi būt wholesale_price stulpelyje

update ps_product_attribute a
join tmp_sketis_prices_amounts tspa on a.id_product_attribute = tspa.id_product_attribute
join ps_product pp on a.id_product = pp.id_product
set a.price=tspa.price-pp.price;

update ps_product_attribute_shop a
    join tmp_sketis_prices_amounts tspa on a.id_product_attribute = tspa.id_product_attribute
    join ps_product pp on a.id_product = pp.id_product
set a.price=tspa.price-pp.price;