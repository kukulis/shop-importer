insert into ps_manufacturer_lang
  (id_manufacturer, id_lang, description)
  select id_manufacturer, ifnull(id_lang, 1), brand from tmp_products
on duplicate key update ps_manufacturer_lang.description=tmp_products.brand, ps_manufacturer_lang.meta_keywords=tmp_products.brand;


insert ignore  into ps_manufacturer_shop
select id_manufacturer, 1 from tmp_products;