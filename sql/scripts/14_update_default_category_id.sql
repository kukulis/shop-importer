update ps_product p
join (select min(cp.id_category) id_category, cp.id_product from ps_category_product cp group by id_product) mincat
on p.id_product = mincat.id_product
set p.id_category_default = case when p.id_category_default > 2 then p.id_category_default else mincat.id_category end;


update ps_product_shop p
  join (select min(cp.id_category) id_category, cp.id_product from ps_category_product cp group by id_product) mincat
  on p.id_product = mincat.id_product
set p.id_category_default = case when p.id_category_default > 2 then p.id_category_default else mincat.id_category end;
