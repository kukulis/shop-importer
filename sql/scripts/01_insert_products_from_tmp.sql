INSERT IGNORE INTO ps_product (
  id_supplier,
  id_manufacturer,
  id_category_default,
  id_shop_default,
  id_tax_rules_group,
  on_sale,
  online_only,
  ean13,
  isbn,
  upc,
  mpn,
  ecotax,
  quantity,
  minimal_quantity,
  low_stock_threshold,
  low_stock_alert,
  price,
  wholesale_price,
  unity,
  unit_price_ratio,
  additional_shipping_cost,
  reference,
  supplier_reference,
  location,
  width,
  height,
  depth,
  weight,
  out_of_stock,
  additional_delivery_times,
  quantity_discount,
  customizable,
  uploadable_files,
  text_fields,
  active,
  redirect_type,
  id_type_redirected,
  available_for_order,
  available_date,
  show_condition,
  `condition`,
  show_price,
  indexed,
  visibility,
  cache_is_pack,
  cache_has_attachments,
  is_virtual,
  cache_default_attribute,
  date_add,
  date_upd,
  advanced_stock_management,
  pack_stock_type,
  state
)
select
  t.id_supplier,
  t.id_manufacturer,
  t.id_category_default,
  t.id_shop_default,
  t.id_tax_rules_group,
  t.on_sale,
  t.online_only,
  -- ifnull(t.ean13, t.nomnr) ean13,
  t.ean13,
  t.isbn,
  t.upc,
  t.mpn,
  t.ecotax,
  t.quantity,
  t.minimal_quantity,
  t.low_stock_threshold,
  t.low_stock_alert,
  t.price,
  t.wholesale_price,
  t.unity,
  t.unit_price_ratio,
  t.additional_shipping_cost,
  ifnull(t.reference, t.nomnr ) reference ,
  t.supplier_reference,
  t.location,
  t.width,
  t.height,
  t.depth,
  t.weight,
  t.out_of_stock,
  t.additional_delivery_times,
  t.quantity_discount,
  t.customizable,
  t.uploadable_files,
  t.text_fields,
  t.active,
  t.redirect_type,
  t.id_type_redirected,
  t.available_for_order,
  t.available_date,
  t.show_condition,
  t.`condition`,
  t.show_price,
  t.indexed,
  t.visibility,
  t.cache_is_pack,
  t.cache_has_attachments,
  t.is_virtual,
  t.cache_default_attribute,
  t.date_add,
  t.date_upd,
  t.advanced_stock_management,
  t.pack_stock_type,
  t.state
  from tmp_products_unique t left join ps_product p on t.nomnr=p.supplier_reference
where p.id_product is null and (t.parent is null or t.parent = '');

-- select * from ps_product
-- select * from ps_product_lang
