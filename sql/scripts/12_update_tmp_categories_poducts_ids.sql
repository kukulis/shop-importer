update tmp_categories_products t
  join ps_category c on t.sketis_code = c.sketis_code
  join ps_product p on t.nomnr = p.supplier_reference
set t.id_product = p.id_product,
    t.id_category = c.id_category;


