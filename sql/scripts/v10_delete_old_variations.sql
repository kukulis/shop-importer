create or replace  table tmp_old_product_attribute (
    id_product_attribute int
);

create or replace  table tmp_old_products (
    id_product int
);

insert into tmp_old_product_attribute select a.id_product_attribute from ps_product_attribute a left join tmp_product_attribute tpa on a.reference=tpa.reference
where tpa.reference is null;

insert into tmp_old_products select a.id_product from ps_product_attribute a left join tmp_product_attribute tpa on a.reference=tpa.reference
where tpa.reference is null;

delete from ps_product_attribute where id_product_attribute in ( select id_product_attribute from tmp_old_product_attribute);

delete from tmp_images where id_product in ( select id_product from tmp_old_products );
