update ps_category c
join tmp_categories tc
on c.id_category = tc.id_category
set
  c.id_parent         = ifnull(tc.id_parent, c.id_parent),
  c.id_shop_default   = tc.id_shop_default,
  c.level_depth       = tc.level_depth,
  c.nleft             = tc.nleft,
  c.nright            = tc.nright,
  c.active            = tc.active,
  c.date_add          = tc.date_add,
  c.date_upd          = tc.date_upd,
  c.position          = tc.position,
  c.is_root_category  = tc.is_root_category,
  c.sketis_code       = tc.sketis_code;


