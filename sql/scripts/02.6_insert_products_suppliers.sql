insert ignore into ps_product_supplier (
 id_product,
 id_supplier,
 id_product_attribute,
 product_supplier_reference,
 product_supplier_price_te,
 id_currency)
select
 id_product,
 id_supplier,
 0,
 null,
 0.0,
 1
from tmp_products_unique
where id_supplier != 0;
