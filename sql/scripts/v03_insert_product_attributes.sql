insert into ps_product_attribute (
id_product,
reference,
supplier_reference,
location,
ean13,
isbn,
upc,
wholesale_price,
price,
ecotax,
quantity,
weight,
unit_price_impact,
default_on,
minimal_quantity,
low_stock_threshold,
low_stock_alert,
available_date
  )
  select
      tpa.id_product,
      tpa.reference,
      tpa.supplier_reference,
      tpa.location,
      tpa.ean13,
      tpa.isbn,
      tpa.upc,
      tpa.wholesale_price,
      tpa.price,
      tpa.ecotax,
      tpa.quantity,
      tpa.weight,
      tpa.unit_price_impact,
      tpa.default_on,
      tpa.minimal_quantity,
      tpa.low_stock_threshold,
      tpa.low_stock_alert,
      tpa.available_date
from tmp_product_attribute tpa
  left join ps_product_attribute ppa on tpa.reference = ppa.reference
where
  ppa.id_product_attribute is null
  and tpa.id_product is not null
;


-- select * from ps_product_attribute
-- select * from tmp_product_attribute