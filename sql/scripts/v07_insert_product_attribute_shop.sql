insert ignore into ps_product_attribute_shop
    (id_product,
     id_product_attribute,
     id_shop, default_on,
     low_stock_threshold,
     available_date)
select id_product,
       id_product_attribute,
       1,
       null,
       1,
       null
from ps_product_attribute;