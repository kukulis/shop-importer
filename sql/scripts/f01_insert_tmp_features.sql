# tmp_feature lentelė atitinka ps_feature_value_lang
# tmp_feature_value atitinka ps_feature_value
# problema tame: kad norint atrinkti unikalias reikšmes, reikia pasirinkti kažkokią kalbą, o jeigu kitoje kalboje reikšmė skiriasi
# tai bus pasirinkta tik viena  reikšmė (vertė) , o likusios prarastos.

DELIMITER $$

CREATE OR REPLACE PROCEDURE createFeature(
    IN pName1 varchar(64),
    IN pIso_code1 varchar(3),
    IN pName2 varchar(64),
    IN pIso_code2 varchar(3),
    OUT result varchar(255)
)

BEGIN
    DECLARE featureId int default 0;
    DECLARE newPosition int default 0;
    DECLARE newFeatureId int default 0;
    DECLARE languageId1 int default 1;
    DECLARE languageId2 int default 1;

    select id_lang into languageId1 from ps_lang where iso_code collate utf8mb4_general_ci = pIso_code1;
    select id_lang into languageId2 from ps_lang where iso_code collate utf8mb4_general_ci = pIso_code2;

    select id_feature into featureId from ps_feature_lang where  name=pName1 and id_lang=languageId1;
    if featureId = 0 THEN
        -- creating new
        select ifnull(max(position)+1, 1 ) into newPosition from ps_feature;
        insert into ps_feature (position) values ( newPosition );
        select last_insert_id() into newFeatureId;

        insert into ps_feature_lang (id_feature, id_lang, name) values (newFeatureId, languageId1, pName1 );
        insert into ps_feature_shop (id_feature, id_shop) values (newFeatureId, 1 );

        set featureId = newFeatureId;

        set result= concat( pName1,  'Feature created' );
    else
        set result= concat( pName1,  'Feature already exists' );
    end if;

    -- creating in language 2
    insert ignore into ps_feature_lang (id_feature, id_lang, name) values (featureId, languageId2, pName2 );

END$$

DELIMITER ;

-- select * from ps_feature_lang;

call createFeature('Tipas', 'lt', 'Type', 'en', @rez1);
call createFeature('Paskirtis', 'lt', 'Appliance', 'en', @rez2);
call createFeature('Dydis', 'lt', 'Size', 'en', @rez3);
call createFeature('Kiekis', 'lt', 'Quantity', 'en',@rez4);
select @rez1, @rez2, @rez3, @rez4;


# surenkam savybių reikšmes (vertes) iš tmp_products.

truncate table tmp_feature;

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
  select  'Tipas', p.tipas, p.nomnr, p.iso_code from tmp_products p where p.iso_code='lt' and p.tipas is not null and p.tipas != '';

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
select  'Type', p.tipas, p.nomnr, p.iso_code from tmp_products p where p.iso_code='en' and p.tipas is not null and p.tipas != '';

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
  select  'Paskirtis', p.paskirtis, p.nomnr, p.iso_code from tmp_products p  where p.iso_code='lt' and p.paskirtis is not null and p.paskirtis != '';

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
select  'Appliance', p.paskirtis, p.nomnr, p.iso_code from tmp_products p where p.iso_code='en' and p.paskirtis is not null and p.paskirtis != '';;

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
  select  'Dydis', p.dydis, p.nomnr, p.iso_code from tmp_products p where p.iso_code='lt' and p.dydis is not null and p.dydis != '';

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
select  'Size', p.dydis, p.nomnr, p.iso_code from tmp_products p where p.iso_code='en' and p.dydis is not null and p.dydis != '';

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
  select  'Kiekis', p.kiekis, p.nomnr, p.iso_code from tmp_products p where p.iso_code='lt' and p.kiekis is not null and p.kiekis != '';

insert into tmp_feature (feature_name, feature_value, nomnr, iso_code)
select  'Quantity', p.kiekis, p.nomnr, p.iso_code from tmp_products p where p.iso_code='en' and p.kiekis is not null and p.kiekis != '';
