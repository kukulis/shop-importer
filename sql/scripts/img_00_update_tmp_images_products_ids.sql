update
  tmp_images img
  join ps_product p on img.nomnr=p.supplier_reference
  set img.id_product=p.id_product;

update
    tmp_images img
join ps_product_attribute a on img.nomnr = a.supplier_reference
   set img.id_product_attribute = a.id_product_attribute,
       img.id_product = a.id_product;
