insert into ps_category (
    id_parent,
    id_shop_default,
    level_depth,
    nleft,
    nright,
    active,
    date_add,
    date_upd,
    position,
    is_root_category,
    sketis_code
    )
select
    ifnull(t.id_parent,0) id_parent,
    t.id_shop_default,
    t.level_depth,
    t.nleft,
    t.nright,
    t.active,
    t.date_add,
    t.date_upd,
    t.position,
    t.is_root_category,
    t.sketis_code
from tmp_categories_unique t left join ps_category c on t.sketis_code=c.sketis_code
where c.id_category is null;

-- delete from ps_category
-- select count(1) from ps_category;
