create or replace table tmp_attributes
(
    attribute_name varchar(64) null,
    attribute_code varchar(64) null,
    group_name varchar(64) null,
    iso_code varchar(3) null,
    id_attribute int null,
    id_attribute_group int null,
    id_lang int null,
    constraint attribute_code
        unique (attribute_code, group_name, iso_code)
);

create or replace table tmp_attributes_combinations
(
    attribute_code varchar(64) null,
    group_name varchar(64) null,
    nomnr varchar(32) null,
    id_attribute int null,
    id_attribute_group int null,
    id_product_attribute int null,
    constraint attribute_code
        unique (attribute_code, group_name, nomnr)
) ;

create or replace table tmp_brands
(
    name varchar(64) not null
        primary key
) ;

create or replace table tmp_categories
(
    sketis_code varchar(100) not null,
    parent_code varchar(100) not null,
    id_category int unsigned null,
    id_parent int unsigned null,
    id_shop_default int unsigned default 1 not null,
    level_depth tinyint unsigned default 0 not null,
    nleft int unsigned default 0 not null,
    nright int unsigned default 0 not null,
    active tinyint(1) unsigned default 0 not null,
    date_add datetime not null,
    date_upd datetime not null,
    position int unsigned default 0 not null,
    is_root_category tinyint(1) default 0 not null,
    id_shop int(11) unsigned default 1 not null,
    id_lang int unsigned not null,
    name varchar(128) not null,
    description text null,
    link_rewrite varchar(128) not null,
    meta_title varchar(255) null,
    meta_keywords varchar(255) null,
    meta_description varchar(512) null,
    iso_code char(3) default 'en' not null,
    primary key (sketis_code, iso_code)
) ;

create index category_id_idx
    on tmp_categories (id_category);

create index category_parent_idx
    on tmp_categories (id_parent);

create or replace table tmp_categories_products
(
    position int null,
    sketis_code varchar(100) not null,
    nomnr char(32) not null,
    id_category int null,
    id_product int null,
    primary key (sketis_code, nomnr)
) ;

create index tmp_catprod_category_id_idx
    on tmp_categories_products (id_category);

create index tmp_catprod_product_id_idx
    on tmp_categories_products (id_product);

create or replace table tmp_categories_unique
(
    sketis_code varchar(100) not null
        primary key,
    parent_code varchar(100) not null,
    id_category int unsigned null,
    id_parent int unsigned null,
    id_shop_default int unsigned default 1 not null,
    level_depth tinyint unsigned default 0 not null,
    nleft int unsigned default 0 not null,
    nright int unsigned default 0 not null,
    active tinyint(1) unsigned default 0 not null,
    date_add datetime not null,
    date_upd datetime not null,
    position int unsigned default 0 not null,
    is_root_category tinyint(1) default 0 not null,
    id_shop int(11) unsigned default 1 not null,
    id_lang int unsigned not null,
    name varchar(128) not null,
    description text null,
    link_rewrite varchar(128) not null,
    meta_title varchar(255) null,
    meta_keywords varchar(255) null,
    meta_description varchar(512) null,
    iso_code char(3) null
) ;

create index category_id_idx
    on tmp_categories_unique (id_category);

create index category_parent_idx
    on tmp_categories_unique (id_parent);

create or replace table tmp_feature
(
    feature_name varchar(64) null,
    feature_value varchar(64) null,
    nomnr varchar(32) null,
    id_feature int null,
    id_feature_value int null,
    id_product int null,
    iso_code varchar(3),
    id_language int
) ;

create index tmp_feature_id_product_idx
    on tmp_feature (id_product);

create index tmp_feature_name_idx
    on tmp_feature (feature_name);

create index tmp_feature_nomnr_idx
    on tmp_feature (nomnr);

create index tmp_feature_value_idx
    on tmp_feature (feature_value);

create or replace table tmp_feature_value
(
    id_feature_value int unsigned auto_increment
        primary key,
    id_feature int unsigned not null,
    custom tinyint unsigned null,
    value varchar(64) null,
    constraint tmp_feature_value_uk
        unique (id_feature, value)
) ;

create or replace table tmp_images
(
    sketis_image_id varchar(32) not null
        primary key,
    uri varchar(255) null,
    nomnr char(32) null,
    position tinyint null,
    version int null,
    uploaded_version int null,
    copied_to_live tinyint default 0 null,
    destination_path varchar(255) null,
    id_product int null,
    id_shop int null,
    id_image int null,
    cover tinyint null,
    resized_version int null,
    id_product_attribute int null
) ;

create or replace table tmp_product_attribute
(
    id_product_attribute int null,
    parent_reference varchar(32) null,
    id_product int unsigned not null,
    reference varchar(64) not null
        primary key,
    supplier_reference varchar(64) null,
    location varchar(64) null,
    ean13 varchar(13) null,
    isbn varchar(32) null,
    upc varchar(12) null,
    wholesale_price decimal(20,6) default 0.000000 not null,
    price decimal(20,6) default 0.000000 not null,
    ecotax decimal(17,6) default 0.000000 not null,
    quantity int(10) default 0 not null,
    weight decimal(20,6) default 0.000000 not null,
    unit_price_impact decimal(20,6) default 0.000000 not null,
    default_on tinyint(1) unsigned null,
    minimal_quantity int unsigned default 1 not null,
    low_stock_threshold int(10) null,
    low_stock_alert tinyint(1) default 0 not null,
    available_date date null
);

create or replace table tmp_product_tag
(
    nomnr char(32) not null,
    tag_name varchar(32) not null,
    iso_code varchar(3) not null,
    id_product int null,
    id_tag int null,
    id_lang int null,
    primary key (nomnr, tag_name, iso_code)
) ;

create or replace table tmp_products
(
    nomnr char(32) not null,
    id_product int null,
    id_supplier int unsigned null,
    supplier varchar(64) null,
    id_manufacturer int unsigned null,
    id_category_default int unsigned null,
    id_shop_default int unsigned default 1 not null,
    id_tax_rules_group int(11) unsigned not null,
    on_sale tinyint(1) unsigned default 0 not null,
    online_only tinyint(1) unsigned default 0 not null,
    ean13 varchar(13) null,
    isbn varchar(32) null,
    upc varchar(12) null,
    mpn varchar(40) null,
    ecotax decimal(17,6) default 0.000000 not null,
    quantity int(10) default 0 not null,
    minimal_quantity int unsigned default 1 not null,
    low_stock_threshold int(10) null,
    low_stock_alert tinyint(1) default 0 not null,
    price decimal(20,6) default 0.000000 not null,
    wholesale_price decimal(20,6) default 0.000000 not null,
    unity varchar(255) null,
    unit_price_ratio decimal(20,6) default 0.000000 not null,
    additional_shipping_cost decimal(20,2) default 0.00 not null,
    reference varchar(64) null,
    supplier_reference varchar(64) null,
    location varchar(64) null,
    width decimal(20,6) default 0.000000 not null,
    height decimal(20,6) default 0.000000 not null,
    depth decimal(20,6) default 0.000000 not null,
    weight decimal(20,6) default 0.000000 not null,
    out_of_stock int unsigned default 2 not null,
    additional_delivery_times tinyint(1) unsigned default 1 not null,
    quantity_discount tinyint(1) default 0 null,
    customizable tinyint(2) default 0 not null,
    uploadable_files tinyint default 0 not null,
    text_fields tinyint default 0 not null,
    active tinyint(1) unsigned default 0 not null,
    redirect_type enum('', '404', '301-product', '302-product', '301-category', '302-category') default '' not null,
    id_type_redirected int unsigned default 0 not null,
    available_for_order tinyint(1) default 1 not null,
    available_date date null,
    show_condition tinyint(1) default 0 not null,
    `condition` enum('new', 'used', 'refurbished') default 'new' not null,
    show_price tinyint(1) default 1 not null,
    indexed tinyint(1) default 0 not null,
    visibility enum('both', 'catalog', 'search', 'none') default 'both' not null,
    cache_is_pack tinyint(1) default 0 not null,
    cache_has_attachments tinyint(1) default 0 not null,
    is_virtual tinyint(1) default 0 not null,
    cache_default_attribute int unsigned null,
    date_add datetime not null,
    date_upd datetime not null,
    advanced_stock_management tinyint(1) default 0 not null,
    pack_stock_type int(11) unsigned default 3 not null,
    state int(11) unsigned default 1 not null,
    id_shop int(11) unsigned default 1 not null,
    id_lang int unsigned null,
    description text null,
    description_short text null,
    link_rewrite varchar(128) not null,
    meta_description varchar(512) null,
    meta_keywords varchar(255) null,
    meta_title varchar(128) null,
    name varchar(128) not null,
    available_now varchar(255) null,
    available_later varchar(255) null,
    delivery_in_stock varchar(255) null,
    delivery_out_stock varchar(255) null,
    brand varchar(64) null,
    tipas varchar(64) null,
    paskirtis varchar(64) null,
    dydis varchar(64) null,
    kiekis varchar(64) null,
    iso_code varchar(3) not null,
    parent varchar(32) null,
    spalva varchar(32) null,
    var_name varchar(32) null,
    primary key (nomnr, iso_code)
) ;

create index tmp_product_id_idx
    on tmp_products (id_product);

create index tmp_products_brand_index
    on tmp_products (brand);

create or replace table tmp_products_unique
(
    nomnr char(32) not null
        primary key,
    id_product int null,
    id_supplier int unsigned null,
    supplier varchar(64) null,
    id_manufacturer int unsigned null,
    id_category_default int unsigned null,
    id_shop_default int unsigned default 1 not null,
    id_tax_rules_group int(11) unsigned not null,
    on_sale tinyint(1) unsigned default 0 not null,
    online_only tinyint(1) unsigned default 0 not null,
    ean13 varchar(13) null,
    isbn varchar(32) null,
    upc varchar(12) null,
    mpn varchar(40) null,
    ecotax decimal(17,6) default 0.000000 not null,
    quantity int(10) default 0 not null,
    minimal_quantity int unsigned default 1 not null,
    low_stock_threshold int(10) null,
    low_stock_alert tinyint(1) default 0 not null,
    price decimal(20,6) default 0.000000 not null,
    wholesale_price decimal(20,6) default 0.000000 not null,
    unity varchar(255) null,
    unit_price_ratio decimal(20,6) default 0.000000 not null,
    additional_shipping_cost decimal(20,2) default 0.00 not null,
    reference varchar(64) null,
    supplier_reference varchar(64) null,
    location varchar(64) null,
    width decimal(20,6) default 0.000000 not null,
    height decimal(20,6) default 0.000000 not null,
    depth decimal(20,6) default 0.000000 not null,
    weight decimal(20,6) default 0.000000 not null,
    out_of_stock int unsigned default 2 not null,
    additional_delivery_times tinyint(1) unsigned default 1 not null,
    quantity_discount tinyint(1) default 0 null,
    customizable tinyint(2) default 0 not null,
    uploadable_files tinyint default 0 not null,
    text_fields tinyint default 0 not null,
    active tinyint(1) unsigned default 0 not null,
    redirect_type enum('', '404', '301-product', '302-product', '301-category', '302-category') default '' not null,
    id_type_redirected int unsigned default 0 not null,
    available_for_order tinyint(1) default 1 not null,
    available_date date null,
    show_condition tinyint(1) default 0 not null,
    `condition` enum('new', 'used', 'refurbished') default 'new' not null,
    show_price tinyint(1) default 1 not null,
    indexed tinyint(1) default 0 not null,
    visibility enum('both', 'catalog', 'search', 'none') default 'both' not null,
    cache_is_pack tinyint(1) default 0 not null,
    cache_has_attachments tinyint(1) default 0 not null,
    is_virtual tinyint(1) default 0 not null,
    cache_default_attribute int unsigned null,
    date_add datetime not null,
    date_upd datetime not null,
    advanced_stock_management tinyint(1) default 0 not null,
    pack_stock_type int(11) unsigned default 3 not null,
    state int(11) unsigned default 1 not null,
    id_shop int(11) unsigned default 1 not null,
    id_lang int unsigned null,
    description text null,
    description_short text null,
    link_rewrite varchar(128) not null,
    meta_description varchar(512) null,
    meta_keywords varchar(255) null,
    meta_title varchar(128) null,
    name varchar(128) not null,
    available_now varchar(255) null,
    available_later varchar(255) null,
    delivery_in_stock varchar(255) null,
    delivery_out_stock varchar(255) null,
    brand varchar(64) null,
    tipas varchar(64) null,
    paskirtis varchar(64) null,
    dydis varchar(64) null,
    kiekis varchar(64) null,
    iso_code varchar(3) not null,
    parent varchar(32) null,
    spalva varchar(32) null,
    var_name varchar(32) null
) ;

create index tmp_products_brand_index
    on tmp_products_unique (brand);

create index tmp_products_unique_id_idx
    on tmp_products_unique (id_product);

create or replace table tmp_sketis_prices_amounts
(
    nomnr varchar(32) not null
        primary key,
    amount int null,
    price decimal(10,2) null,
    wholesale_price decimal(10,2) null,
    id_product int null,
    id_product_attribute int null
) ;

create index tmp_sketis_prices_amounts_id_product_index
    on tmp_sketis_prices_amounts (id_product);

create or replace table tmp_special_price
(
    nomnr char(32) null,
    group_name varchar(64) null,
    price decimal(10,2) null,
    id_product int null,
    id_group int null,
    id_specific_price int null
) ;

create index tmp_special_price_group_idx
    on tmp_special_price (group_name);

create index tmp_special_price_key_index
    on tmp_special_price (id_product, id_group);

create index tmp_special_price_nomnr_idx
    on tmp_special_price (nomnr);

create or replace table tmp_tag
(
    tag_name varchar(32) not null,
    iso_code varchar(3) not null,
    id_tag int null,
    id_lang int null,
    primary key (tag_name, iso_code)
) ;


alter table ps_category
    add sketis_code varchar(100) null;

create index ps_category_sketis_code_index
    on ps_category (sketis_code);

create index ps_product_ean13_idx
    on ps_product (ean13);

create index ps_product_reference_idx
    on ps_product (reference);

update ps_carrier set NAME='OFICE' where id_carrier=1;

update ps_category set sketis_code='shop_root' where is_root_category=1;

-- gal į atskirą failą perkelti
alter table ps_image
    add sketis_image_id varchar(32); -- naudojamas susieti presta pav su mūsų pav.


create index ps_image_sketis_img_idx
    on ps_image (sketis_image_id);