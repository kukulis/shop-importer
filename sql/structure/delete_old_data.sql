truncate ps_product;
truncate ps_product_lang;
truncate ps_product_shop;
truncate ps_stock_available;
truncate ps_product_attribute;
truncate ps_product_attribute_combination;
truncate ps_product_attribute_image;
truncate ps_product_attribute_shop;

truncate ps_feature_product;
truncate ps_feature_shop;
truncate ps_feature_value;
truncate ps_feature_value_lang;

truncate ps_image;

delete from ps_category where id_category > 2;
delete from ps_category_shop where id_category > 2;
delete from ps_category_lang where id_category > 2;
delete from ps_category_group where id_category > 2;

truncate ps_category_product;

truncate tmp_attributes;
truncate tmp_attributes_combinations;
truncate tmp_brands;
truncate tmp_categories;
truncate tmp_categories_products;
truncate tmp_categories_unique;
truncate tmp_feature;
truncate tmp_feature_value;
-- truncate tmp_id_products;
truncate tmp_images;
truncate tmp_old_products;
truncate tmp_product_attribute;
truncate tmp_product_tag;
truncate tmp_products;
truncate tmp_products_unique;
truncate tmp_sketis_prices_amounts;
truncate tmp_special_price;
truncate tmp_tag;

