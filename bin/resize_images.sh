#!/usr/bin/env bash

set -e

# mysql prisijungimo parametrai yra .my.cnf faile
. bin/settings.sh


echo "Resizing images"
php bin/commands/resize_images.php $SHOP_BASE_PATH 100
