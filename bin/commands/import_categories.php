<?php
require __DIR__ . '/../../vendor/autoload.php';
ini_set("error_reporting", "-1" );
$importCategoriesCommand = Kernel::instance()->getImportCategoriesCommand();
try {
    return $importCategoriesCommand->run($argv);
} catch (\Exception $e ) {
    echo "nutiko klaida: ".$e->getMessage()."\n";
}