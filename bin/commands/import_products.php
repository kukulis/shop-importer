<?php
require __DIR__ . '/../../vendor/autoload.php';
ini_set("error_reporting", "-1" );
$importProductsCommand = Kernel::instance()->getImportProductsCommand();
try {
    return $importProductsCommand->run($argv);
} catch (\Exception $e ) {
    echo "nutiko klaida: ".$e->getMessage()."\n";
}