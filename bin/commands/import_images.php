<?php
require __DIR__ . '/../../vendor/autoload.php';
ini_set("error_reporting", "-1" );
$importImagesCommand = Kernel::instance()->getImportImagesCommand();
try {
    return $importImagesCommand->run($argv);
} catch (\Exception $e ) {
    echo "nutiko klaida: ".$e->getMessage()."\n";
}