<?php
require __DIR__ . '/../../vendor/autoload.php';
ini_set("error_reporting", "-1" );
$importCategoriesCommand = Kernel::instance()->getBuildCategoriesTreeCommand();
try {
    return $importCategoriesCommand->run();
} catch (\Exception $e ) {
    echo "nutiko klaida: ".$e->getMessage()."\n";
    echo "Trace: ". $e->getTraceAsString();
}