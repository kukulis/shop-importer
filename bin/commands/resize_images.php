<?php
require __DIR__ . '/../../vendor/autoload.php';
ini_set("error_reporting", "-1");
$resizeImagesCommand = Kernel::instance()->getResizeImagesCommands();
try {
    return $resizeImagesCommand->run($argv);
} catch (\Exception $e) {
    echo "nutiko klaida: " . $e->getMessage() . "\n";
    echo "Trace: " . $e->getTraceAsString();
}

