<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.8.10
 * Time: 19.21
 */

require __DIR__ . '/../../vendor/autoload.php';
ini_set("error_reporting", "-1" );
$importAmountCommand = Kernel::instance()->getImportAmountsAndPricesCommand();

try {
    return $importAmountCommand->run($argv);
} catch (\Exception $e ) {
    echo "nutiko klaida: ".$e->getMessage()."\n";
}