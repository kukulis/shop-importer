<?php
require __DIR__ . '/../../vendor/autoload.php';
ini_set("error_reporting", "-1");
$downloadImagesCommand = Kernel::instance()->getDownloadImagesCommands();
try {
    return $downloadImagesCommand->run($argv);
} catch (\Exception $e) {
    echo "nutiko klaida: " . $e->getMessage() . "\n";
    echo "Trace: " . $e->getTraceAsString();
}