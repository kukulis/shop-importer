<?php
$workAround = true;

if (!$workAround) {
    $adminDir = getenv('ADMIN_DIR');
    $prestaReindexToken = getenv('PRESTA_REINDEX_TOKEN');
    $shopBasePath = getenv('SHOP_BASE_PATH');

    // echo negalima daryti, nes prestashopas kažkurioj vietoj pasigeneruoja klaidą, nes tipo dirba su http requestu
    // ir responsas prestashopo core inicializacijos metu turi būti tuščias
//echo "adminDir=$adminDir\n";
//echo "prestaReindexToken=$prestaReindexToken\n";
//echo "shopBasePath=$shopBasePath\n";

    $_GET['full'] = 1;
    $_GET['id_shop'] = 1;
    $_GET['token'] = $prestaReindexToken;

    include $shopBasePath . DIRECTORY_SEPARATOR . $adminDir . DIRECTORY_SEPARATOR . 'searchcron.php';
}
else {

//  apeinam visą tą prestashopo nesąmonę
// Tiesiai kviečiam klasę

// šitas failas bus nukopijuotas į prestashop home
    require __DIR__ . '/config/config.inc.php';
    require_once __DIR__ . '/autoload.php';

// kad matyti progresą į failą Search.php esantį prestashope  classes/Search.php
// maždaug 720 eilutėje
// po while (($products = Search::getProductsToIndex($total_languages, $id_product, 50, $weight_array)) && (count($products) > 0)) {
// įmesti šitą eilutę
//            echo sprintf("search cycle count = %s\n", $countCycles++);
// o prieš ciklą
// $countCycles=0;

    $before = microtime(true);
    echo "Go make your cup of tea, or better two ... \n";
    \SearchCore::indexation(1);

    $after = microtime(true);

    $duration = $after - $before;

    echo "The indexation took $duration seconds\n";
}
