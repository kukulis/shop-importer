#!/usr/bin/env bash
# leidžiama iš ezplatform direktorijos

set -e

# mysql prisijungimo parametrai yra .my.cnf faile
. bin/settings.sh

# pirma išvalom sąryšines tarpines lenteles
mysql < sql/scripts/clear_tmp_relations.sql

# taip pat išvalom kategorijų tarpinę lentelę
mysql < sql/scripts/_00_deactivate_tmp_categories.sql

#for LOCALE in "${LOCALES[@]}"
#do
#output="tmp_products_$LOCALE.sql"
## quiet opcija neleidžia warningams eiti į error stream'ą, bet warningas toliau eis į output streamą
#bin/console --quiet  --env=$SYMFONY_ENV sketis:shopapi:import_tmp_products $TOKEN $LOCALE $output
#echo "feeding sql to database " $LOCALE
#mysql < $output
#done


for LOCALE in "${LOCALES[@]}"
do
echo "importing categories from provider"
catoutput="tmp_categories_$LOCALE.sql"
php bin/commands/import_categories.php $catoutput $LOCALE
echo "importing categories done";

echo "feeding categories sql"
mysql < $catoutput
echo "feeding categories sql done"


echo "importing products from provider, locale: $LOCALE"
output="tmp_products_$LOCALE.sql"
php bin/commands/import_products.php $output $LOCALE
echo "importing products done"

echo "feeding products sql, locale: $LOCALE"
mysql < $output
echo "feeding products done"
done







# šito gal nereikia, galim tik jeigu yra pakeitimų prekėje, nurezetinti indeksą atskiroms prekėms
#mysql < sql/00_zero_indexed_products.sql

bash scripts/_tmp_to_presta_sqls.sh

# generuojam kategorijų medžio left/right parametrus
echo "generating categories tree (right left attributes)"
php bin/commands/build_categories_tree.php
echo "generating categories tree done"

echo "reindexing presta"
bash scripts/reindex_presta.sh