#!/usr/bin/env bash

set -e

# mysql prisijungimo parametrai yra .my.cnf faile
. bin/settings.sh

php bin/commands/import_images.php tmp_images.sql

mysql < tmp_images.sql

bash scripts/_tmp_images_sqls.sh

php bin/commands/calculate_images_paths.php

php bin/commands/download_images.php - $SHOP_BASE_PATH

