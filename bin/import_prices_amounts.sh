#!/usr/bin/env bash

set -e

# mysql prisijungimo parametrai yra .my.cnf faile
. bin/settings.sh

php bin/commands/import_prices_and_amounts.php amounts.sql

mysql < sql/scripts/00_zero_tmp_amounts.sql

mysql < amounts.sql

if [ -z ${ZERO_PRODUCTS_DISPLAY+x} ]; then export ZERO_PRODUCTS_DISPLAY='none'; fi
echo "set @ZERO_PRODUCTS_DISPLAY='$ZERO_PRODUCTS_DISPLAY';" | cat - sql/scripts/tmp_to_prices_amounts.sql | mysql