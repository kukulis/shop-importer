# Elko - to Presta importer

Pradžioje bus Command line programa. Vėliau padarysim presta modulį.


## instaliavimas


### klonuoti projektą
`` git clone git@gitlab.com:kukulis/shop-importer.git ``

### suinstaliuoti php bibliotekas per  composer
Parsisiunčiam composer iš https://getcomposer.org/download/

pvz.:

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"

atsiras failas composer.phar. Kopijuojam failą į projekto root dirą ( jeigu dar ne ten jis yra)

Tada vykdom: 
`` php composer.phar install``

Alternatyva: susiinstaliuoti composer į distribuciją. Pvz. ubuntu taip: 
`` # apt install composer ``


###  konfigūruojam mysql klientą
tam užpildom .my.cnf failą vartotojo direktorijoje:

Mano .my.cnf failo pavyzdys:

    [client]
    password="tinklasshop"
    database="tinklasshop"
    user="tinklasshop"


### sudiegti pradinius mysql struktūros pakeitimus:

Einam į projekto dir, ir vykdom komandą:

``  mysql < sql/structure/whole_whole.sql ``

### išvalyti duombazę ( pasirinktinai, gal veiktų ir be db išvalymo )

`` mysql < sql/structure/delete_old_data.sql  ``

### paruošti .env failą
nukopijuojam iš pavyzdžio:

`` cp .env.dist .env ``

Tada redaguojam .env failą ir supildom reikšmes prisijungimui prie db ir prie Elko/(kito provaiderio) api.  

### paruošiam bash konfigūraciją

`` cp bin/settings.sh.example bin/settings.sh ``

redaguojam bin/settings.sh failą ir supildom reikšmes.


PRESTA_REINDEX_TOKEN reikšmę galima pažiūrėti prisijungus per naršyklę prie parduotuvės admin dalies, tada eiti:

Parduotuvės nustatymai -> paieška

Lange "indeksavimas" rasit tokią nuorodą:


    Naudodamiesi šia nuoroda galite nustatyti periodinį automatinį perindeksavimą:
    https://tinklas-shop.dv/admin070j1xbts/index.php?controller=AdminSearch&action=searchCron&ajax=1&full=1&token=kpeGYdWs&id_shop=1

Žr. nuorodoje parametrą "token=kpeGYdWs"


## importavimas

Leidžiam skriptą:

bin/import.sh

**Pastaba** šiuo metu apribota iki 6 prekių ( 3 žingsniai po 2 prekes ). Taip padaryta, dėl developinimo greičio.
Be šių apribojimų pilną prekių sąrašą (apie 15k prekių ) suimportuoja per 30 minučių.

Apribojimus nuimti galima kode: klasėje  
ImportElkoDataService

reikia STEP padidinti iki 100 (eilutėje nr 27), o $debug priskirti false ( 114 eilutėje ).

**Pastaba 2** jeigu nepasikeičia išvaizda po importo, reiškia neveikia reindeksavimas, arba (ir) reikia paleisti kešo išvalymo skriptą.
Reindeksavimą galima rankiniu būdu paleisti per shop adminą einant į Parduotuvės Nustatymai -> paieška.

Prestashop kešas valomas:

    cd /var/www/prestashop
    php bin/console cache:clear
    
## patarimai dėl shopo konfigūracijos

### sukurti php fpm poolą, su einamojo vartotojo teisėmis

sudo bash 
(arba lxc exec ...)
 
cd /etc/php/7.3/fpm/pool.d/

cp www.conf jonas.conf

redaguoti jonas.conf ir pakeisti šias vietas:

    [www] -> [jonas]

eilutes:

    user = www-data
    group = www-data

keičiam į

    user = jonas
    group = ubuntu

Soketo pavadinimas

    listen = /run/php/php7.3-fpm.sock

keičiam į

    listen = /run/php/php7.3-fpm-jonas.sock
    
    
Tada redaguojam nginx konfigūraciją:


    vim /etc/nginx/sites-available/tinklas.eu
    
`` fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;``

pakeičiam į 

`` fastcgi_pass unix:/var/run/php/php7.3-fpm-jonas.sock;``

restartuojam fpm'ą

    
`` # /etc/init.d/php7.3-fpm restart``

Reloadinam nginx

`` service nginx reload ``

Jeigu dar buvo shopas owneris www-data, pakeičiam į uzerio ownerį ir grupę:

    cd /var/www/html
    choown jonas:ubuntu -R prestashop


Ką tai duoda.
Tai duoda tai, kad tu su parduotuvės uzeriu gali eiti į shopo direktoriją ir valyti keshą be teisių perdėliojimo.

``php bin/console cache:clear``


## github tokenai
Faile ~/.config/composer/auth.json

"http-basic": {
"github.com": {
"username": "[YOUR-GITHUB-USERNAME]",
"password": "ghp_[YOUR-PERSONAL-TOKEN]"
}
}

Vietoj:

"github-oauth": {
"github.com": "ghp_[YOUR-PERSONAL-TOKEN]"
}


