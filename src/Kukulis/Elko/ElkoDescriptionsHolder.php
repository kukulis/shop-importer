<?php

namespace Kukulis\Elko;

use Kukulis\Elko\Data\DescriptionCriteria;

class ElkoDescriptionsHolder
{
    const CRITERIA_HOT_SWAP = '3.5" SAS Hot-swap';
    const CRITERIA_DESCRIPTION = 'Description';
    const CRITERIA_FULL_DESCRIPTION_LINE = 'Full Description Line';
    const CRITERIA_HDD_CONFIGURATION = 'HDD configuration';
    const CRITERIA_MINISAS_EXT = 'MiniSAS, ext';
    const CRITERIA_RACK_4U = 'Rack 4U';
    const CRITERIA_RJ45 = 'RJ45';
    const CRITERIA_SUPPORTED_DRIVES = 'Supported drives';
    const CRITERIA_UNIT_BRUTTO_VOLUME = 'Unit Brutto Volume';
    const CRITERIA_UNIT_GROS_WIGHT = 'Unit Gross Weight';
    const CRITERIA_UNIT_NET_WEIGHT = 'Unit Net Weight';
    const CRITERIA_VENDOR_HOME_PAGE = 'Vendor Homepage';


    const ALL_CRITERIAS = [
        self::CRITERIA_HOT_SWAP,
        self::CRITERIA_DESCRIPTION,
        self::CRITERIA_FULL_DESCRIPTION_LINE,
        self::CRITERIA_HDD_CONFIGURATION,
        self::CRITERIA_MINISAS_EXT,
        self::CRITERIA_RACK_4U,
        self::CRITERIA_RJ45,
        self::CRITERIA_SUPPORTED_DRIVES,
        self::CRITERIA_UNIT_BRUTTO_VOLUME,
        self::CRITERIA_UNIT_GROS_WIGHT,
        self::CRITERIA_UNIT_NET_WEIGHT,
        self::CRITERIA_VENDOR_HOME_PAGE,
    ];

    const CRITERIA_ID_HOT_SWAP = 2034;
    const CRITERIA_ID_DESCRIPTION = 419;
    const CRITERIA_ID_FULL_DESCRIPTION_LINE = 999;
    const CRITERIA_ID_HDD_CONFIGURATION = 2157;
    const CRITERIA_ID_MINISAS_EXT = 1827;
    const CRITERIA_ID_RACK_4U = 1224;
    const CRITERIA_ID_RJ45 = 1309;
    const CRITERIA_ID_SUPPORTED_DRIVES = 1854;
    const CRITERIA_ID_UNIT_BRUTTO_VOLUME = 1449;
    const CRITERIA_ID_UNIT_GROS_WIGHT = 790;
    const CRITERIA_ID_UNIT_NET_WEIGHT = 791;
    const CRITERIA_ID_VENDOR_HOME_PAGE = 426;

    const ALL_CRITERIAS_IDS = [
        self::CRITERIA_ID_HOT_SWAP,
        self::CRITERIA_ID_DESCRIPTION,
        self::CRITERIA_ID_FULL_DESCRIPTION_LINE,
        self::CRITERIA_ID_HDD_CONFIGURATION,
        self::CRITERIA_ID_MINISAS_EXT,
        self::CRITERIA_ID_RACK_4U,
        self::CRITERIA_ID_RJ45,
        self::CRITERIA_ID_SUPPORTED_DRIVES,
        self::CRITERIA_ID_UNIT_BRUTTO_VOLUME,
        self::CRITERIA_ID_UNIT_GROS_WIGHT,
        self::CRITERIA_ID_UNIT_NET_WEIGHT,
        self::CRITERIA_ID_VENDOR_HOME_PAGE,
    ];

    /** @var DescriptionCriteria */
    private static $nullElement = null;


    private $productId;

    /** @var DescriptionCriteria[] */
    private $map = [];

    /** @var DescriptionCriteria[] */
    private $idMap = [];

    /**
     * ElkoDescriptionsHolder constructor.
     * @param $productId
     */
    public function __construct($productId)
    {
        $this->productId = $productId;
        if (static::$nullElement == null) {
            static::$nullElement = new DescriptionCriteria();
        }
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param $criteria
     * @return DescriptionCriteria
     */
    public function getByCriteria($criteria)
    {
        if (!array_key_exists($criteria, $this->map)) {
            return static::$nullElement;
        }
        return $this->map[$criteria];
    }

    /**
     * @param $criteriaId
     * @return mixed|null
     */
    public function getByCriteriaId($criteriaId)
    {
        if (!array_key_exists($criteriaId, $this->idMap)) {
            return static::$nullElement;
        }
        return $this->idMap[$criteriaId];
    }

    /**
     * @param DescriptionCriteria $element
     */
    public function add($element)
    {
        $this->map[$element->criteria] = $element;
        $this->idMap[$element->criteriaId] = $element;
    }

    /**
     * @return array
     */
    public function getAllCriterias()
    {
        return array_keys($this->map);
    }

    /**
     * @return array
     */
    public function getAllCriteriasIds()
    {
        return array_keys($this->idMap);
    }

    // specific
    public function getHOT_SWAP(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_HOT_SWAP);
        } else {
            return $this->getByCriteria(self::CRITERIA_HOT_SWAP);
        }
    }

    public function getDESCRIPTION(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_DESCRIPTION);
        } else {
            return $this->getByCriteria(self::CRITERIA_DESCRIPTION);
        }
    }

    public function getFULL_DESCRIPTION_LINE(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_FULL_DESCRIPTION_LINE);
        } else {
            return $this->getByCriteria(self::CRITERIA_FULL_DESCRIPTION_LINE);
        }
    }

    public function getHDD_CONFIGURATION(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_HDD_CONFIGURATION);
        } else {
            return $this->getByCriteria(self::CRITERIA_HDD_CONFIGURATION);
        }
    }

    public function getMINISAS_EXT(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_MINISAS_EXT);
        } else {
            return $this->getByCriteria(self::CRITERIA_MINISAS_EXT);
        }
    }

    public function getRACK_4U(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_RACK_4U);
        } else {
            return $this->getByCriteria(self::CRITERIA_RACK_4U);
        }
    }

    public function getRJ45(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_RJ45);
        } else {
            return $this->getByCriteria(self::CRITERIA_RJ45);
        }
    }

    public function getSUPPORTED_DRIVES(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_SUPPORTED_DRIVES);
        } else {
            return $this->getByCriteria(self::CRITERIA_SUPPORTED_DRIVES);
        }
    }

    public function getUNIT_BRUTTO_VOLUME(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_UNIT_BRUTTO_VOLUME);
        } else {
            return $this->getByCriteria(self::CRITERIA_UNIT_BRUTTO_VOLUME);
        }
    }

    public function getUNIT_GROS_WIGHT(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_UNIT_GROS_WIGHT);
        } else {
            return $this->getByCriteria(self::CRITERIA_UNIT_GROS_WIGHT);
        }
    }

    public function getUNIT_NET_WEIGHT(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_UNIT_NET_WEIGHT);
        } else {
            return $this->getByCriteria(self::CRITERIA_UNIT_NET_WEIGHT);
        }
    }

    public function getVENDOR_HOME_PAGE(bool $byId = true)
    {
        if ($byId) {
            return $this->getByCriteriaId(self::CRITERIA_ID_VENDOR_HOME_PAGE);
        } else {
            return $this->getByCriteria(self::CRITERIA_VENDOR_HOME_PAGE);
        }
    }
}