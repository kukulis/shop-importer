<?php


namespace Kukulis\Elko;


class TitleTransformer
{
    public function transform($sourceTitle) {

        if ( !preg_match('/^(\w+)(.*?)(\w+)\s*$/', $sourceTitle, $matches)) {
            // returning the same
            return $sourceTitle;
        }

        list($whole,$firstWord,$middle,$lastWord) = $matches;

        return trim($middle).' '.trim($firstWord);
    }
}