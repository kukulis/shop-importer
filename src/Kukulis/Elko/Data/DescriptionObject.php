<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.4.17
 * Time: 09.31
 */

namespace Kukulis\Elko\Data;


class DescriptionObject
{
    /** @var int */
    public $productId;

    /** @var DescriptionCriteria[] */
    public $description=[];
}