<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.4.17
 * Time: 09.55
 */

namespace Kukulis\Elko\Data;


class MediaFile
{
    public $mediaType;     //: 1,
    public $link;          // : "https://static.elkogroup.com/ProductImages/73b9a296-73af-4e1c-8814-8fb452555acb.Jpeg",
    public $objectId;      // : 517419,
    public $sequence;      // : 1
}