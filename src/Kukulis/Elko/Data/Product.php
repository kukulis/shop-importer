<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.4.17
 * Time: 09.49
 */

namespace Kukulis\Elko\Data;


class Product
{
    public $id;                 // : 1303388,
    public $elkoCode;           // : 1303388,
    public $name;               // : "MOUSE PAD CLOTH RUBBER/BLACK MP-S-BK GEMBIRD",
    public $manufacturerCode;   // : "MP-S-BK",
    public $vendorName;         // : "GEMBIRD",
    public $vendorCode;         // : "G2",
    public $catalog;            // : "MOP", // category
    public $quantity;           // : "0",
    public $price;              // : 0.27,
    public $discountPrice;      // : 0.27,
    public $imagePath;          // : "https://static.elkogroup.com/ProductImages/8e88f3cd-e0d7-4458-abd6-95771866ab09.Jpeg",
    public $thumbnailImagePath; // : "https://static.elkogroup.com/ProductImagesThumbs/8e88f3cd-e0d7-4458-abd6-95771866ab09.Jpeg",
    public $fullDsc;            // : "",
    public $currency;           // : "EUR",
    public $httpDescription;    // : "https://ecom.elkogroup.com/Catalog/Product/1303388",
    public $packagingQuantity;  // : 1,
    public $warranty;           // : "24",
    public $eanCode;            // : "8716309111782",
    public $obligatoryKit;      // : 0,
    public $reservedQuantity;   // : 0,
    public $promDate;           // : 0,
    public $promQuant;          // : 0,
    public $quantityForPrice2;  // : "0",
    public $price2;             // : 0.0,
    public $lotNumber;          // : "",
    public $copyrightTax;       // : 0.0,
    public $incomingQuantity;   // : 0
}