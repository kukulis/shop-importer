<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.4.17
 * Time: 09.29
 */

namespace Kukulis\Elko\Data;


class DescriptionCriteria
{
    public $criteria;           // : "Description",
    public $measurement;        // : null,
    public $comlexName;         // : null,
    public $value;              // : "SEAGATE Exos X 5U84 RAID system. Total available RAW capacity 1344TB with 84 of 84 (3.5&quot;) drive bays populated.&nbsp;<br />\n320k IOPS<br />\n7GB/s sequential read troughput<br />\n5.5G/b sequential write troughput<br />\nThin Provisioning,&nbsp;SSD Read Cashe,&nbsp;Real-time Auto Tiering, Snapshots, Asynchronous Replications,&nbsp;ADAPT<br />\n<br />\nDrives: 84x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos<br />\n2x SFP+ 4-pack for 10Gb iSCSI<br />\n5x Hot-Swap Fan Cooling Modules<br />\n2x High-Efficiency 2.2kW Redundant Power Supplies<br />\nRail Kit<br />\n2x Bezels for each Drawer<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; FC or iSCSI (SFP+) (FC - 16Gb/s, 8Gb/s; iSCSI - 10Gb/s, 1Gb/s)<br />\n- 1 x4 mini-SAS HD connector for expansion (up to 10 2U (total 240 drives) or up to 4 5U enclousures (336 drives)<br />\n- 1 RJ45 port per IOM<br />\n- 4 Serial Ports<br />\n- 1 Mini-USB<br />\n- 16GB cache<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
    public $orderNo;            // : null,
    public $criteriaId;         // : 419,
    public $lastUpdateDate;     // : "0001-01-01T00:00:00"
}