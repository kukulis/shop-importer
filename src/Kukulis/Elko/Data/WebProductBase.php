<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.4.15
 * Time: 22.31
 */

namespace Kukulis\Elko\Data;


class WebProductBase
{
    public $id;                  // : 1166012,
    public $vendor;              // : "GENWAY",
    public $name;                // : "CABLE ACC JACK RJ45/WTYKRJ45 GENWAY",
    public $description;         // : "",
    public $catalogCode;         // : "CBL",
    public $manufacturerCode;    // : "WTYKRJ45",
    public $pictureUrl;          // : "https://static.elkogroup.com/ProductImages/10414.Jpeg"

}