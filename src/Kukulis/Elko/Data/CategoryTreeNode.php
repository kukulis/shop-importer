<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.5.3
 * Time: 18.48
 */

namespace Kukulis\Elko\Data;


class CategoryTreeNode
{
    /** @var string */
    public $code;

    /** @var string */
    public $name;

    /** @var CategoryTreeNode[] */
    public $childs=[];
}