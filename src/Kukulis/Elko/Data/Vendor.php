<?php

namespace Kukulis\Elko\Data;


class Vendor
{
    /** @var string */
    public $name;

    /** @var  string */
    public $code;

    /** @var  boolean */
    public $canEdit=true;

    /** @var  string] */
    public $type;
}
