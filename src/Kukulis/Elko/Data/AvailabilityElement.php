<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.8.15
 * Time: 19.36
 */

namespace Kukulis\Elko\Data;


class AvailabilityElement
{
    public $productId; //  1301630,
    public $stockQuantity; // "0",
    public $incoming; // null,
    /** @var PriceObject */
    public $prices;
}