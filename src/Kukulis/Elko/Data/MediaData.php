<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.4.17
 * Time: 19.13
 */

namespace Kukulis\Elko\Data;


class MediaData
{
    /** @var int */
    public $id;

    /** @var MediaFile[]  */
    public $mediaFiles=[];
}