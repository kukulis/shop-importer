<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.8.15
 * Time: 19.37
 */

namespace Kukulis\Elko\Data;


class PriceObject
{
    public $currency; // "EUR",
    public $price; // 61214.18,
    public $discountPrice; // 61214.18,
    public $copyrightTax; //  0.0
}