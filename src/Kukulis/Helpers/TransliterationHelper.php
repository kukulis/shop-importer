<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.17
 * Time: 09.39
 */

namespace Kukulis\Helpers;


class TransliterationHelper
{
    public static function transliterate($str)
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    }

    public static function nonAlphaNumericToConnectors($str, $connector = '_')
    {
        return preg_replace('/[^[:alnum:]]+/', $connector, $str);
    }

    public static function toCode(string $str, $connector = '-'): string
    {
        return self::nonAlphaNumericToConnectors(strtoupper(self::transliterate($str)), $connector);
    }
}