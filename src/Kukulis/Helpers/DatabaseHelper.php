<?php
/**
 * DatabaseHelper.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.7
 * Time: 15.01
 */

namespace Kukulis\Helpers;


use Kukulis\Data\IRowObject;

class DatabaseHelper
{
    const AUTOINCREMENTID='@Autoincrementid';
    const NODB='@Nodb';
    const RESERVEDS = ['condition', 'table'];

    public function generateInsertSql ( IRowObject $rowobject, $tableName, $ignore='',  \Closure $quoter=null ) {
        $ro = new \ReflectionObject($rowobject);
        $properties = $ro->getProperties();
        $columns = [];
        $values=[];

        $idcolumns=[];
        $idvalues=[];
        foreach ( $properties as $p ) {
            $comm = $p->getDocComment();

            if ( strpos( $comm,self::NODB) !== false ) {
                continue;
            }

            if ( strpos( $comm,self::AUTOINCREMENTID) !== false ) {
                // identifikatorius, praleidžiam
                $idcolumns[] = $p->getName();
                $idvalues[]= $p->getValue($rowobject);
                continue;
            }

            $columns[] = $p->getName();
            $values[] = $p->getValue($rowobject);
        }

        if ( $quoter==null) {
            $quoter = function($str) {
                if ( $str === null ) {
                    return 'null';
                }
                return '\''.$str.'\'';
            };
        }

        $qValues = array_map ( $quoter, $values );
        $valuesStr = join (  ",\n", $qValues);

        $qColumns = array_map ([$this, 'quoteColumnName'], $columns);
        $columnsStr = join ( ",\n", $qColumns);

        $sql = "INSERT $ignore INTO $tableName ($columnsStr)
                VALUES ($valuesStr)";

        return $sql;
    }


    public function quoteColumnName($col) {
        if ( array_search($col, self::RESERVEDS) !== false ) {
            return '`'.$col.'`';
        }

        return $col;
    }
}