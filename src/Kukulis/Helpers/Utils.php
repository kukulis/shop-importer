<?php
/**
 * Utils.php
 * Created by Giedrius Tumelis.
 * Date: 17.7.26
 * Time: 12.15
 */

namespace Kukulis\Helpers;


use DateTime;
use DateTimeZone;
use Exception;
use Kukulis\Presta\Exception\SystemException;
use Kukulis\Presta\Exception\ValidationException;
use stdClass;

class Utils
{
    const SEO_MAX_LENGHT = 200;
    const LAST_SECOND = '23:59:59';
    const FIRST_SECOND = '00:00:00';

    // http://www.infolex.lt/ta/368200:str123
    const LITHUANIAN_HOLIDAYS = [
        '01-01', //  1) sausio 1-ąją – Naujųjų metų dieną;
        '02-16',  // 2) 16-ąją – Lietuvos valstybės atkūrimo dieną;
        '03-11',  // 3) kovo 11-ąją – Lietuvos nepriklausomybės atkūrimo dieną;
        // šitą praleidžiam ne sekmadienį ok, o pirmadienis kilnojamas gaunasi   // 4) sekmadienį ir pirmadienį – krikščionių Velykų (pagal vakarietiškąją tradiciją) dienomis;
        '05-01',  // 5) gegužės 1-ąją – Tarptautinę darbo dieną;
        // ir taip sekmadienis   // 6) pirmąjį gegužės sekmadienį – Motinos dieną;
        // ir taip sekmadienis 7) pirmąjį birželio sekmadienį – Tėvo dieną;
        '06-24', // 8) birželio 24-ąją – Rasos ir Joninių dieną;
        '07-06', // 9) liepos 6-ąją – Valstybės (Lietuvos karaliaus Mindaugo karūnavimo) ir Tautiškos giesmės dieną;
        '08-15', // 10) rugpjūčio 15-ąją – Žolinę (Švč. Mergelės Marijos ėmimo į dangų dieną);
        '11-01', // 11) lapkričio 1-ąją – Visų Šventųjų dieną;
        '11-01', // 12) lapkričio 2-ąją – Mirusiųjų atminimo (Vėlinių) dieną;
        '12-24', // 13) gruodžio 24-ąją – Kūčių dieną;
        '12-25', // 14) gruodžio 25-ąją ir 26-ąją – Kalėdų dienomis.
    ];

    const EASTERS2 = [ // pirmadieniai po velykų
        '2021-04-05', // https://lk.katalikai.lt/2021/04
        '2022-04-18', // https://lk.katalikai.lt/2022/04
        '2023-04-10', // https://lk.katalikai.lt/2023/04
        '2024-04-01', // https://lk.katalikai.lt/2024/04
        '2025-04-21', // https://lk.katalikai.lt/2025/04
    ];

    /**
     * Array of letters transformation to latin
     * @var array
     */
    static protected $__transformationToLatin = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v',
        'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u',
        'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
        'ь' => '', 'ы' => 'y', 'ъ' => '',
        'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
        "ї" => "yi", "є" => "ye", "ą" => "a",
        "č" => "c", "ę" => "e", "ė" => "e",
        "į" => "i", "š" => "s", "ų" => "u",
        "ū" => "u", "ž" => "z",

        'А' => 'A', 'Б' => 'B', 'В' => 'V',
        'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
        'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => 'Y', 'К' => 'K',
        'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U',
        'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
        'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
        'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        "Ї" => "yi", "Є" => "ye", "Ą" => "A",
        "Č" => "C", "Ę" => "E", "Ė" => "E",
        "Į" => "I", "Š" => "S", "Ų" => "U",
        "Ū" => "U", "Ž" => "Z",
    );


    public static function get_url_ascii($url)
    {
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $url);
        $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_| -]+/", '-', $clean);
        return $clean;
    }


    /**
     * Changes colons to dots.
     *
     * @param $str
     * @return mixed
     */
    public static function dots($str)
    {
        return str_replace(',', '.', $str);
    }

    /**
     * Is paduotos frazes sugeneruoja nuorodai skirta fraze
     *
     * @param string $link linko formavimo fraze
     * @param bool $lowercase jei true nuoroda paverciama i mazasias raides
     * @param integer $maxLength koks maksimalus ilgis frazes
     * @return string
     */
    public static function generateSeoLink($link, $lowercase = TRUE, $maxLength = self::SEO_MAX_LENGHT)
    {
        if (is_array($link)) $link = join(' ', $link);
        // Jei i ka pakeisti, tada paliekame tik reikiamus simbolius is frazes
        if (!count(self::$__transformationToLatin)) {
            $link = trim(strip_tags($link));
            return preg_replace("/[^a-z0-9\_\-]+/mi", '', $link);
        }//if
        $link = trim(strip_tags($link));
        $link = preg_replace("/\s+/ms", '-', $link);
        $link = str_replace("/", "-", $link);
        //Paverziam i lotyniskas raides
        $link = strtr($link, self::$__transformationToLatin);
        // Jei true tada isvalome ir taskus
        //if($dot) $link=preg_replace("/[^a-z0-9\_\-.]+/mi","",$link);
        $link = preg_replace("/[^a-z0-9\_\-]+/mi", '', $link);
        //Pakeiciam atitinkamus simbolius i bruksnelius
        $link = preg_replace('#[\-]+#i', '-', $link);
        //Jei true tada paverciam mazosomis
        if ($lowercase) $link = mb_strtolower($link);

        //Isvalome php extensionus del apache
        $link = str_replace('.php', '', $link);
        //Pakartotinai valome tada pakeiciame i ppp kad nereiketu rekusiskai valyti php
        $link = str_replace('.php', '.ppp', $link);
        if (mb_strlen($link) > $maxLength) {
            $link = mb_substr($link, 0, $maxLength);
            if (($tempMax = mb_strrpos($link, '-'))) $link = mb_substr($link, 0, $tempMax);
        }//if
        return $link;
    }

    /**
     * Function copied from ecommerce.gs common.2.12.16.lib
     * @param int $l
     * @return string
     */
    public static function phash($l = 10)
    {
        $s = '';
        for ($i = 0; $i < $l; $i++) {
            $r = @rand(48, 120);
            while (($r >= 58 && $r <= 64) || ($r >= 91 && $r <= 96))
                $r = @rand(48, 120);
            $s .= chr($r);
        } // for
        return $s;
    }

    /**
     * Tinka tik kai laukai "public"
     * @param mixed $from
     * @param mixed $to
     * @param array $exclude
     */
    public static function copyProperties($from, $to, $exclude)
    {
        foreach ($from as $property => $value) {
            if (array_search($property, $exclude) !== false)
                continue;

            $to->$property = $value;
        }
    }

    /**
     * @param $amount
     * @param $value
     * @return array
     */
    public static function builArray($amount, $value)
    {
        $arr = [];
        for ($i = 0; $i < $amount; $i++)
            $arr[] = $value;

        return $arr;
    }

    public static function quote($val)
    {
        return '\'' . $val . '\'';
    }

    public static function isNumericalString($val)
    {
        return preg_match("/^\d+$/", $val);
    }

    public static function appendAssocArray(&$modifiedArray, $appendable, $override = true)
    {
        foreach ($appendable as $key => $value) {
            if ($override || !isset($modifiedArray[$key])) {
                $modifiedArray[$key] = $value;
            }
        }
    }

    public static function sliceAssocArray($array, $amount)
    {
        $count = 0;
        $rez = [];
        foreach ($array as $key => $value) {
            if ($count >= $amount)
                break;
            $rez[$key] = $value;
            $count++;
        }
        return $rez;
    }

    /**
     * @param $length
     * @param string $keyspace
     * @return string
     * @throws Exception
     */
    public static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    /**
     * @return string
     * @throws SystemException
     */
    public static function newId()
    {
        try {
            return self::random_str(32, '0123456789abcdef');
        } catch (Exception $e) {
            throw new SystemException($e->getMessage());
        }
    }

    public static function sk_to_lt($l)
    {

        $sk1000[0] = "tūkstantis ";
        $sk1000[1] = "tūkstančiai ";
        $sk1000[2] = "tūkstančių ";

        $sk1000000[0] = "milijonas ";
        $sk1000000[1] = "milijonai ";
        $sk1000000[2] = "milijonų ";

        $minus = "minus ";
        $skaiz = "";

        if ($l < 0) {
            $skaiz = $skaiz . $minus;
            $l1 = $l * (-1);
        } else $l1 = $l;

// skaidom skaiciu
        $lv = $l1;

        settype($lv, "integer");
        $ld = [];
        for ($i = 8; $i >= 0; $i--) {
            $ls = (integer)($lv / 10);
            $ll = $lv % 10;
            $ld[$i] = $ll;
            $lv = $ls;

        }
// rasom zodzius
        $skaiz = $skaiz . self::simtai($ld[0], $ld[1], $ld[2]);

// milijonai
        if ($ld[1] <> 1 and $ld[2] == 1)
            $skaiz = $skaiz . $sk1000000[0];
        if ($ld[1] <> 1 and $ld[2] <> 1 and $ld[2] <> 0)
            $skaiz = $skaiz . $sk1000000[1];
        if ($ld[1] == 1 and $ld[2] <> 0)
            $skaiz = $skaiz . $sk1000000[2];
        if (($ld[0] <> 0 or $ld[1] <> 0) and $ld[2] == 0)
            $skaiz = $skaiz . $sk1000000[2];

        $skaiz = $skaiz . self::simtai($ld[3], $ld[4], $ld[5]);

// tukstanciai
        if ($ld[4] <> 1 and $ld[5] == 1)
            $skaiz = $skaiz . $sk1000[0];
        if ($ld[4] <> 1 and $ld[5] <> 1 and $ld[5] <> 0)
            $skaiz = $skaiz . $sk1000[1];
        if ($ld[4] == 1 and $ld[5] <> 0)
            $skaiz = $skaiz . $sk1000[2];
        if (($ld[3] <> 0 or $ld[4] <> 0) and $ld[5] == 0)
            $skaiz = $skaiz . $sk1000[2];

        $skaiz = $skaiz . self::simtai($ld[6], $ld[7], $ld[8]);

        return ucfirst($skaiz);
    }

    public static function simtai($s, $d, $v)
    {
        $sk1[0] = "vienas ";
        $sk1[1] = "du ";
        $sk1[2] = "trys ";
        $sk1[3] = "keturi ";
        $sk1[4] = "penki ";
        $sk1[5] = "šeši ";
        $sk1[6] = "septyni ";
        $sk1[7] = "aštuoni ";
        $sk1[8] = "devyni ";
        $sk1[9] = "dešimt ";
        $sk11[0] = "vienuolika ";
        $sk11[1] = "dvylika ";
        $sk11[2] = "trylika ";
        $sk11[3] = "keturiolika ";
        $sk11[4] = "penkiolika ";
        $sk11[5] = "šešiolika ";
        $sk11[6] = "septyniolika ";
        $sk11[7] = "aštuoniolika ";
        $sk11[8] = "devyniolika ";
        $sk11[9] = "dvidešimt ";
        $sk10[0] = "dešimt ";
        $sk10[1] = "dvidešimt ";
        $sk10[2] = "trisdešimt ";
        $sk10[3] = "keturiasdešimt ";
        $sk10[4] = "penkiasdešimt ";
        $sk10[5] = "šešiasdešimt ";
        $sk10[6] = "septyniasdešimt ";
        $sk10[7] = "aštuoniasdešimt ";
        $sk10[8] = "devyniasdešimt ";
        $sk10[9] = "šimtas ";
        $sk100[0] = "šimtas ";
        $sk100[1] = "šimtai ";

        $simtz = "";

        if ($s <> 0) {
            $simtz = $simtz . $sk1[$s - 1];
            if ($s == 1)
                $simtz = $simtz . $sk100[0];
            else
                $simtz = $simtz . $sk100[1];
        }

        if ($d <> 0) {
            if ($d <> 1 or $v == 0)
                $simtz = $simtz . $sk10[$d - 1];
        }

        if ($v <> 0) {
            if ($d == 1)
                $simtz = $simtz . $sk11[$v - 1];
            else
                $simtz = $simtz . $sk1[$v - 1];
        }
        return $simtz;
    }

    /**
     * @param $l
     * @return string
     */
    public static function currencyToLt($l)
    {
        $fraction = floor($l * 100 - floor($l) * 100);

        return self::sk_to_lt($l) . 'eurai ' . $fraction . ' ct';
    }

    /**
     * @valArr array
     * @param $valArr
     * @return mixed
     */
    public static function isEmpty($valArr)
    {
        foreach ($valArr as $val) {
            if (!empty($val)) {
                return $val;
            }
        }
        return null;
    }

    /**
     * @param string $dateStr
     * @return DateTime
     * @throws Exception
     */
    public static function convertToDate($dateStr)
    {
        return new DateTime($dateStr, new DateTimeZone('GMT'));
    }

    public static function splitMultipleEmailField($field)
    {
        return array_filter(preg_split('/[\s,;]+/', $field), function ($x) {
            return !empty($x);
        });
    }

    public static function extractNumber($str)
    {
        $matches = [];
        preg_match('/[\s,\d]+/', $str, $matches);
        if (!empty($matches) && isset($matches[0])) {
            return str_replace(' ', '', $matches[0]);
        }
        return '';
    }

    /**
     * @param $str
     * @return float
     */
    public static function extractFloat($str)
    {
        $str = str_replace(',', '.', $str);
        $matches = [];
        preg_match('/[\.\d]+/', $str, $matches);
        if (!empty($matches) && isset($matches[0])) {
            return floatval($matches[0]);
        }
        return 0;
    }


    /**
     * @param $suma
     * @param array $matas
     * @return string
     */
    public static function parinktiMata($suma, $matas = array('', '', '', ''))
    {
        $simtai = intval($suma / 100);

        $suma -= $simtai * 100;
        $desimtys = intval($suma / 10);

        $suma -= $desimtys * 10;
        $vienetai = $suma;


        if ($desimtys != 1) {
            switch ($vienetai) {
                case 0:
                    $rezultatas = ' ' . $matas[0];

                    break;
                case 1:
                    $rezultatas = ' ' . $matas[1];
                    break;
                default:
                    $rezultatas = ' ' . $matas[2];
                    break;
            }
        } else {
            $rezultatas = ' ' . $matas[3];
        }
        return $rezultatas;
    }

    public static function ifEmpty($value, $alternative)
    {
        if (empty($value)) {
            return $alternative;
        }

        return $value;

    }

    public static function extractTexts($xmlText)
    {
        return trim(preg_replace('/<.*?\>/', '', $xmlText));
    }

    /**
     * @param $val
     * @return string
     *
     */
    public static function nullValue($val)
    {
        if ($val === null) {
            return 'null';
        }
        return $val;
    }

    public static function convertArrayToStringWithNullValues(array $array)
    {
        return join(',', array_map(function ($item) {
            return Utils::nullValue($item);
        }, $array));
    }

    /**
     * @param $val
     * @return string
     * Null value or quote
     */
    public static function nullValueOrQuote($val)
    {
        if ($val === null) {
            return 'null';
        }

        return "'" . $val . "'";
    }

    /**
     * @param $val
     * @return string
     * Null value or quote
     */
    public static function nullValueOrQuoter($val, \Closure $quoter)
    {
        if ($val === null) {
            return 'null';
        }

        return $quoter($val);
    }

    /**
     * @param $instance
     * @param string $className
     * @return mixed
     */
    public static function objectToObject($instance, string $className)
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }

    /**
     * @param $path
     */
    public static function createDirectory($path)
    {
        $dir = dirname($path);
        if (!file_exists($dir)) {
            @mkdir($dir, 0775, true);
        }
    }

    /**
     * @param $compositeOrderNumber
     * @return array pair of serie number and order number
     */
    public static function splitOrderNumber($compositeOrderNumber)
    {
        preg_match('/([\w\?]+?)_([\w\?]+)/', $compositeOrderNumber, $matches);
        if (count($matches) >= 3) {
            return [$matches[1], $matches[2]];
        } else {
            return ['', $compositeOrderNumber];
        }
    }

    /**
     * @param $obj
     * @return stdClass
     */
    public static function emptyStdClass($obj)
    {
        if (empty($obj)) {
            return new stdClass();
        } else {
            return $obj;
        }
    }

    /**
     * @param array $params
     * @param mixed $default
     * @return mixed
     */
    public static function nonEmpty($params, $default = '')
    {
        foreach ($params as $param) {
            if (!empty($param)) {
                return $param;
            }
        }
        // pagal indeksą negalima grąžinti, nes masyvas gali būti paduotas tuščias.
        return $default;
    }

    /**
     * @param array $map any map
     * @param string $joiner symbols used to join key with value
     * @param string $separator symbols used to separate different map pairs
     * @return string
     */
    public static function map2string($map, $joiner = ":", $separator = "\n")
    {
        $rezArr = [];
        foreach ($map as $key => $value) {
            $rezArr[] = $key . $joiner . $value;
        }
        return join($separator, $rezArr);
    }


    /**
     * @param array $arr
     * @param string $joint
     * @param string $separator
     * @return string
     */
    public static function assocToString($arr, $joint, $separator)
    {
        $pairs = [];
        foreach ($arr as $key => $value) {
            $pairs[] = $key . $joint . $value;
        }
        return join($separator, $pairs);
    }

    /**
     * Grąžina vertes iš map'o tokiu eiliškumu, kokiu yra raktų masyvas
     * @param array $map
     * @param string[] $keys raktų masyvas
     * @return array
     */
    public static function getMapValues($map, $keys)
    {
        $values = [];
        foreach ($keys as $key) {
            $values[] = $map[$key];
        }
        return $values;
    }

    /**
     * @param $originaloValiutosKursas
     * @param $valiutosKursas
     * @return float|int
     */
    public static function calculateValiutosKursoDaugiklis($originaloValiutosKursas, $valiutosKursas)
    {
        return $originaloValiutosKursas / $valiutosKursas;
    }

    /**
     * @param $antkainis
     * @param $valiutosKursoDaugiklis
     * @return float|int
     */
    public static function calculateAntkainioIrValiutosKursoDaugiklis($antkainis, $valiutosKursoDaugiklis)
    {
        return (1 + ($antkainis / 100)) * $valiutosKursoDaugiklis;
    }

    /**
     * pagal useriu poreikius leidziami naudoti du skyrikliai: kablelis(,) ir kabliataskis(;)
     * @param string|null $separatedValues
     * @return array
     */
    public static function uniqueValuesArrayFromSeparatedString(?string $separatedValues): array
    {
        if ($separatedValues == null) {
            return [];
        }

        $uniqueValuesArray = preg_split("/,|;/", $separatedValues);
        $uniqueValuesArray = array_map('trim', $uniqueValuesArray);
        $uniqueValuesArray = array_unique($uniqueValuesArray);

        $uniqueValuesNonEmptyArray = [];
        if (count($uniqueValuesArray) > 0) {
            foreach ($uniqueValuesArray as $key => $item) {
                if ($item != '') {
                    $uniqueValuesNonEmptyArray[] = $item;
                }
            }
        }
        return $uniqueValuesNonEmptyArray;
    }

    /**
     * @param string|null $separatedValues
     * @param string $context
     * @return string|null
     * @throws ValidationException
     */
    public static function validateOnlyNumbersAndSeparators(?string $separatedValues, string $context): ?string
    {
        if (preg_match('/^[0-9,; ]*$/', $separatedValues)) {
            return $separatedValues;
        } else {
            $errorMessage = sprintf(
                '%s turi neleistinų simbolių [%s]',
                $context,
                $separatedValues
            );
            throw new ValidationException($errorMessage);
        }
    }

    /**
     * @param string $dateFromTime
     * @param string $dateToTime
     * @param string $context
     * @throws ValidationException
     */
    public static function validateDateFromTo(string $dateFromTime, string $dateToTime, string $context)
    {
        if ($dateFromTime > $dateToTime) {
            $errorMessage = sprintf(
                '%s: data nuo [%s] negali būti didesnė nei data iki [%s]',
                $context,
                $dateFromTime,
                $dateToTime
            );
            throw new ValidationException($errorMessage);
        }
    }

    /**
     * @param $date
     * @return string
     */
    public static function getToDateLastSecondIncluded($date)
    {
        return $date . ' ' . self::LAST_SECOND;
    }

    /**
     * @param $date
     * @return string
     */
    public static function getFromDateFirstSecondIncluded($date)
    {
        return $date . ' ' . self::FIRST_SECOND;
    }


    /**
     * @param string $text
     * @return array
     */
    public static function parseTextareaString(?string $text)
    {
        if ($text == null) {
            return [];
        }
        $values = explode("\r\n", $text);
        $text = implode("\n", $values);

        $values = explode(",", $text);
        $text = implode("\n", $values);

        $values = explode(";", $text);

        $text = implode("\n", $values);

        $values = explode("\n", $text);

        $values = array_map('trim', $values);
        // remove empty elements
        $values = array_filter($values);
        // reindex
        $values = array_values($values);
        return $values;
    }

    /**
     * @param $naujasSkaicius
     * @param $skaiciusSuKuriuoLyginama
     * @return float|int
     */
    public static function procentinisSantykis($naujasSkaicius, $skaiciusSuKuriuoLyginama)
    {
        return ($naujasSkaicius - $skaiciusSuKuriuoLyginama) / $skaiciusSuKuriuoLyginama * 100;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public static function stripFirstLine($text)
    {
        return substr($text, strpos($text, "\n") + 1);
    }

    /**
     * @param string $str
     * @return string
     */
    public static function unsuffixMinusNumber($str)
    {
        $regexp = '/-\d{1}$/';
        return preg_replace($regexp, '', $str);
    }

    /**
     * @param mixed $obj
     * @param string $class
     * @return bool
     */
    public static function yra_klase($obj, $class)
    {
        return is_subclass_of($obj, $class) ||
            get_class($obj) == $class;
    }

    /**
     * @param string $code
     * @return string|null
     */
    public static function unprefixZeros($code)
    {
        return preg_replace('/^0*/', '', $code);
    }

    /**
     * Ne šiaip nuimti nulius, o pervesti į EAN8 formatą.
     *
     * @param string $code
     * @return string
     */
    public static function unprefixZerosEan8($code)
    {
        if (strlen($code) > 8) {
            $beginning = substr($code, 0, strlen($code) - 8);
            // jeigu priekis visi 0
            if (preg_match('/^0*$/', $beginning)) {
                // tai grąžiname pabaigą iš 8 simbolių
                return substr($code, strlen($code) - 8);
            }
        }
        return $code;
    }

    /**
     * @param $price
     * @return float
     */
    public static function formatCentsPrice($price)
    {
        return round($price * 100);
    }

    /**
     * @param $str
     * @return string|null
     */
    public static function extractEmail($str)
    {
        if ($str == null) {
            return null;
        }
        preg_match_all("/[._a-zA-Z0-9\-]+@[._a-zA-Z0-9\-]+/i", $str, $matches);
        if (count($matches) > 0 && count($matches[0]) > 0) {
            return $matches[0][0];
        }
        return null;
    }

    /**
     * @param mixed $obj
     * @param string $property
     * @return null
     */
    public static function getPropertyValue($obj, $property)
    {
        if (isset($obj->{$property})) {
            return $obj->{$property};
        }
        return null;
    }

    /**
     * @param $obj
     * @return array
     */
    public static function getObjectValuesMap($obj)
    {
        $map = [];
        foreach ($obj as $key => $value) {
            $map[$key] = $value;
        }
        return $map;
    }

    public static function validateCode($code)
    {
        return preg_match('/^[_A-Z]+$/', $code);
    }

    /**
     * @param $givenDate
     * @return false|string
     */
    public static function getNextWorkDay($givenDate)
    {
        $givenTime = strtotime($givenDate);
        $thisYear = date('Y', $givenTime);

        $holidays = array_merge(self::getHolidaysForYear($thisYear),
            self::getHolidaysForYear($thisYear + 1),
            self::EASTERS2
        );

        $i = 1;
        $nextBusinessDay = date('Y-m-d', strtotime($givenDate . ' +' . $i . ' Weekday'));

        while (in_array($nextBusinessDay, $holidays)) {
            $i++;
            $nextBusinessDay = date('Y-m-d', strtotime($givenDate . ' +' . $i . ' Weekday'));
        }
        return $nextBusinessDay;
    }

    /**
     * @param string $date
     * @return bool
     */
    public static function isWorkDay($givenDate)
    {
        $givenTime = strtotime($givenDate);
        $thisYear = date('Y', $givenTime);

        $holidays = array_merge(self::getHolidaysForYear($thisYear),
            self::getHolidaysForYear($thisYear + 1),
            self::EASTERS2
        );

        if (in_array($givenDate, $holidays)) {
            return false;
        }
        $currentWeekDay = date("w", strtotime($givenDate));
        if ($currentWeekDay == "0" || $currentWeekDay == "6") { // sunday or saturday
            return false;
        }
        return true;
    }

    /**
     * @param $dateTime
     * @param $maxHour
     * @return false|string
     */
    public static function getThisOrNextWorkDay($dateTime, $maxHour)
    {

        // 1) patikrinam ar valanda ne didesnė nei $maxHour
        // 2) jei mažesnė, tai tikrinam ar šiandien darbo diena
        // tada grąžinam duotą datą
        // 3) kitu atveju kviečiam getNextWorkDay

        $time = strtotime($dateTime);
        $givenHour = date("H", $time);
        $givenDate = date('Y-m-d', $time);
        if ($givenHour < $maxHour && self::isWorkDay($givenDate)) {
            return $givenDate;
        }
        return self::getNextWorkDay($givenDate);
    }

    /**
     * @param $year
     * @return array
     */
    public static function getHolidaysForYear($year)
    {
        $fPrependYear = function ($monthAndDay) use ($year) {
            return $year . '-' . $monthAndDay;
        };

        $holidays = array_map($fPrependYear, self::LITHUANIAN_HOLIDAYS);
        return $holidays;
    }

    /**
     * @param string $str
     * @param int $maxLength
     * @return false|string
     */
    public static function truncate($str, $maxLength)
    {
        if (strlen($str) <= $maxLength) {
            return $str;
        } else {
            return mb_substr($str, 0, $maxLength);
        }
    }

    /**
     * @param mixed $val
     * @return bool
     */
    public static function isNonEmpty($val)
    {
        return !empty($val);
    }

    /**
     * @param string|null $str
     * @return false|string|null
     */
    public static function unquote(string $str = null)
    {
        if ($str == null) {
            return $str;
        }
        $len = strlen($str);
        if ($len < 2) {
            return $str;
        }

        return substr($str, 1, $len - 2);
    }

    public static function median($values)
    {
        if (!is_array($values) || count($values) == 0) {
            return null;
        }
        sort($values);
        $i = count($values) / 2;
        return $values[$i];
    }

    /**
     * @param mixed[][][] $triple
     * @return mixed[][]
     */
    public static function putInColumnsArrays($triple) {

        $lenghts = array_map ('count', $triple);
        $maxLenght = max($lenghts);

        $replacements = [];
        foreach ($triple as $arrayOfArrays ) {
            $size = count($arrayOfArrays[0]);
            $replacement = array_fill(0, $size, '' );
            $replacements[] = $replacement;
        }

        $lines = [];

        for ( $i=0; $i < $maxLenght; $i++) {
            $line = [];
            for ( $j=0; $j < count($triple); $j++) {
                $arrayOfArrays = $triple[$j];
                $replacement = $replacements[$j];

                if (count($arrayOfArrays) > $i ) {
                    $subLine = $arrayOfArrays[$i];
                }
                else {
                    $subLine = $replacement;
                }

                $line = array_merge($line, $subLine );
            }
            $lines[] = $line;
        }

        return $lines;
    }
}