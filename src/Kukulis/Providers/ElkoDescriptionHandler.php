<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.6.26
 * Time: 12.06
 */

namespace Kukulis\Providers;


use Kukulis\Elko\Data\DescriptionObject;
use Kukulis\Elko\ElkoDescriptionsHolder;

class ElkoDescriptionHandler
{
    /**
     * @param DescriptionObject[] $descriptionObjects
     * @return ElkoDescriptionsHolder[]
     */
    public static function makeElkoDescriptionsHolders($descriptionObjects)
    {

        /** @var ElkoDescriptionsHolder[] $descriptionHolders */
        $descriptionHolders = [];

        foreach ($descriptionObjects as $descriptionObject) {
            $productId = $descriptionObject->productId;
            $descriptionHolder = new ElkoDescriptionsHolder($productId);

            foreach ($descriptionObject->description as $d) {
                $descriptionHolder->add($d);
            }

            $descriptionHolders[$productId] = $descriptionHolder;
        }

        return $descriptionHolders;
    }

}