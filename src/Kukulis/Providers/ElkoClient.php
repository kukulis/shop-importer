<?php

namespace Kukulis\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Utils;
use Kukulis\Elko\Data\AvailabilityElement;
use Kukulis\Elko\Data\CategoryTreeNode;
use Kukulis\Elko\Data\DescriptionObject;
use Kukulis\Elko\Data\MediaData;
use Kukulis\Elko\Data\Product;
use Kukulis\Elko\Data\Vendor;
use Kukulis\Elko\Data\WebProductBase;
use Kukulis\Presta\Exception\ErrorException;
use Psr\Log\LoggerInterface;

class ElkoClient
{
    const URI_AUTH_TOKENS = '/v3.0/api/Token';
    const URI_WEB_PRODUCTS_LIST = '/v3.0/api/Web/ProductList';
    const URI_GET_CATALOG_PRODUCTS = '/v3.0/api/Catalog/Products';
    const URI_MEDIA_ITEMS = '/v3.0/api/Catalog/MediaItems';
    const URI_PRODUCTS_DESCRIPTIONS = '/v3.0/api/Catalog/Products/IDS_PLACEHOLDER/Description';
    const URI_CATEGORY_TREE = '/v3.0/api/Catalog/CategoryTree';
    const URI_AVAILABILITY_AND_PRICE = '/v3.0/api/Catalog/AvailabilityAndPrice/AvailabilityPrice'; // ?elkoCodes=1303388,1247557,1301633,1301630
    const URI_VENDORS = '/v3.0/api/Catalog/Vendors';

    const IDS_PLACEHOLDER = 'IDS_PLACEHOLDER';


    const PARAM_ELKO_CODE = 'elkoCode';


    /** @var LoggerInterface */
    private $logger;

    /** @var Client */
    private $guzzle;

    /** @var string */
    private $baseUrl;

    /** @var string */
    private $_user;

    /** @var string */
    private $_password;

    /**
     * ElkoClient constructor.
     * @param LoggerInterface $logger
     * @param Client $guzzle
     */
    public function __construct(LoggerInterface $logger, Client $guzzle, string $baseUrl)
    {
        $this->logger = $logger;
        $this->guzzle = $guzzle;
        $this->baseUrl = $baseUrl;
    }


    /**
     * @param $user
     * @param $password
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login($user, $password)
    {
        $requestUri = $this->baseUrl . self::URI_AUTH_TOKENS;

        $loginsArr = [
            'username' => $user,
            'password' => $password,
        ];

        $loginsJson = Utils::jsonEncode($loginsArr);

        /** @var Response $rez */
        $rez = $this->guzzle->post($requestUri,
            [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $loginsJson
            ]
        );

        $token = $rez->getBody()->getContents();

        return $token;

    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getToken()
    {
        return $this->login($this->_user, $this->_password);
    }

    /**
     * @param string $token
     * @return bool
     */
    public function validateToken($token)
    {
        return strpos($token, '{') === false; // '{' possible sign of json data
    }

    /**
     * @param string $token
     * @throws ErrorException
     * @return WebProductBase[]
     *
     */
    public function getWebProductList($token)
    {
        try {
            $requestUri = $this->baseUrl . self::URI_WEB_PRODUCTS_LIST;

            /** @var Response $rez */
            $rez = $this->guzzle->get($requestUri,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ],
                ]
            );
            if ($rez->getStatusCode() != 200) {
                throw new ErrorException('ELKO Response error: ' . $rez->getReasonPhrase());
            }
            $contents = $rez->getBody()->getContents();

            /** @var WebProductBase[] $webProductList */
            $webProductList = Utils::jsonDecode($contents);

            return $webProductList;
        } catch (GuzzleException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param string $token
     * @throws ErrorException
     * @return Vendor[]
     *
     */
    public function getVendorList($token)
    {
        try {
            $requestUri = $this->baseUrl . self::URI_VENDORS;

            /** @var Response $rez */
            $rez = $this->guzzle->get($requestUri,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ],
                ]
            );
            if ($rez->getStatusCode() != 200) {
                throw new ErrorException('ELKO Response error: ' . $rez->getReasonPhrase());
            }
            $contents = $rez->getBody()->getContents();

            /** @var Vendor[] $VendorList */
            $VendorList = Utils::jsonDecode($contents);

            return $VendorList;
        } catch (GuzzleException $e) {
            throw new ErrorException($e->getMessage());
        }
    }


    /**
     * @param string $token
     * @param int[] $ids
     * @return Product[]
     * @throws ErrorException
     */
    public function getCatalogProducts($token, $ids)
    {
        try {
            $requestUri = $this->baseUrl . self::URI_GET_CATALOG_PRODUCTS;

            /** @var Response $rez */
            $rez = $this->guzzle->get($requestUri,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ],
                    'query' => [
                        self::PARAM_ELKO_CODE => join(',', $ids),
                    ]
                ]
            );
            if ($rez->getStatusCode() != 200) {
                throw new ErrorException('ELKO Response error: ' . $rez->getReasonPhrase());
            }
            $contents = $rez->getBody()->getContents();

            /** @var Product[] $products */
            $products = Utils::jsonDecode($contents);

            return $products;
        } catch (GuzzleException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param string $token
     * @param int[] $ids
     * @return DescriptionObject[]
     * @throws ErrorException
     */
    public function getProductsDescriptions($token, $ids)
    {
        $requestUrl = $this->baseUrl . self::URI_PRODUCTS_DESCRIPTIONS;

        try {
            $idsStr = join(',', $ids);
            $fixedUrl = str_replace(self::IDS_PLACEHOLDER, $idsStr, $requestUrl);

            /** @var Response $rez */
            $rez = $this->guzzle->get($fixedUrl,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ]
                ]
            );
            if ($rez->getStatusCode() != 200) {
                throw new ErrorException('ELKO Response error: ' . $rez->getReasonPhrase());
            }
            $contents = $rez->getBody()->getContents();

            /** @var DescriptionObject[] $descriptionObjects */
            $descriptionObjects = Utils::jsonDecode($contents);

            return $descriptionObjects;
        } catch (GuzzleException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param string $token
     * @param int[] $ids
     * @return MediaData[]
     * @throws ErrorException
     * @throws GuzzleException
     */
    public function getProductsMedias($token, $ids)
    {
        $requestUri = $this->baseUrl . self::URI_MEDIA_ITEMS . '/' . join(',', $ids);

        /** @var Response $rez */
        $rez = $this->guzzle->get($requestUri,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ]
            ]
        );
        if ($rez->getStatusCode() != 200) {
            throw new ErrorException('ELKO Response error: ' . $rez->getReasonPhrase());
        }
        $contents = $rez->getBody()->getContents();

        /** @var MediaData[] $mediaDatas */
        $mediaDatas = Utils::jsonDecode($contents);

        return $mediaDatas;
    }


    /**
     * @param string $token
     * @return CategoryTreeNode[]
     * @throws ErrorException
     * @throws GuzzleException
     */
    public function getCategoriesTree($token)
    {
        $requestUri = $this->baseUrl . self::URI_CATEGORY_TREE;

        /** @var Response $rez */
        $rez = $this->guzzle->get($requestUri,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ]
            ]
        );
        if ($rez->getStatusCode() != 200) {
            throw new ErrorException('ELKO Response error: ' . $rez->getReasonPhrase());
        }
        $contents = $rez->getBody()->getContents();

        /** @var CategoryTreeNode[] $categoryTreeNodes */
        $categoryTreeNodes = Utils::jsonDecode($contents);

        return $categoryTreeNodes;
    }

    /**
     * @param string $token
     * @return AvailabilityElement[]
     * @throws ErrorException
     */
    public function getAvailabilityAndPrice($token, $ids): array {
        $requestUri = $this->baseUrl . self::URI_AVAILABILITY_AND_PRICE  . '?elkoCodes=' . join(',', $ids);

        try {
            /** @var Response $rez */
            $rez = $this->guzzle->get($requestUri,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ]
                ]
            );
        } catch (GuzzleException $e ) {
            throw new ErrorException('Elko Response error: '.$e->getMessage());
        }
        if ($rez->getStatusCode() != 200) {
            throw new ErrorException('ELKO Response error: ' . $rez->getReasonPhrase());
        }
        $contents = $rez->getBody()->getContents();

        /** @var AvailabilityElement[] $availabilityElements */
        $availabilityElements = Utils::jsonDecode($contents);

        return $availabilityElements;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user): void
    {
        $this->_user = $user;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->_password = $password;
    }

}