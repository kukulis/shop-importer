<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.17
 * Time: 09.25
 */

namespace Kukulis\Data;


class Category
{
    /** @var string */
    public $code;

    /** @var string */
    public $name;

    /** @var string */
    public $parentCode;

    /**
     * @param $code
     * @param $name
     * @param $parentCode
     * @return Category
     */
    public static function create($code, $name, $parentCode)
    {
        $c = new Category();
        $c->code = $code;
        $c->name = $name;
        $c->parentCode = $parentCode;
        return $c;
    }
}