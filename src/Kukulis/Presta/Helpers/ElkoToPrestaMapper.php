<?php

namespace Kukulis\Presta\Helpers;

use Kukulis\Data\Category;
use Kukulis\Elko\Data\AvailabilityElement;
use Kukulis\Elko\Data\MediaData;
use Kukulis\Elko\Data\Product;
use Kukulis\Elko\ElkoDescriptionsHolder;
use Kukulis\Helpers\Utils;
use Kukulis\Presta\Data\TmpCategory;
use Kukulis\Presta\Data\TmpCategoryProduct;
use Kukulis\Presta\Data\TmpImage;
use Kukulis\Presta\Data\TmpProduct;
use Kukulis\Presta\Data\TmpSketisPricesAmounts;
use Kukulis\Helpers\TransliterationHelper;

class ElkoToPrestaMapper
{

    const ELKO_PREFIX = 'E_';

    /**
     * @param Product $elkoCatalogProduct
     * @param $defaultDate
     * @param $isoCode
     * @return TmpProduct
     */
    public static function mapProduct(
        $elkoCatalogProduct,
        $defaultDate,
        $isoCode,
        ElkoDescriptionsHolder $descriptionsHolder
    ) {
        $tmpProduct = new TmpProduct();
        $codeForReference = self::getNomnr($elkoCatalogProduct->elkoCode);

        $tmpProduct->nomnr = self::getNomnr($elkoCatalogProduct->elkoCode);
        $tmpProduct->id_supplier = 0; // paskui sql'ai anaujins pagal supplier reikšmę
        $tmpProduct->supplier = $elkoCatalogProduct->vendorCode; //Kodėl čia vendorCode, manau čia turi būt tiekėjas (pvz ELKO)
        $tmpProduct->id_manufacturer = 0;
        $tmpProduct->id_category_default = 1; // šitą gal dinamiškai reiks dėti
        $tmpProduct->id_shop_default = 1;
        $tmpProduct->id_tax_rules_group = 1;
        $tmpProduct->on_sale = 0;
        $tmpProduct->online_only = 0;
        $tmpProduct->ean13 = substr($elkoCatalogProduct->eanCode, 0, 13);
        $tmpProduct->isbn = '';
        $tmpProduct->upc = '';
        $tmpProduct->mpn = $elkoCatalogProduct->manufacturerCode;
        $tmpProduct->ecotax = 0;
        $tmpProduct->quantity = Utils::extractNumber($elkoCatalogProduct->quantity);
        $tmpProduct->minimal_quantity = 1;
        $tmpProduct->low_stock_threshold = 1;
        $tmpProduct->low_stock_alert = 0;
        $tmpProduct->price = $elkoCatalogProduct->discountPrice; //TODO: padaryt atskirą funkciją pardavimo kainai
        $tmpProduct->wholesale_price = $elkoCatalogProduct->discountPrice;
        $tmpProduct->unity = null;
        $tmpProduct->unit_price_ratio = 0;
        $tmpProduct->additional_shipping_cost = 0;
        $tmpProduct->reference = $elkoCatalogProduct->manufacturerCode;  //Buvo $codeForReference
        $tmpProduct->supplier_reference = $codeForReference;
        $tmpProduct->location = null;
        $tmpProduct->width = 0;
        $tmpProduct->height = 0;
        $tmpProduct->depth = 0;
        $tmpProduct->weight = Utils::extractFloat($descriptionsHolder->getUNIT_NET_WEIGHT()->value);
//        $tmpProduct->weight = 0;
        $tmpProduct->out_of_stock = 1;
        $tmpProduct->additional_delivery_times = 1;
        $tmpProduct->quantity_discount = 0;
        $tmpProduct->customizable = 0;
        $tmpProduct->uploadable_files = 0;
        $tmpProduct->text_fields = 0;
        $tmpProduct->active = 1;
        $tmpProduct->redirect_type = '301-product';
        $tmpProduct->id_type_redirected = 0;
        $tmpProduct->available_for_order = 1;
        $tmpProduct->available_date = "0000-00-00";
        $tmpProduct->show_condition = 0;
        $tmpProduct->condition = 'new';
        $tmpProduct->show_price = 1;
        $tmpProduct->indexed = 0;
        $tmpProduct->visibility = 'both';
        $tmpProduct->cache_is_pack = 0;
        $tmpProduct->cache_has_attachments = 0;
        $tmpProduct->is_virtual = 0;
        $tmpProduct->cache_default_attribute = null;
        $tmpProduct->date_add = $defaultDate;
        $tmpProduct->date_upd = $defaultDate;
        $tmpProduct->advanced_stock_management = 0;
        $tmpProduct->pack_stock_type = 3;
        $tmpProduct->state = 1;
        $tmpProduct->id_shop = 1;
//        $tmpProduct->id_lang = 1;
        $tmpProduct->description = $descriptionsHolder->getDESCRIPTION()->value;
        $tmpProduct->description_short = $descriptionsHolder->getFULL_DESCRIPTION_LINE()->value;
        $tmpProduct->link_rewrite = Utils::generateSeoLink($elkoCatalogProduct->name, true, 100);
        $tmpProduct->meta_description = TransliterationHelper::nonAlphaNumericToConnectors(substr($elkoCatalogProduct->fullDsc, 0, 512));
        $tmpProduct->meta_keywords = '';
        $tmpProduct->meta_title = '';
        $tmpProduct->name = substr($elkoCatalogProduct->name, 0, 128);
        $tmpProduct->available_now = '';
        $tmpProduct->available_later = '';
        $tmpProduct->delivery_in_stock = '';
        $tmpProduct->delivery_out_stock = '';
        $tmpProduct->brand = $elkoCatalogProduct->vendorName;

        $tmpProduct->tipas = ''; // TODO
        $tmpProduct->paskirtis = ''; // TODO
        $tmpProduct->dydis = '';
        $tmpProduct->kiekis = '';
        $tmpProduct->iso_code = $isoCode; // TODO

        $tmpProduct->parent = ''; // TODO
        $tmpProduct->spalva = ''; // TODO
        $tmpProduct->var_name = ''; // TODO

        return $tmpProduct;
    }

    /**
     * @param Product $elkoCatalogProduct
     * @return TmpCategoryProduct
     */
    public static function mapProductCategory($elkoCatalogProduct)
    {
        $elkoCatalogProduct->catalog;

        $tmpCategoryProduct = new TmpCategoryProduct();

        $tmpCategoryProduct->sketis_code = $elkoCatalogProduct->catalog;
        $tmpCategoryProduct->nomnr = self::getNomnr($elkoCatalogProduct->elkoCode);
        $tmpCategoryProduct->position = 1;

        return $tmpCategoryProduct;
    }

    /**
     * @param Category $elkoCategory
     * @param string $defaultDate
     * @param string $isoCode
     * @return TmpCategory
     */
    public static function mapCategory(Category $elkoCategory, string $defaultDate, string $isoCode)
    {
        $categoryPath = strtolower($elkoCategory->parentCode . '_' . $elkoCategory->code);

        $tmpCategory = new TmpCategory();
        $tmpCategory->name = $elkoCategory->name;
        $tmpCategory->sketis_code = $elkoCategory->code;
        $tmpCategory->parent_code = $elkoCategory->parentCode;

        $tmpCategory->id_shop_default = 1;
        $tmpCategory->date_add = $defaultDate;
        $tmpCategory->date_upd = $defaultDate;
        $tmpCategory->id_shop = 1;
        $tmpCategory->id_lang = 1;
        $tmpCategory->description = '';
        $tmpCategory->link_rewrite = $categoryPath;  // PRISIMINT: ps_category_lang lentelėj turi išlikt originalus link_rewrite turinys

        $tmpCategory->meta_title = '';
        $tmpCategory->meta_keywords = $elkoCategory->name;
        $tmpCategory->meta_description = '';
        $tmpCategory->active = 1;
        $tmpCategory->position = 1;
        $tmpCategory->iso_code = $isoCode;
        $tmpCategory->id_parent = 2; // defaultinis, paskui bus perskaičiuojamas


        return $tmpCategory;
    }

    /**
     * @param AvailabilityElement $availabilityElement
     * @return TmpSketisPricesAmounts
     */
    public static function mapAmountsElement($availabilityElement)
    {
        $tmpAmounts = new TmpSketisPricesAmounts();
        $tmpAmounts->nomnr = self::getNomnr($availabilityElement->productId);

        $tmpAmounts->amount = Utils::extractNumber($availabilityElement->stockQuantity);
        $tmpAmounts->price = $availabilityElement->prices->discountPrice*1.07;  //TODO: padaryt atskirą funkciją pardavimo kainai
        $tmpAmounts->wholesale_price = $availabilityElement->prices->discountPrice;
        $tmpAmounts->id_product = null;
        $tmpAmounts->id_product_attribute = 0;

        return $tmpAmounts;
    }

    /**
     * @param MediaData $mediaData
     * @param bool $forceNullCower
     * @return TmpImage[]
     */
    public static function mapTmpImage($mediaData, bool $forceNullCower = false)
    {

        /** @var TmpImage[] $tmpImages */
        $tmpImages = [];


        foreach ($mediaData->mediaFiles as $mediaFile) {
            $tmpImage = new TmpImage();
            $tmpImage->sketis_image_id = $mediaFile->objectId;
            $tmpImage->uri = $mediaFile->link;
            $tmpImage->nomnr = self::getNomnr($mediaData->id);
            $tmpImage->position = $mediaFile->sequence;
            $tmpImage->version = $tmpImage->sketis_image_id;
            $tmpImage->copied_to_live = 0;
            $tmpImage->id_shop = 1;

            if ($forceNullCower) {
                $tmpImage->cover = null;
            } else {
                $tmpImage->cover = ($mediaFile->sequence == 1) ? 1 : null;
            }

            $tmpImages[] = $tmpImage;
        }

        return $tmpImages;
    }

    public static function getNomnr($elkoProductId)
    {
        return self::ELKO_PREFIX . $elkoProductId;
    }
}