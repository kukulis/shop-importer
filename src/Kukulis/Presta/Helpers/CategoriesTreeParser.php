<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.17
 * Time: 09.23
 */

namespace Kukulis\Presta\Helpers;


use Kukulis\Data\CategoriesHolder;
use Kukulis\Data\Category;
use Kukulis\Elko\Data\CategoryTreeNode;
use Kukulis\Helpers\TransliterationHelper;
use stdClass;

class CategoriesTreeParser
{
    /**
     * @param stdClass[]|CategoryTreeNode[] $nodes
     * @param string|null $parentCode
     * @param CategoriesHolder|null $categoriesHolder
     * @return Category[]
     */
    public static function treeToFlatMap(array $nodes, $parentCode, ?CategoriesHolder $categoriesHolder=null): array
    {
        if ($categoriesHolder == null) {
            $categoriesHolder = new CategoriesHolder();
        }

        foreach ($nodes as $node) {
            $category = self::treeNodeToCategory($node, $parentCode);
            $categoriesHolder->categories[$category->code] = $category;

            if (!empty($node->childs) && is_array($node->childs)) {
                self::treeToFlatMap($node->childs, $category->code, $categoriesHolder);
            }
        }
        return $categoriesHolder->categories;
    }

    /**
     * @param CategoryTreeNode|stdClass $treeNode
     * @return Category
     */
    public static function treeNodeToCategory($treeNode, $parentCode): Category
    {
        $category = new Category();
        $code = $treeNode->code;
        if (empty($code)) {
            $code = TransliterationHelper::toCode($treeNode->name, '_');
        }

        $category->name = $treeNode->name;
        $category->code = $code;
        $category->parentCode = $parentCode;
        return $category;
    }
}