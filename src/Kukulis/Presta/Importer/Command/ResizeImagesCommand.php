<?php

namespace Kukulis\Presta\Importer\Command;

use Gumlet\ImageResizeException;
use Kukulis\Presta\Exception\ErrorException;
use Kukulis\Presta\Services\ImportImagesService;
use Psr\Log\LoggerInterface;

class ResizeImagesCommand
{
    /** @var ImportImagesService */
    private $importImagesService;

    /** @var LoggerInterface */
    private $logger;

    /**
     * DownloadImagesCommand constructor.
     * @param ImportImagesService $importImagesService
     */
    public function __construct(
        ImportImagesService $importImagesService,
        LoggerInterface $logger
    ) {
        $this->importImagesService = $importImagesService;
        $this->logger = $logger;
    }

    /**
     * @param array $params
     * @throws ErrorException
     * @return int
     */
    public function run(array $params)
    {
        try {
            if (count($params) < 3) {
                throw new ErrorException('Must be 2 parameters: shop dir and portion size');
            }
            $shopDir = $params[1];
            $portionSize = $params[2];

            $this->importImagesService->resizeImages($shopDir, $portionSize);

            return 0;
        } catch (ImageResizeException $e) {
            throw new ErrorException($e->getMessage());
        }
    }
}