<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.28
 * Time: 19.55
 */

namespace Kukulis\Presta\Importer\Command;


use Kukulis\Presta\Services\PrestaTreeService;
use Psr\Log\LoggerInterface;

class BuildCategoriesTreeCommand
{
    /** @var LoggerInterface */
    private $logger;


    /** @var PrestaTreeService */
    private $prestaTreeService;

    /**
     * BuildCategoriesTreeCommand constructor.
     * @param LoggerInterface $logger
     * @param PrestaTreeService $prestaTreeService
     */
    public function __construct(LoggerInterface $logger, PrestaTreeService $prestaTreeService)
    {
        $this->logger = $logger;
        $this->prestaTreeService = $prestaTreeService;
    }

    /**
     * @return bool
     */
    public function run()
    {
        $this->logger->debug('BuildCategoriesTreeCommand->run called');
        $this->prestaTreeService->generateTree();
        return true;
    }
}