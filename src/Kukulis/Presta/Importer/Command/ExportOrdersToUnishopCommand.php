<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.9.30
 * Time: 12.41
 */

namespace Kukulis\Presta\Importer\Command;


use Kukulis\Presta\Services\Unishop\ExportOrders3Service;
use Psr\Log\LoggerInterface;

class ExportOrdersToUnishopCommand
{
    /** @var LoggerInterface */
    private $logger;

    /** @var ExportOrders3Service */
    private $exportOrdersService;

    /**
     * ExportOrdersToUnishopCommand constructor.
     * @param LoggerInterface $logger
     * @param ExportOrders3Service $exportOrdersService
     */
    public function __construct(LoggerInterface $logger, ExportOrders3Service $exportOrdersService)
    {
        $this->logger = $logger;
        $this->exportOrdersService = $exportOrdersService;
    }


    public function run($params): bool
    {

        if (count($params) < 2) {
            $this->logger->error('days count must be given');
            return false;
        }
//        $clientCode = $params[1];
        $days = $params[1];
        $this->exportOrdersService->exportOrders($days);

        return true;
    }

}