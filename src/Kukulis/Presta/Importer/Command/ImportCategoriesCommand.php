<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.8
 * Time: 20.58
 */

namespace Kukulis\Presta\Importer\Command;


use Kukulis\Presta\Services\ImportElkoDataService;
use Psr\Log\LoggerInterface;

class ImportCategoriesCommand
{
    /** @var LoggerInterface */
    private $logger;

    /** @var ImportElkoDataService */
    private $importElkoDataService;

    /**
     *
     * @param LoggerInterface $logger
     * @param ImportElkoDataService $importElkoDataService
     */
    public function __construct(LoggerInterface $logger, ImportElkoDataService $importElkoDataService)
    {
        $this->logger = $logger;
        $this->importElkoDataService = $importElkoDataService;
    }

    /**
     * @param $params
     * @return bool
     * @throws \Kukulis\Presta\Exception\ErrorException
     */
    public function run($params)
    {
        $this->logger->debug('ImportCategoriesCommand->run called');
        if (count($params) < 3) {
            $this->logger->error('file and locale parameters must be given');
            return false;
        }
        $file = $params[1];
        $locale = $params[2];


        $count = $this->importElkoDataService->importCategories($file, $locale);
        $this->logger->info('Imported categories ' . $count);
        return true;
    }

}