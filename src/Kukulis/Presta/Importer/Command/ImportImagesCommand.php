<?php

namespace Kukulis\Presta\Importer\Command;


use Kukulis\Presta\Services\ImportElkoDataService;
use Psr\Log\LoggerInterface;

class ImportImagesCommand
{
    /** @var LoggerInterface */
    private $logger;

    /** @var ImportElkoDataService */
    private $importImagesService;

    /**
     * ImportImagesCommand constructor.
     * @param LoggerInterface $logger
     * @param ImportElkoDataService $importImagesService
     */
    public function __construct(LoggerInterface $logger, ImportElkoDataService $importImagesService)
    {
        $this->logger = $logger;
        $this->importImagesService = $importImagesService;
    }


    /**
     * @param array $params
     * @return int
     * @throws \Kukulis\Presta\Exception\ErrorException
     */
    public function run(array $params) {
        if (count($params) < 2) {
            $this->logger->error('file parameter must be given');
            return false;
        }

        $file = $params[1];

        return $this->importImagesService->importImages($file);
    }
}