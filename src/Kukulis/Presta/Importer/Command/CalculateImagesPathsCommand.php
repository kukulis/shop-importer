<?php

namespace Kukulis\Presta\Importer\Command;

use Kukulis\Presta\Services\ImportImagesService;
use Psr\Log\LoggerInterface;

class CalculateImagesPathsCommand
{
    /** @var ImportImagesService */
    private $importImagesService;

    /** @var LoggerInterface */
    private $logger;

    /**
     * DownloadImagesCommand constructor.
     * @param ImportImagesService $importImagesService
     */
    public function __construct(
        ImportImagesService $importImagesService,
        LoggerInterface $logger
    )
    {
        $this->importImagesService = $importImagesService;
        $this->logger = $logger;
    }


    public function run() {
        $count = $this->importImagesService->calculateImagesPaths();

        $this->logger->info('calculated images count='.$count );

        return 0;
    }
}