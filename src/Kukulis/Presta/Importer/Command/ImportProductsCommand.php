<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.6.12
 * Time: 08.39
 */

namespace Kukulis\Presta\Importer\Command;


use Kukulis\Presta\Exception\ErrorException;
use Kukulis\Presta\Services\ImportElkoDataService;
use Psr\Log\LoggerInterface;


class ImportProductsCommand
{
    /** @var LoggerInterface */
    private $logger;

    /** @var ImportElkoDataService */
    private $importElkoDataService;

    /**
     * ImportProductsCommand constructor.
     * @param LoggerInterface $logger
     * @param ImportElkoDataService $importElkoDataService
     */
    public function __construct(LoggerInterface $logger, ImportElkoDataService $importElkoDataService)
    {
        $this->logger = $logger;
        $this->importElkoDataService = $importElkoDataService;
    }


    /**
     * @param $params
     * @return bool
     * @throws \Kukulis\Presta\Exception\ErrorException
     */
    public function run($params)
    {
        try {
            $this->logger->debug("ImportProductsCommand->run called");
            if (count($params) < 3) {
                $this->logger->error('file parameter must be given, and isoCode (en,lt) must be given');
                return false;
            }
            $file = $params[1];
            $isoCode = $params[2];
            $count = $this->importElkoDataService->doImport($file, $isoCode);
            $this->logger->info('Imported ' . $count . ' products');

            return true;
        } catch (ErrorException $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
    }
}