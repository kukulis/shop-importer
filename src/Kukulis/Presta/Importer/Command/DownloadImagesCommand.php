<?php

namespace Kukulis\Presta\Importer\Command;

use Kukulis\Presta\Exception\ErrorException;
use Kukulis\Presta\Services\ImportImagesService;
use Psr\Log\LoggerInterface;

class DownloadImagesCommand
{
    /** @var ImportImagesService */
    private $importImagesService;

    /** @var LoggerInterface */
    private $logger;

    /**
     * DownloadImagesCommand constructor.
     * @param ImportImagesService $importImagesService
     */
    public function __construct(
        ImportImagesService $importImagesService,
        LoggerInterface $logger
    )
    {
        $this->importImagesService = $importImagesService;
        $this->logger = $logger;
    }


    /**
     * @param array $params
     * @throws ErrorException
     * @return int
     */
    public function run(array $params) {

        if (count($params) < 3 ) {
            throw new ErrorException('Must be two parameters: source url prefix, and shop base path' );
        }

        $urlPrefix = $params[1];

        if ( $urlPrefix == '-') {
            $urlPrefix = '';
        }

        $shopBasePath = $params[2];

        $count = $this->importImagesService->downloadImages($urlPrefix, $shopBasePath);

        $this->logger->info('downloaded images '.$count );

        return 0;
    }

}