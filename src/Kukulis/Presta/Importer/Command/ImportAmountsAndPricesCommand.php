<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.8.15
 * Time: 21.16
 */

namespace Kukulis\Presta\Importer\Command;


use Kukulis\Presta\Services\ImportElkoDataService;
use Psr\Log\LoggerInterface;

class ImportAmountsAndPricesCommand
{
    /** @var LoggerInterface */
    private $logger;

    /** @var ImportElkoDataService */
    private $importElkoDataService;

    /**
     * ImportAmountsAndPricesCommand constructor.
     * @param LoggerInterface $logger
     * @param ImportElkoDataService $importElkoDataService
     */
    public function __construct(LoggerInterface $logger, $importElkoDataService)
    {
        $this->logger = $logger;
        $this->importElkoDataService = $importElkoDataService;
    }


    /**
     * @param $params
     * @return bool|int
     * @throws \Kukulis\Presta\Exception\ErrorException
     */
    public function run($params) {
        if (count($params) < 2) {
            $this->logger->error('file parameter must be given');
            return false;
        }
        $file = $params[1];
        $count = $this->importElkoDataService->importAmountsAndPrices($file);
        $this->logger->info("imported amount ".$count );
        return 0;
    }

}