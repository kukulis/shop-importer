<?php
/**
 * Cart.php
 * Created by Giedrius Tumelis.
 * Date: 2020-07-21
 * Time: 11:32
 */

namespace Kukulis\Presta\Data;


class Cart
{
    public $id_cart;
    public $id_shop_group;
    public $id_shop;
    public $id_carrier;
    public $delivery_option;
    public $id_lang;
    public $id_address_delivery;
    public $id_address_invoice;
    public $id_currency;
    public $id_customer;
    public $id_guest;
    public $secure_key;
    public $recyclable;
    public $gift;
    public $gift_message;
    public $mobile_theme;
    public $allow_seperated_package;
    public $date_add;
    public $date_upd;
    public $checkout_session_data;

    /** @var Currency */
    public $currency;
}