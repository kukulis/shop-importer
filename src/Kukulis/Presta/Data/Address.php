<?php
/**
 * Address.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.28
 * Time: 09.22
 */

namespace Kukulis\Presta\Data;


class Address
{
    public $id_address;
    public $id_country;
    public $id_state;
    public $id_customer;
    public $id_manufacturer;
    public $id_supplier;
    public $id_warehouse;
    public $alias;
    public $company;
    public $lastname;
    public $firstname;
    public $address1;
    public $address2;
    public $postcode;
    public $city;
    public $other;
    public $phone;
    public $phone_mobile;
    public $vat_number;
    public $dni;
    public $date_add;
    public $date_upd;
    public $active;
    public $deleted;

    // -- not in db
    /** @var Country */
    public $country;

    /** @var Customer */
    public $customer;

}