<?php
/**
 * CategoryTreeData.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.22
 * Time: 16.17
 */

namespace Kukulis\Presta\Data;


class CategoryTreeItem
{
    public $id_category;
    public $id_parent;
    public $level_depth;
    public $nleft;
    public $nright;
    public $date_add;
    public $date_upd;

    public $children=[];
}