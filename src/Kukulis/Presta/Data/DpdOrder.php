<?php
/**
 * DpdOrder.php
 * Created by Giedrius Tumelis.
 * Date: 20.6.26
 * Time: 09.46
 */

namespace Kukulis\Presta\Data;


class DpdOrder
{
    public $id_dpd_order;
    public $carrier_type;
    public $num_packages;
    public $order_weight;
    public $phone_number;
    public $use_cod;
    public $cod_amount;
    public $delivery_time;
    public $id_dpd_delivery_point;
    public $date_add;
    public $date_upd;

    /** @var DpdDeliveryPoint */
    public $deliveryPoint;
}