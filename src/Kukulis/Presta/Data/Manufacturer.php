<?php
/**
 * Manufacturer.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.7
 * Time: 14.56
 */

namespace Kukulis\Presta\Data;


use Kukulis\Data\IRowObject;

class Manufacturer implements IRowObject
{
    /** @Autoincrementid */
    public $id_manufacturer;
    public $name;
    public $date_add;
    public $date_upd;
    public $active;
}