<?php
/**
 * Order.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.28
 * Time: 09.07
 */

namespace Kukulis\Presta\Data;


class Order
{
    const PAYSERA_MODULE_NAME = 'paysera';

    const CHECKPAYMENT_MODULE= 'ps_checkpayment';
    const WIREPAYMENT_MODULE = 'ps_wirepayment';
    const COD_MODULE = 'ps_cashondelivery';
//    const codwfeeplus

    public $id_order;
    public $reference;
    public $id_shop_group;
    public $id_shop;
    public $id_carrier;
    public $id_lang;
    public $id_customer;
    public $id_cart;
    public $id_currency;
    public $id_address_delivery;
    public $id_address_invoice;
    public $current_state;
    public $secure_key;
    public $payment;
    public $conversion_rate;
    public $module;
    public $recyclable;
    public $gift;
    public $gift_message;
    public $mobile_theme;
    public $shipping_number;
    public $total_discounts;
    public $total_discounts_tax_incl;
    public $total_discounts_tax_excl;
    public $total_paid;
    public $total_paid_tax_incl;
    public $total_paid_tax_excl;
    public $total_paid_real;
    public $total_products;
    public $total_products_wt;
    public $total_shipping;
    public $total_shipping_tax_incl;
    public $total_shipping_tax_excl;
    public $carrier_tax_rate;
    public $total_wrapping;
    public $total_wrapping_tax_incl;
    public $total_wrapping_tax_excl;
    public $round_mode;
    public $round_type;
    public $invoice_number;
    public $delivery_number;
    public $invoice_date;
    public $delivery_date;
    public $valid;
    public $date_add;
    public $date_upd;


    /** @var OrderDetail[] */
    public $lines;

    /** @var Address */
    public $addressDelivery;

    /** @var Address */
    public $addressInvoice;

    /** @var Carrier */
    public $carrier;

    /** @var CustomerMessage[] */
    public $orderMessages;

    /** @var Customer */
    public $customer;

    /** @var DpdOrder */
    public $dpdOrder;

    /** @var Cart */
    public $cart;
}