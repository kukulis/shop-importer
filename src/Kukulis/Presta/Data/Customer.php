<?php
/**
 * Customer.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.28
 * Time: 14.11
 */

namespace Kukulis\Presta\Data;


class Customer
{
    public $id_customer;
    public $id_shop_group;
    public $id_shop;
    public $id_gender;
    public $id_default_group;
    public $id_lang;
    public $id_risk;
    public $company;
    public $siret;
    public $ape;
    public $firstname;
    public $lastname;
    public $email;
    public $passwd;
    public $last_passwd_gen;
    public $birthday;
    public $newsletter;
    public $ip_registration_newsletter;
    public $newsletter_date_add;
    public $optin;
    public $website;
    public $outstanding_allow_amount;
    public $show_public_prices;
    public $max_payment_days;
    public $secure_key;
    public $note;
    public $active;
    public $is_guest;
    public $deleted;
    public $date_add;
    public $date_upd;
    public $reset_password_token;
    public $reset_password_validity;
}