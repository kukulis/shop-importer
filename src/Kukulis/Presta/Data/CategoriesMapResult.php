<?php
/**
 * CategoriesMapResult.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.20
 * Time: 11.18
 */

namespace Kukulis\Presta\Data;

class CategoriesMapResult
{
    /** @var Category[] */
    public $categories=[];

    /** @var CategoryLang[] */
    public $categoriesLangs=[];

    /** @var CategoryProduct[] */
    public $categoriesProducts=[];

    /** @var CategoryShop[] */
    public $categoriesShops=[];
}