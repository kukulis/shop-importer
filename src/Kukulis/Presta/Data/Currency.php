<?php
/**
 * Currency.php
 * Created by Giedrius Tumelis.
 * Date: 2020-07-21
 * Time: 11:32
 */

namespace Kukulis\Presta\Data;


class Currency
{
    public $id_currency;
    public $name;
    public $iso_code;
    public $numeric_iso_code;
    public $precision;
    public $conversion_rate;
    public $deleted;
    public $active;
}