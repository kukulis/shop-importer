<?php
/**
 * Supplier.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.7
 * Time: 14.51
 */

namespace Kukulis\Presta\Data;


use Kukulis\Data\IRowObject;

class Supplier implements IRowObject
{
    /** @Autoincrementid */
    public $id_supplier;
    public $name;
    public $date_add;
    public $date_upd;
    public $active;
}