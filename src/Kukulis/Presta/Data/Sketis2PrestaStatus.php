<?php
/**
 * OrderStatusMap.php
 * Created by Giedrius Tumelis.
 * Date: 20.6.11
 * Time: 15.02
 */

namespace Kukulis\Presta\Data;

/**
 * Šitą duomenų struktūrą vėliau saugosim db lentelėje.
 *
 * Class Sketis2PrestaStatus
 * @package Kukulis\Presta\Data
 */
class Sketis2PrestaStatus
{
    public $sketisStatusCode;
    public $prestaStatusId;
    public $meaning;

    /**
     * @param string $sketisStatusCode
     * @param int $prestaStatusId
     * @param string $meaning
     * @return Sketis2PrestaStatus
     */
    public static function create( $sketisStatusCode, $prestaStatusId, $meaning) {
        $s2p = new Sketis2PrestaStatus();
        $s2p->sketisStatusCode = $sketisStatusCode;
        $s2p->prestaStatusId   = $prestaStatusId;
        $s2p->meaning          = $meaning;
        return $s2p;
    }
}