<?php
/**
 * CategoryShop.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.20
 * Time: 09.48
 */

namespace Kukulis\Presta\Data;


class CategoryShop
{
    public $id_category;
    public $id_shop;
    public $position;

    // nesaugoma į db
    public $sketis_code; // kategorjos kodas
}