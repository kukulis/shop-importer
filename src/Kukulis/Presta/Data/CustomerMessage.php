<?php
/**
 * CustomerMessage.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.29
 * Time: 08.03
 */

namespace Kukulis\Presta\Data;


class CustomerMessage
{
    public $id_customer_message;
    public $id_customer_thread;
    public $id_employee;
    public $message;
    public $file_name;
    public $ip_address;
    public $user_agent;
    public $date_add;
    public $date_upd;
    public $private;
    public $read;
}