<?php
/**
 * TmpSpecialPrice.php
 * Created by Giedrius Tumelis.
 * Date: 20.6.30
 * Time: 12.06
 */

namespace Kukulis\Presta\Data;


class TmpSpecialPrice
{
    public $nomnr;
    public $group_name;
    public $price;
    public $id_product;
    public $id_group;
}