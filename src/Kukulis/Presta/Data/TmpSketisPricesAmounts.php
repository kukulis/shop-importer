<?php
/**
 * TmpSketisPricesAmounts.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.22
 * Time: 12.07
 */

namespace Kukulis\Presta\Data;


class TmpSketisPricesAmounts
{
    public $nomnr;
    public $amount;
    public $price;
    public $wholesale_price;
    public $id_product;
    public $id_product_attribute;
}