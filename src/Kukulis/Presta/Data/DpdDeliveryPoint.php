<?php
/**
 * DpdDeliveryPoint.php
 * Created by Giedrius Tumelis.
 * Date: 20.6.26
 * Time: 09.47
 */

namespace Kukulis\Presta\Data;


class DpdDeliveryPoint
{
    public $id_dpd_delivery_point;
    public $id_country;
    public $identifier;
    public $active;
    public $deleted;
    public $name;
    public $address;
    public $postcode;
    public $city;
    public $comment;
    public $business_hours;
    public $longitude;
    public $latitude;
    public $date_add;
    public $date_upd;
}