<?php
/**
 * CategoryLang.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.20
 * Time: 09.48
 */

namespace Kukulis\Presta\Data;


class CategoryLang
{
    public $id_category;
    public $id_shop;
    public $id_lang;
    public $name;
    public $description;
    public $link_rewrite;
    public $meta_title;
    public $meta_keywords;
    public $meta_description;

    public $sketis_code; // pačioje lentelėje nėra, bet kad surišti su category lentele
}