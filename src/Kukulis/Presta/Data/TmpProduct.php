<?php
/**
 * TmpProducts.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.22
 * Time: 15.06
 */

namespace Kukulis\Presta\Data;


class TmpProduct
{
    public $nomnr; // raktas
    public $id_product; // atanujinamas vėliau
    public $id_supplier;
    public $id_manufacturer;
    public $id_category_default;
    public $id_shop_default;
    public $id_tax_rules_group;
    public $on_sale;
    public $online_only;
    public $ean13;
    public $isbn;
    public $upc;
    public $mpn;
    public $ecotax;
    public $quantity;
    public $minimal_quantity;
    public $low_stock_threshold;
    public $low_stock_alert;
    public $price;
    public $wholesale_price;
    public $unity;
    public $unit_price_ratio;
    public $additional_shipping_cost;
    public $reference;
    public $supplier_reference;
    public $location;
    public $width;
    public $height;
    public $depth;
    public $weight;
    public $out_of_stock;
    public $additional_delivery_times;
    public $quantity_discount;
    public $customizable;
    public $uploadable_files;
    public $text_fields;
    public $active;
    public $redirect_type;
    public $id_type_redirected;
    public $available_for_order;
    public $available_date;
    public $show_condition;
    public $condition;
    public $show_price;
    public $indexed;
    public $visibility;
    public $cache_is_pack;
    public $cache_has_attachments;
    public $is_virtual;
    public $cache_default_attribute;
    public $date_add;
    public $date_upd;
    public $advanced_stock_management;
    public $pack_stock_type;
    public $state;
    public $id_shop; // neaišku ar šitų reikia
    public $id_lang; // reikia (surišta su lauku 'locale')
    public $description;
    public $description_short;
    public $link_rewrite;
    public $meta_description;
    public $meta_keywords;
    public $meta_title;
    public $name;
    public $available_now;
    public $available_later;
    public $delivery_in_stock;
    public $delivery_out_stock;
    public $brand;

    public $tipas;
    public $paskirtis;
    public $dydis;
    public $kiekis;
    public $iso_code;
    public $parent;
    public $spalva;
    public $var_name;

    public $supplier;

}