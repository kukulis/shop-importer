<?php
/**
 * TmpCategories.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.22
 * Time: 15.06
 */

namespace Kukulis\Presta\Data;


class TmpCategory
{
    public $sketis_code;
    public $parent_code;
    public $id_category;
    public $id_parent;
    public $id_shop_default;
    public $level_depth=0; // čia nesvarbus
    public $nleft=0;
    public $nright=0;
    public $active=0;
    public $date_add;
    public $date_upd;
    public $position=0;
    public $is_root_category=0;
    public $id_shop;
    public $id_lang;
    public $name;
    public $description;
    public $link_rewrite;
    public $meta_title;
    public $meta_keywords;
    public $meta_description;
    public $iso_code;
}