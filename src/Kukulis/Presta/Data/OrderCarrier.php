<?php
/**
 * OrderCarrier.php
 * Created by Giedrius Tumelis.
 * Date: 20.6.26
 * Time: 09.45
 */

namespace Kukulis\Presta\Data;


class OrderCarrier
{
    public $id_order_carrier;
    public $id_order;
    public $id_carrier;
    public $id_order_invoice;
    public $weight;
    public $shipping_cost_tax_excl;
    public $shipping_cost_tax_incl;
    public $tracking_number;
    public $date_add;

}