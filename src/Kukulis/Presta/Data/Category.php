<?php
/**
 * Category.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.7
 * Time: 14.46
 */

namespace Kukulis\Presta\Data;


use Kukulis\Data\IRowObject;

class Category implements IRowObject
{
    /** @Autoincrementid */
    public $id_category;
    public $id_parent;
    public $id_shop_default;
    public $level_depth;
    public $nleft;
    public $nright;
    public $active;
    public $date_add;
    public $date_upd;
    public $position;
    public $is_root_category;

    // šitas saugomas
    public $sketis_code;

    // šitas nesaugomas db
    public $parent_code;
}