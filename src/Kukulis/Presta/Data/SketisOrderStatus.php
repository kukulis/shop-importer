<?php
/**
 * SketisOrderStatus.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.29
 * Time: 10.44
 */

namespace Kukulis\Presta\Data;


class SketisOrderStatus
{
    public $id_order;
    public $transfered=0;
    public $response_msg='';
    public $error=0;
    public $missings='';
    public $allow_execute_sent=0;
    public $error_count=0;
}