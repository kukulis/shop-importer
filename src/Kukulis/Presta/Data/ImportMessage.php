<?php
/**
 * ImportCsvMessage.php
 * Created by Giedrius Tumelis.
 * Date: 2021-01-27
 * Time: 09:21
 */

namespace Kukulis\Presta\Data;


class ImportMessage
{
    public $orderNumber;
    public $sku;
    public $message='';
    public $fileName;
}