<?php
/**
 * Carrier.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.28
 * Time: 16.11
 */

namespace Kukulis\Presta\Data;


class Carrier
{
    const DPD_EXTERNAL_MODULE_NAME='prdpd';

    public $id_carrier;
    public $id_reference;
    public $id_tax_rules_group;
    public $name;
    public $url;
    public $active;
    public $deleted;
    public $shipping_handling;
    public $range_behavior;
    public $is_module;
    public $is_free;
    public $shipping_external;
    public $need_range;
    public $external_module_name;
    public $shipping_method;
    public $position;
    public $max_width;
    public $max_height;
    public $max_depth;
    public $max_weight;
    public $grade;
}