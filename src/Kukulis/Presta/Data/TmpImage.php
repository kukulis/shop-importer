<?php
/**
 * TmpImages.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.26
 * Time: 08.44
 */

namespace Kukulis\Presta\Data;


class TmpImage
{
    public $sketis_image_id;
    public $uri;
    public $nomnr;
    public $position;
    public $version;
    public $uploaded_version;
    public $copied_to_live;
    public $id_product;
    public $id_shop;
    public $id_image;
    public $destination_path;
    public $cover;
    public $resized_version;
}