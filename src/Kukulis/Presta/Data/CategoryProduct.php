<?php
/**
 * CategoryProduct.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.20
 * Time: 09.48
 */

namespace Kukulis\Presta\Data;


class CategoryProduct
{
    public $id_category;
    public $id_product;
    public $position;

    // nesaugomi į db
    public $sketis_code; // kategorijos kodas
    public $nomnr; // produkto nomnr
}