<?php
/**
 * TmpProductTag.php
 * Created by Giedrius Tumelis.
 * Date: 2020-08-04
 * Time: 08:59
 */

namespace Kukulis\Presta\Data;


class TmpProductTag
{
    public $nomnr;
    public $tag_name;
    public $iso_code;
    public $id_product;
    public $id_tag;
    public $id_lang;
}