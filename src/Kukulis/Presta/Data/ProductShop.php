<?php
/**
 * ProductShop.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.8
 * Time: 08.20
 */

namespace Kukulis\Presta\Data;


class ProductShop
{
    public $id_product;
    public $id_shop;
    public $id_category_default;
    public $id_tax_rules_group;
    public $on_sale;
    public $online_only;
    public $ecotax;
    public $minimal_quantity;
    public $low_stock_threshold;
    public $low_stock_alert;
    public $price;
    public $wholesale_price;
    public $unity;
    public $unit_price_ratio;
    public $additional_shipping_cost;
    public $customizable;
    public $uploadable_files;
    public $text_fields;
    public $active;
    public $redirect_type;
    public $id_type_redirected;
    public $available_for_order;
    public $available_date;
    public $show_condition;
    public $condition;
    public $show_price;
    public $indexed;
    public $visibility;
    public $cache_default_attribute;
    public $advanced_stock_management;
    public $date_add;
    public $date_upd;
    public $pack_stock_type;

    /** @Nodb */
    public $ean13;
}