<?php
/**
 * ProductLang.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.8
 * Time: 08.21
 */

namespace Kukulis\Presta\Data;


class ProductLang
{
    public $id_product;
    public $id_shop;
    public $id_lang;
    public $description;
    public $description_short;
    public $link_rewrite;
    public $meta_description;
    public $meta_keywords;
    public $meta_title;
    public $name;
    public $available_now;
    public $available_later;
    public $delivery_in_stock;
    public $delivery_out_stock;

    /** @Nodb */
    public $ean13;
}