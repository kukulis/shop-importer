<?php
/**
 * Country.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.28
 * Time: 09.24
 */

namespace Kukulis\Presta\Data;


class Country
{
    public $id_country;
    public $id_zone;
    public $id_currency;
    public $iso_code;
    public $call_prefix;
    public $active;
    public $contains_states;
    public $need_identification_number;
    public $need_zip_code;
    public $zip_code_format;
    public $display_tax_label;
}