<?php
/**
 * ForbidenStatusTransit.php
 * Created by Giedrius Tumelis.
 * Date: 20.6.11
 * Time: 15.04
 */

namespace Kukulis\Presta\Data;


class ForbidenStatusTransit
{
    public $sourceStatusId;
    public $destinationStatusId;
}