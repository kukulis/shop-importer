<?php
/**
 * CustomerThread.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.29
 * Time: 08.03
 */

namespace Kukulis\Presta\Data;


class CustomerThread
{
    public $id_customer_thread;
    public $id_shop;
    public $id_lang;
    public $id_contact;
    public $id_customer;
    public $id_order;
    public $id_product;
    public $status;
    public $email;
    public $token;
    public $date_add;
    public $date_upd;
}