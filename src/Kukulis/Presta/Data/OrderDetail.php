<?php
/**
 * OrderDetail.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.28
 * Time: 09.07
 */

namespace Kukulis\Presta\Data;


class OrderDetail
{
    public $id_order_detail;
    public $id_order;
    public $id_order_invoice;
    public $id_warehouse;
    public $id_shop;
    public $product_id;
    public $product_attribute_id;
    public $id_customization;
    public $product_name;
    public $product_quantity;
    public $product_quantity_in_stock;
    public $product_quantity_refunded;
    public $product_quantity_return;
    public $product_quantity_reinjected;
    public $product_price;
    public $reduction_percent;
    public $reduction_amount;
    public $reduction_amount_tax_incl;
    public $reduction_amount_tax_excl;
    public $group_reduction;
    public $product_quantity_discount;
    public $product_ean13;
    public $product_isbn;
    public $product_upc;
    public $product_reference;
    public $product_supplier_reference;
    public $product_weight;
    public $id_tax_rules_group;
    public $tax_computation_method;
    public $tax_name;
    public $tax_rate;
    public $ecotax;
    public $ecotax_tax_rate;
    public $discount_quantity_applied;
    public $download_hash;
    public $download_nb;
    public $download_deadline;
    public $total_price_tax_incl;
    public $total_price_tax_excl;
    public $unit_price_tax_incl;
    public $unit_price_tax_excl;
    public $total_shipping_price_tax_incl;
    public $total_shipping_price_tax_excl;
    public $purchase_supplier_price;
    public $original_product_price;
    public $original_wholesale_price;
}