<?php
/**
 * ImageType.php
 * Created by Giedrius Tumelis.
 * Date: 2020-08-03
 * Time: 10:54
 */

namespace Kukulis\Presta\Data;


class ImageType
{
    public $id_image_type;
    public $name;
    public $width;
    public $height;
    public $products;
    public $categories;
    public $manufacturers;
    public $suppliers;
    public $stores;
}