<?php
/**
 * TmpCategoryProduct.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.22
 * Time: 15.23
 */

namespace Kukulis\Presta\Data;


class TmpCategoryProduct
{
    public $position;
    public $sketis_code; // kategorijos kodas; rakto pusė
    public $nomnr; // produkto nomnr; antra rakto pusė
    public $id_category;
    public $id_product;
}