<?php

namespace Kukulis\Presta\Services;


class TmpImagesHelper
{
    public static function calculateImagePath($id) {
        $pathElems=[
            'img',
            'p',
        ];
        $idStr = "".$id;
        for ($i=0; $i<strlen($idStr); $i++ ) {
            $char = $idStr[$i];
            $pathElems[] = $char;
        }

        $imageItself = $id.'.jpg';
        $pathElems[] = $imageItself;
        return join("/", $pathElems);
    }
}