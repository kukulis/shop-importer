<?php

namespace Kukulis\Presta\Services;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Kukulis\Elko\Data\Vendor;
use Kukulis\Elko\Data\WebProductBase;
use Kukulis\Elko\ElkoDescriptionsHolder;
use Kukulis\Presta\Data\TmpCategory;
use Kukulis\Presta\Exception\ErrorException;
use Kukulis\Presta\Exception\SystemException;
use Kukulis\Presta\Helpers\CategoriesTreeParser;
use Kukulis\Presta\Helpers\ElkoToPrestaMapper;
use Kukulis\Providers\ElkoClient;
use Kukulis\Providers\ElkoDescriptionHandler;
use Psr\Log\LoggerInterface;

class ImportElkoDataService
{
    const STEP = 100;
    const CATEGORIES_STEP = 100;
    const ROOT_CATEGORY_CODE = 'root'; // TODO change later
    const SLEEP_TIME = 30;

    const DEFAULT_LOCALES_MAP = [
        'lt-lt' => 'lt',
        'en-gb' => 'en'
    ];

    /** @var LoggerInterface */
    private $logger;

    /** @var ElkoClient */
    private $elkoClient;

    /** @var PrestaDatabaseService */
    private $prestaDatabaseService;

    /** @var ElkoDescriptionsHolder */
    private static $nullHolder = null;

    /**
     * the correct map is put in the setter
     * @var array
     */
    private $localesMap = self::DEFAULT_LOCALES_MAP;

    private $debug = false;

    /**
     * ImportElkoDataService constructor.
     * @param LoggerInterface $logger
     * @param ElkoClient $elkoClient
     */
    public function __construct(
        LoggerInterface $logger,
        ElkoClient $elkoClient,
        PrestaDatabaseService $prestaDatabaseService
    ) {
        $this->logger = $logger;
        $this->elkoClient = $elkoClient;
        $this->prestaDatabaseService = $prestaDatabaseService;

        if (static::$nullHolder == null) {
            static::$nullHolder = new ElkoDescriptionsHolder(0);
        }
    }

    /**
     * @param $locale
     * @return bool|mixed|string
     */
    public function getIsoCode($locale)
    {
        $locale = strtolower($locale);

        if (array_key_exists($locale, $this->localesMap)) {
            $isoCode = $this->localesMap[$locale];
        } else {
            $isoCode = substr($locale, 0, 2);
        }
        return $isoCode;
    }

    /**
     * @param $file
     * @return int
     * @throws SystemException
     * @throws ErrorException
     */
    public function doImport($file, $isoCode)
    {
        try {
            $this->logger->debug('ImportElkoDataService.doImport called to file ' . $file);
            $this->prestaDatabaseService->getDatabase()->changeFile($file);

            $token = $this->elkoClient->getToken();

            $webProductsList = $this->getWebProductsList($token);

            $this->logger->debug('Extracted [' . count($webProductsList) . '] web products');

            $count = 0;

            // importuojam gabaliukais
            for ($i = 0; $i < count($webProductsList); $i += self::STEP) {
                $this->logger->debug('Working with a step ' . $i . ' limit ' . self::STEP);
                $part = array_slice($webProductsList, $i, self::STEP);
                // import part
                $count += $this->importProducts($part, $token, $isoCode);

                if ($this->debug && $i > 2) {
                    break;
                }
            }

            $this->prestaDatabaseService->getDatabase()->getPdo()->commit();
            return $count;
        } catch (GuzzleException $e) {
            throw new SystemException($e->getMessage());
        }
    }

    /**
     * @param WebProductBase[] $webProducts
     * @param string $token
     * @return int
     *
     * @throws SystemException
     * @throws ErrorException
     */
    private function importProducts($webProducts, $token, $isoCode)
    {
        // extract ids
        $ids = array_map(function ($p) {
            return $p->id;
        }, $webProducts);
        $now = date('Y-m-d H:i:s');
        $catalogProducts = $this->elkoClient->getCatalogProducts($token, $ids); // TODO give locale in parameters
        // descriptions
        $elkoDescriptions = $this->elkoClient->getProductsDescriptions($token, $ids); // TODO give locale in parameters

        $descriptionsHolders = ElkoDescriptionHandler::makeElkoDescriptionsHolders($elkoDescriptions);


        $tmpProducts = [];
        $tmpCategoriesProducts = [];
        $count = 0;
        foreach ($catalogProducts as $catalogProduct) {
            $descriptionHolder = $descriptionsHolders[$catalogProduct->id] ?? static::$nullHolder;

            $tmpProduct = ElkoToPrestaMapper::mapProduct($catalogProduct, $now, $isoCode, $descriptionHolder);
            $tmpCategoryProduct = ElkoToPrestaMapper::mapProductCategory($catalogProduct);
            $tmpProducts[] = $tmpProduct;
            $tmpCategoriesProducts[] = $tmpCategoryProduct;
            $count++;
        }

        $this->prestaDatabaseService->importTmpProducts($tmpProducts);
        $this->prestaDatabaseService->importTmpCategoriesProducts($tmpCategoriesProducts);

        return $count;
    }


    public function getWebProductsList($token)
    {
        return $this->getWebProductsListReal($token);
//        return $this->fakeGetWebProductsList($token);
    }

    /**
     * @param $token
     * @return WebProductBase[]
     * @throws ErrorException
     */
    public function getWebProductsListReal($token)
    {
        $webProductsList = $this->elkoClient->getWebProductList($token);

        if ($this->debug) {
            file_put_contents('tmp_web_products_list.json', json_encode($webProductsList, JSON_PRETTY_PRINT));
            file_put_contents('tmp_web_products_list.txt', serialize($webProductsList));
        }
        return $webProductsList;
    }

    /**
     * @param $token
     * @return Vendor[]
     * @throws ErrorException
     */
    public function getVendorsList($token)
    {
        $VendorsList = $this->elkoClient->getVendorList($token);

        if ($this->debug) {
            file_put_contents('tmp_vendors_list.json', json_encode($VendorsList, JSON_PRETTY_PRINT));
            file_put_contents('tmp_vendors_list.txt', serialize($VendorsList));
        }
        return $VendorsList;
    }

    /**
     * @return WebProductBase[]
     */
    public function fakeGetWebProductsList($token)
    {
//        $productsSerialized = file_get_contents('tmp_web_products_list.txt');
        $productsJson = file_get_contents('tmp_web_products_list_small.json');
        /** @var WebProductBase[] $products */
//        $products = unserialize($productsSerialized);
        $products = json_decode($productsJson);

        return $products;
    }

    /**
     * @param string $file
     * @return int
     * @throws ErrorException
     */
    public function importCategories($file, $locale)
    {
        $isoCode = $this->getIsoCode($locale);
        try {
            $this->logger->debug('Importing categories in to file ' . $file);
            $this->prestaDatabaseService->getDatabase()->changeFile($file);
            $token = $this->elkoClient->getToken();

            $VendorsList = $this->getVendorsList($token);
            $this->logger->debug('Extracted [' . count($VendorsList) . '] vendors (manufacturers)');

            $tree = $this->elkoClient->getCategoriesTree($token);

//            file_put_contents('categories_tree.txt', serialize($tree));

            $flatMap = CategoriesTreeParser::treeToFlatMap($tree, self::ROOT_CATEGORY_CODE);
            $today = date('Y-m-d H:i:s');

            /** @var TmpCategory[] $tmpCategories */
            $tmpCategories = [];
            foreach ($flatMap as $category) {
                $tmpCategories[] = ElkoToPrestaMapper::mapCategory($category, $today, $isoCode);
            }

            $count = 0;
            for ($i = 0; $i < count($tmpCategories); $i += self::CATEGORIES_STEP) {
                $part = array_slice($tmpCategories, $i, self::CATEGORIES_STEP);
                if ($this->prestaDatabaseService->importTmpCategories($part)) {
                    $count += count($part);
                }
            }

            $this->prestaDatabaseService->getDatabase()->getPdo()->commit();

            return $count;
        } catch (GuzzleException $e) {
            throw new SystemException($e->getMessage());
        }
    }

    /**
     * @param array $localesMap
     */
    public function setLocalesMap(array $localesMap): void
    {
        $this->localesMap = $localesMap;
    }

    /**
     * @param $file
     * @return int
     * @throws ErrorException
     */
    public function importAmountsAndPrices($file)
    {
        $this->logger->debug('Importing amounts in to file ' . $file);
        $this->prestaDatabaseService->getDatabase()->changeFile($file);
        try {
            $token = $this->elkoClient->getToken();


            $webProductsList = $this->getWebProductsList($token);
            $ids = array_map(function ($b) {
                return $b->id;
            }, $webProductsList);

            $debug = false;
            $count = 0;
            for ($i = 0; $i < count($ids); $i += self::STEP) {
                if ($debug && $count > 0) {
                    break;
                }
                $this->logger->debug('Importing prices amounts step ' . $i . ' limit ' . self::STEP);
                $part = array_slice($ids, $i, self::STEP);
                $count += $this->importAmountsAndPricesPart($token, $part);
            }

            return $count;
        } catch (GuzzleException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param int[] $part
     * @return int
     * @throws ErrorException
     */
    public function importAmountsAndPricesPart(string $token, array $part)
    {
        $amounts = $this->elkoClient->getAvailabilityAndPrice($token, $part);
        $tmpAmounts = [];
        foreach ($amounts as $availabilityElement) {
            $tmpAmounts[] = ElkoToPrestaMapper::mapAmountsElement($availabilityElement);
        }

        $this->prestaDatabaseService->insertTmpPricesAmounts($tmpAmounts);
        return count($tmpAmounts);
    }

    /**
     * @param $file
     * @return int
     * @throws ErrorException
     */
    public function importImages($file)
    {
        $this->logger->debug('Importing images data in to file ' . $file);
        $this->prestaDatabaseService->getDatabase()->changeFile($file);
        try {
            $token = $this->elkoClient->getToken();
            $count = 0;

            $webProductsList = $this->getWebProductsList($token);
            $ids = array_map(function ($b) {
                return $b->id;
            }, $webProductsList);

            for ($i = 0; $i < count($ids); $i += self::STEP) {
                $this->logger->debug('Importing images step ' . $i . ' limit ' . self::STEP);
                $part = array_slice($ids, $i, self::STEP);

                $count += $this->importImagesPartWithRepeatingCycle($token, $part);
            }

            return $count;
        } catch (GuzzleException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    private function importImagesPartWithRepeatingCycle($token, array $part): int
    {
        $requestOk = false;
        while (!$requestOk) {
            $requestOk = true;
            try {
                return $this->importImagesPart($token, $part);
            } catch (ClientException $e) {
                if ($e->getResponse()->getStatusCode() == 429) {
                    $this->logger->warning('429 error: ' . $e->getMessage());
                    $this->logger->info('Repeating same request again in ' . self::SLEEP_TIME . ' .. ');
                    sleep(self::SLEEP_TIME);
                    $this->logger->info('.. continuing');
                    $requestOk = false;
                } else {
                    $this->logger->error('Other client error: ' . $e->getMessage());
                }
            }
        }

        return 0;
    }

    /**
     * @param string $token
     * @param array $part
     * @return int
     * @throws GuzzleException
     * @throws ErrorException
     */
    public function importImagesPart(string $token, array $part)
    {
        $mediaDatas = $this->elkoClient->getProductsMedias($token, $part);

        $tmpImages = [];
        foreach ($mediaDatas as $mediaData) {
            $tmpImagesPart = ElkoToPrestaMapper::mapTmpImage($mediaData);
            $tmpImages = array_merge($tmpImages, $tmpImagesPart);
        }

        for ($i = 0; $i < count($tmpImages); $i += self::STEP) {
            $tmpImagesPartForInsert = array_slice($tmpImages, $i, self::STEP);
            $this->prestaDatabaseService->insertTmpImages($tmpImagesPartForInsert);
        }

        return count($tmpImages);
    }
}