<?php
/**
 * PrestaTreeService.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.22
 * Time: 16.25
 */

namespace Kukulis\Presta\Services;


use Kukulis\Presta\Data\CategoryTreeItem;
use Psr\Log\LoggerInterface;

class PrestaTreeService
{
    /** @var LoggerInterface */
    private $logger;

    /** @var PrestaDatabaseService */
    private $prestaDatabaseService;

    /** @var CategoryTreeItem[] */
    private $_recollectedItems=[];

    /**
     * PrestaTreeService constructor.
     * @param LoggerInterface $logger
     * @param PrestaDatabaseService $prestaDatabaseService
     */
    public function __construct(LoggerInterface $logger, PrestaDatabaseService $prestaDatabaseService)
    {
        $this->logger = $logger;
        $this->prestaDatabaseService = $prestaDatabaseService;
    }

    public function generateTree() {
        $treeItems = $this->prestaDatabaseService->loadCategoryTreeItems();
//        foreach ($treeItems as $item ) {
//            $this->logger->debug( print_r ($item, true));
//        }

        $this->logger->debug ( 'Loaded '.count($treeItems). ' categories ');

        /** @var CategoryTreeItem[] $itemMap key is category_id */
        $itemMap = [];
        foreach ($treeItems as $item ) {
            $itemMap[$item->id_category] = $item;
        }

        // assign children
        foreach ($treeItems as $item ) {
            if ( !array_key_exists($item->id_parent, $itemMap)) {
                $this->logger->debug('Kategorijai '.$item->id_category.' nerasta tėvinė kategorija '.$item->id_parent );
                continue;
            }
            $itemMap[$item->id_parent]->children[] = $item;
        }

        // find root
        $root = $treeItems[0];
        while ( array_key_exists($root->id_parent, $itemMap)) {
            $root = $itemMap[$root->id_parent];
        }

//        // =================================================
//        // =========== Testavimui ==========================
//        // surinkim vėl kategorijas iš medžio į vieną sąrašą
//        $this->_recollectedItems=[];
//        $this->recollectItems($root);
//
//        // palyginkim surinktas kategorijas su pradiniu užkrautu sąrašu.
//        /** @var CategoryTreeItem $recolectedMap */
//        $recollectedMap = [];
//        foreach ($this->_recollectedItems as $item) {
//            $recollectedMap[$item->id_category] =$item;
//        }
//
//        $okCount = 0;
//        $failCount = 0;
//
//        foreach ( $treeItems as $item) {
//            if ( !array_key_exists($item->id_category, $recollectedMap)) {
//                $this->logger->debug('Kategorija '.$item->id_category.' nepateko į rezultatų masyvą ');
//                $failCount++;
//            }
//            else {
//                $okCount++;
//            }
//        }
//
//        $this->logger->debug('Nepateko į medį kategorijų '.$failCount );
//        $this->logger->debug('Pateko į medį kategorijų '.$okCount );
//        // =========== Testavimo pabaiga ===================
//        // =================================================



        self::assignLeftRight(1, $root, 1 );
        $this->logger->debug ( 'left right assigned, writting to db' );


//        //=====================================================
//        // ============= dar vienas testavimas ================
//        foreach ($treeItems as $item) {
//            if( $item->nleft == 0 ) {
//                $this->logger->warning('Kategorijai '.$item->id_category.' kažkodėl nesuskaičiuotas nleft' );
//            }
//
//            if ( $item->id_category == 1269) {
//                $this->logger->debug ( 'Kategorijos '.$item->id_category.' nleft='.$item->nleft );
//            }
//        }
//
//        // ============ dar vieno testavimo pabaiga ===========
//        // ====================================================

        // write categories back
        $step = 100;
        for ($i=0; $i < count($treeItems); $i+= $step) {
            $this->logger->debug('writting from '.$i );
            $subitems = array_slice($treeItems, $i, $step);
            $this->prestaDatabaseService->storeTreeItems($subitems);
        }
        $this->logger->debug('writting done' );
    }

    /**
     * @param int $givenLeft
     * @param CategoryTreeItem $item
     * @return int
     */
    public static function assignLeftRight( $givenLeft, CategoryTreeItem & $item, $depth ) {
        $item->nleft = $givenLeft;
        $item->level_depth = $depth;
        foreach ($item->children as $child ) {
            $givenLeft++;
            $givenLeft = self::assignLeftRight($givenLeft, $child, $depth+1);
        }

        $givenLeft++;
        $item->nright = $givenLeft;
        return $givenLeft;
    }


    /**
     * @param CategoryTreeItem $node
     */
    public function recollectItems( CategoryTreeItem $node) {
        $this->_recollectedItems[] = $node;
        foreach ($node->children as $child ) {
            $this->recollectItems($child);
        }
    }

}