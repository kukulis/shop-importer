<?php

namespace Kukulis\Presta\Services\Mysql;


use Kukulis\Presta\Exception\SystemException;
use Kukulis\Presta\Services\IDatabase;

class Database implements IDatabase
{
    /** @var \PDO */
    private $pdo;
    /** @var string */
    private $host;
    /** @var string */
    private $database;
    /** @var string */
    private $user;
    /** @var string */
    private $password;

    /**
     * Database constructor.
     * @param string $host
     * @param string $database
     * @param string $user
     * @param string $password
     */
    public function __construct(string $host, string $database, string $user, string $password)
    {
        $this->host = $host;
        $this->database = $database;
        $this->user = $user;
        $this->password = $password;
    }


    /**
     * @return \PDO
     * @throws SystemException
     */
    public function getPdo() {
        if ($this->pdo == null ) {
            $this->pdo = $this->connectPdo();
        }
        return $this->pdo;
    }

    /**
     * @return \PDO
     * @throws SystemException
     */
    public function connectPdo() {
        try {
            $connectionString = sprintf("mysql:host=%s;dbname=%s", $this->host, $this->database);
            $pdo = new \PDO(
                $connectionString,
                $this->user, $this->password);

            // klaidos visos tegul virsta exception'ais
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (\PDOException $e) {
            throw new SystemException($e->getMessage());
        }
    }

    public function changeFile($fileName)
    {
        // not needed for this class
    }
}