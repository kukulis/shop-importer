<?php

namespace Kukulis\Presta\Services;

use \Exception;
use Gumlet\ImageResize;
use Kukulis\Presta\Data\TmpImage;
use Psr\Log\LoggerInterface;


/**
 * Class ImportImagesService
 *
 * This is universal service
 *
 */
class ImportImagesService
{
    const STEP = 200;

    /** @var string[]
     */
    const SIZES_SUFFIXES = [
        '-cart_default.jpg',
        '-home_default.jpg',
        '-large_default.jpg',
        '-medium_default.jpg',
        '-small_default.jpg',
    ];

    /** @var LoggerInterface */
    private $logger;

    /** @var PrestaDatabaseService */
    private $prestaDatabaseService;

    /**
     * ImportImagesService constructor.
     * @param LoggerInterface $logger
     * @param PrestaDatabaseService $prestaDatabaseService
     */
    public function __construct(
        LoggerInterface $logger,
        PrestaDatabaseService $prestaDatabaseService
    ) {
        $this->logger = $logger;
        $this->prestaDatabaseService = $prestaDatabaseService;
    }


    public function calculateImagesPaths()
    {
        $step = 200;
        $offset = 0;
        /** @var TmpImage[] $tmpImages */
        $count = 0;
        do {
            $this->logger->debug('from ' . $offset . ' count=' . $count);
            $tmpImages = $this->prestaDatabaseService->loadTmpImages($offset, $step);
            foreach ($tmpImages as $tmpImage) {
                if (empty($tmpImage->destination_path)) {
                    if (empty($tmpImage->id_image)) {
                        $this->logger->notice('Paveikslėliui ' . $tmpImage->sketis_image_id . ' nepriskirtas presta id ');
                        continue;
                    }
                    $tmpImage->destination_path = TmpImagesHelper::calculateImagePath($tmpImage->id_image);
                    $count++;
                }
                $this->prestaDatabaseService->storeDestinationPaths($tmpImages);
            }

            $offset += $step;
        } while (!empty($tmpImages));

        return $count;
    }

    /**
     * @param string $urlPrefix
     * @param string $shopBasePath
     * @return int
     */
    public function downloadImages($urlPrefix, $shopBasePath)
    {
        $step = self::STEP;
        $offset = 0;
        $count = 0;

        $lastId = '';
        do {
            $this->logger->debug('from ' . $offset . ' count=' . $count);
            $tmpImages = $this->prestaDatabaseService->loadOldVersionsTmpImages($lastId, $step);
            foreach ($tmpImages as $tmpImage) {
                $lastId = $tmpImage->sketis_image_id;

                if (empty($tmpImage->destination_path)) {
                    $this->logger->notice('Paveikslėliui ' . $tmpImage->sketis_image_id . ' nepaskaičiuotas kelias ');
                } else {
                    $sourceUrl = $urlPrefix . $tmpImage->uri;
                    $destinationPath = $shopBasePath . '/' . $tmpImage->destination_path;
                    try {
                        $imageContent = file_get_contents($sourceUrl);
                        if (strlen($imageContent) < 10) {
                            $this->logger->warning('Loaded content size ' . strlen($imageContent));
                            $this->logger->warning('Source path = ' . $sourceUrl);
                        }

                        self::createDirectory($destinationPath);
                        file_put_contents($destinationPath, $imageContent);
                        $tmpImage->uploaded_version = $tmpImage->version;
//                    $this->logger->debug('sourceUrl='.$sourceUrl.'  destinationPath='.$destinationPath );
                        $count++;
                        $dir = dirname($destinationPath);

                        // tegul būna kopijavimas, nes iki resaizinimo vis vien kad rodytų paveikslėlius
                        foreach (self::SIZES_SUFFIXES as $suffix) {
                            $copyPath = $dir . '/' . $tmpImage->id_image . $suffix;
                            if (!file_exists($copyPath)) { // neoverridinam esančių, nes esančius resaizas užkopijuos vėliau
                                copy($destinationPath, $copyPath);
                            }

                        }
                    } catch (Exception $e) {
                        $msg = $e->getMessage();
                        $msg = str_replace('Warning', 'W a r n i n g', $msg); // kad neregistruotų į o3
                        $this->logger->error('Klaida įkeliant paveikslėlį: ' . $msg);
                    }
                }
            }
            if (!empty($tmpImages)) {
                $this->prestaDatabaseService->storeUploadedVersions($tmpImages); // kadangi čia darom pakeitimus į db, tai ankstesnis offset, limit selektas netenka prasmės
            }
            $offset += $step;
        } while (!empty($tmpImages));

        return $count;
    }

    /**
     * @param string $shopDir
     * @param int $portionSize
     * @return int
     * @throws \Gumlet\ImageResizeException
     */
    public function resizeImages($shopDir, $portionSize) {
        $this->logger->debug('Resizing images started');

        $imageTypes = $this->prestaDatabaseService->loadProductsImageTypes();

        $count=0;
        $lastId = 0;
        $tmpImages = $this->prestaDatabaseService->loadUnresizedTmpImages($lastId, $portionSize );
        while ( count($tmpImages) > 0 ) {
            $this->logger->debug('Loaded tmp images portion '.count($tmpImages).' from id '.$lastId);
            $this->logger->debug('Resized so far '.$count);
            foreach ($tmpImages as $tmpImage ) {
                $lastId = $tmpImage->sketis_image_id;

                try {
                    if (empty($tmpImage->destination_path)) {
                        $this->logger->debug('Paveikslėliui ' . $tmpImage->sketis_image_id . ' tuščias kelias');
                        continue;
                    }

                    // 1) calculate path of the source image
                    $sourcePath = $shopDir . '/' . $tmpImage->destination_path;
                    if (!file_exists($sourcePath)) {
                        $this->logger->notice('Neegzistuoja šaltinio paveikslėlis ' . $sourcePath);

                        continue;
                    }
                    foreach ($imageTypes as $imageType) {
                        // 2) calculate path of the destination image
                        // a) get dir
                        $dir = dirname($sourcePath);
                        // b) calculate image name with the type
                        $destinationPath = $dir . '/' . $tmpImage->id_image . '-' . $imageType->name . '.jpg';

//                        $this->logger->debug('resizing to ' . $destinationPath);
                        // 3) resize image
//                        $this->logger->debug('Trying to resize ' . $sourcePath);
                        $imageResize = new ImageResize($sourcePath);
                        $imageResize->resizeToBestFit($imageType->width, $imageType->height);
                        $imageResize->save($destinationPath);

                    }
                    // 4) update resized_version
                    $this->prestaDatabaseService->updateImageResizeVersion($tmpImage->sketis_image_id, $tmpImage->version);
                    $count++;
                } catch (\Gumlet\ImageResizeException $e ) {
                    $this->logger->notice ( 'Klaida keičiant dydį paveikslėliui '.$tmpImage->sketis_image_id. ' [' . $tmpImage->destination_path.'] '.$e->getMessage());
                } catch ( \Exception $e ) {
                    $this->logger->error ( 'Klaida keičiant dydį paveikslėliui '. get_class($e) .'   '.$tmpImage->sketis_image_id. ' [' . $tmpImage->destination_path.'] '.$e->getMessage());
                }
            }
            $tmpImages = $this->prestaDatabaseService->loadUnresizedTmpImages($lastId, $portionSize );
        }
        return $count;
    }


    /**
     * @param $path
     */
    public static function createDirectory($path)
    {
        $dir = dirname($path);
        if (!file_exists($dir)) {
            @mkdir($dir, 0775, true);
        }
    }
}