<?php
/**
 * FileDatabase.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.21
 * Time: 14.00
 */

namespace Kukulis\Presta\Services\File;


use Kukulis\Presta\Services\IDatabase;

class FileDatabase implements IDatabase
{
    /**
     * @var string
     */
    private $fileName;

    /** @var \PDO */
    private $_pdo;
    /**
     * FileDatabase constructor.
     */
    public function __construct($defaultFileName)
    {
        $this->fileName = $defaultFileName;
    }

    /**
     * @param $fileName
     */
    public function changeFile($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return \PDO
     */
    public function connectPdo()
    {
        $pdo = new PDOFile($this->fileName);

        return $pdo;
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        if ($this->_pdo == null ) {
            $this->_pdo = $this->connectPdo();
        }
        return $this->_pdo;
    }
}