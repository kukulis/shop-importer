<?php

namespace Kukulis\Presta\Services\File;

class PDOFile extends \PDO
{
    private $file;

    /**
     * PDOFile constructor.
     */
    public function __construct($filePath)
    {
        $this->file= fopen( $filePath, 'w');
    }

    /**
     * @param string $statement
     *
     * @return bool
     */
    public function exec($statement)
    {
        fwrite( $this->file, $statement .";\n" );
        return true;
    }

    /**
     * @param string $string
     * @param int $parameter_type
     *
     * @return mixed|string
     */
    public function quote($string, $parameter_type = \PDO::PARAM_STR)
    {
        if ( $parameter_type == \PDO::PARAM_STR) {
            $string = str_replace(["\\", "'",'--',],["\\\\", "\\'", '', ], $string);
            return "'" . $string . "'";
        }
        else
        {
            return $string;
        }
    }

    /**
     *
     */
    public function close() {
        fclose( $this->file );
    }

    /**
     * @return bool|void
     */
    public function commit()
    {
        fflush($this->file);
    }
}