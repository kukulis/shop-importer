<?php

namespace Kukulis\Presta\Services;


interface IDatabase
{
    /**
     * @param $fileName
     */
    public function changeFile($fileName);

    /**
     * @return \PDO
     */
    public function connectPdo();

    /**
     * @return \PDO
     */
    public function getPdo();

}