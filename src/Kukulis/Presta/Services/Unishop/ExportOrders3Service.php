<?php
/**
 * ExportOrders3Service.php
 * Created by Giedrius Tumelis.
 * Date: 2020-09-30
 * Time: 10:39
 */

namespace Kukulis\Presta\Services\Unishop;


//use Kotas\Core\Utils;
//use Kotas\Pragma\Exceptions\SketisSystemException;
//use Kotas\Pragma\Exceptions\SketisValidateException;
//use Kotas\Pragma\Exceptions\SketisWarningException;
use Kukulis\Helpers\Utils;
use Kukulis\Presta\Exception\SystemException;
use Kukulis\Presta\Exception\ValidationException;
use Kukulis\Presta\Exception\WarningException;
use Kukulis\Presta\Services\PrestaDatabaseService;
use Psr\Log\LoggerInterface;
use Sketis\B2b\Client\SketisB2bOrdersClient;
use \Exception;

class ExportOrders3Service
{
    const MAX_ERROR_COUNT = 10;

    /** @var LoggerInterface */
    private $logger;

    /** @var PrestaDatabaseService */
    private $prestaDatabaseService;

    /** @var SketisB2bOrdersClient */
    private $b2bOrdersClient;

    private $clientCode;

    private $_transportNomnr;

    /**
     * ExportOrdersService constructor.
     * @param LoggerInterface $logger
     * @param PrestaDatabaseService $prestaDatabaseService
     */
    public function __construct(
        LoggerInterface $logger,
        PrestaDatabaseService $prestaDatabaseService,
        SketisB2bOrdersClient $b2bOrdersClient,
        $clientCode
    )
    {
        $this->logger = $logger;
        $this->prestaDatabaseService = $prestaDatabaseService;
        $this->b2bOrdersClient = $b2bOrdersClient;
        $this->clientCode = $clientCode;
    }

    /**
     * @param mixed $transportNomnr
     */
    public function setTransportNomnr($transportNomnr): void
    {
        $this->_transportNomnr = $transportNomnr;
    }

    public function exportOrders( $days) {
        $fromDate = date('Y-m-d', strtotime('-' . $days . ' days', time()));

        $ids = $this->prestaDatabaseService->findOrders($fromDate);

        $this->logger->debug('Found '.count($ids). ' orders' );

        foreach ($ids as $id ) {
            $sketisOrderStatus = $this->prestaDatabaseService->getOrCreateSketisOrderStatus($id);
            try {
                $order = $this->prestaDatabaseService->loadOrderWithAllDetails($id);

                $usedClientCode = $this->clientCode;

                // TODO review this later
                // instead of taking the whole note, need to extract the code from the notes
//                if ( !empty($order->customer->note )) {
//                    $usedClientCode = $order->customer->note;
//                }

                $deliveryInfo = PrestaOrderMapper::mapToDeliveryInfo($order);
                $order3 = PrestaOrderMapper::mapToSketisOrder3($order, $this->_transportNomnr);
                $order3->setDeliveryInfo($deliveryInfo);

                // missings
                $missingAmounts = $this->b2bOrdersClient->registerOrder($usedClientCode, $order3);

                if (!empty($missingAmounts)) {
                    $amountsStr = ' Trūksta:' . Utils::map2string($missingAmounts);
                    $this->logger->warning($amountsStr);
                    $sketisOrderStatus->missings=$amountsStr;
                }
                $sketisOrderStatus->transfered=1;
            }
            catch ( SystemException | ValidationException | WarningException $e ) {
                $sketisOrderStatus->response_msg = $e->getMessage();
                $this->logger->error('Klaida perduodant užsakymą:'.$e->getMessage());
                // kadangi tai sisteminė klaida tai nežymime klaidos požymio.
                $sketisOrderStatus->error_count++;
                if ( $sketisOrderStatus->error_count > self::MAX_ERROR_COUNT ) {
                    $sketisOrderStatus->error = 1;
                }
            }
            catch (Exception $e) {
                $sketisOrderStatus->error=1;
                $sketisOrderStatus->response_msg = $e->getMessage();
                $this->logger->error('Klaida perduodant užsakymą:'.$e->getMessage());
            }
            finally {
                $this->prestaDatabaseService->upsertSketisOrderStatus($sketisOrderStatus);
            }
        }
    }
}