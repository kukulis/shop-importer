<?php

namespace Kukulis\Presta\Services\Unishop;

use Kukulis\Helpers\Utils;
use Kukulis\Presta\Data\Carrier;
use Kukulis\Presta\Data\Customer;
use Kukulis\Presta\Data\Order;
use Kukulis\Presta\Data\OrderDetail;
use Kukulis\Presta\Exception\MappingException;
use Sketis\B2b\Common\Data\Rest\DeliveryInfo;
use Sketis\B2b\Common\Data\Rest\Order3;
use Sketis\B2b\Common\Data\Rest\Order3Line;

class PrestaOrderMapper
{
    const PAYMENT_CARD = 'CARD';
    const PAYMENT_COD = 'COD';
    const PAYMENT_EBANK = 'EBANK';
    const PAYMENT_INVOICE = 'INVOICE';
    const PAYMENT_NEOPAY = 'NEOPAY';
    const PAYMENT_PAYSERA = 'PAYSERA';
    const DELIVERY_DPD = 'DPD';

    const DELIVERY_DPD_COURIER = 'DPD_COURIER';
    const DELIVERY_DPD_TERMINAL = 'DPD_TERMINAL';

    const COD_PAYMENTS = [self::PAYMENT_CARD, self::PAYMENT_COD];

    /**
     * @param Order $order presta order
     * @return Order3 sketis order
     */
    public static function mapToSketisOrder3(Order $order, $transportProductNomnr)
    {
        $order3 = new Order3();

        $order3->setOrderNumber($order->reference);
        $order3->setCurrency($order->cart->currency->iso_code);
        $order3->setTotalSum($order->total_paid);

        foreach ($order->lines as $orderDetail) {
            $line = self::mapOrder3Line($orderDetail);
            $order3->addLine($line);
        }

        $deliveryPrice = $order->total_shipping;
        // add transport line
        if ($deliveryPrice > 0) {
            $transportLine = new Order3Line();
            $transportLine->setSku($transportProductNomnr);
            $transportLine->setAmount(1);
            $transportLine->setPrice($deliveryPrice);
            $order3->addLine($transportLine);
        }

        return $order3;
    }

    /**
     * @param $module
     * @return string
     */
    public static function getPaymentTypeByModule($module)
    {
        switch ($module) {
            case Order::CHECKPAYMENT_MODULE:
                return self::PAYMENT_INVOICE;
            case Order::WIREPAYMENT_MODULE :
                return self::PAYMENT_EBANK;
            case Order::COD_MODULE:
                return self::PAYMENT_COD;
            case Order::PAYSERA_MODULE_NAME:
                return self::PAYMENT_PAYSERA;
            default:
                return self::PAYMENT_INVOICE;
        }
    }

    /**
     * Map order line.
     *
     * @param OrderDetail $orderDetails
     * @return Order3Line
     */
    public static function mapOrder3Line(OrderDetail $orderDetails)
    {
        $line = new Order3Line();
        $line->setAmount($orderDetails->product_quantity);
        $line->setSku($orderDetails->product_reference);
        $line->setPrice($orderDetails->unit_price_tax_incl);
        return $line;
    }

    /**
     * @return DeliveryInfo
     */
    public static function mapToDeliveryInfo(Order $order)
    {
        $deliveryInfo = new DeliveryInfo();
        $address = $order->addressDelivery;
        if (empty($address)) {
            $address = $order->addressInvoice;
        }

        /** @var Customer $customer */
        $customer = Utils::nonEmpty([$address->customer, $order->customer], new Customer());

        $deliveryInfo->countryCode = $address->country->iso_code;
        $deliveryInfo->deliveryCode = self::getDeliveryCode($order);
        $deliveryInfo->deliveryTypeChosen = self::getSpecificDeliveryCode($order);
        $notesArr = [];
        foreach ($order->orderMessages as $msg) {
            $notesArr[] = $msg->message;
        }
        $notesStr = join(" ", $notesArr);
        $deliveryInfo->deliveryNotes = $notesStr;
        $deliveryInfo->paymentCode = self::getPaymentTypeByModule($order->module);
        $deliveryInfo->paymentNotes = ""; // ??
        $deliveryInfo->deliveryAddress = trim($address->city . ' ' . $address->address1 . "\n" . $address->address2);
        $deliveryInfo->terminalCode = self::getTerminalCode($order);
        $deliveryInfo->terminalAddress = self::getTerminalAddress($order);
        $deliveryInfo->buyerName = trim($address->firstname . ' ' . $address->lastname);
        // alternatyva:
        if (empty($deliveryInfo->buyerName)) {
            $deliveryInfo->buyerName = $customer->firstname . ' ' . $customer->lastname;
        }
        $deliveryInfo->buyerEmail = $customer->email;
        $deliveryInfo->buyerPhone = Utils::nonEmpty([$address->phone_mobile, $address->phone]);
        $deliveryInfo->buyerCellPhone = $address->phone_mobile;
        $deliveryInfo->buyerPostCode = $address->postcode;
        $deliveryInfo->buyerStreet = trim($address->address1 . "\n" . $address->address2);
        $deliveryInfo->buyerCity = $address->city;

        if (array_search($deliveryInfo->paymentCode, self::COD_PAYMENTS) !== false) {
            $deliveryInfo->codValue = $order->total_paid_tax_incl;
        } else {
            $deliveryInfo->codValue = 0;
        }
//
        $deliveryInfo->companyName = $customer->company;
        $deliveryInfo->companyCode = $customer->company;
        $deliveryInfo->companyVatCode = ""; // TODO?
        $deliveryInfo->companyStreet = "";
        $deliveryInfo->companyZip = "";
        $deliveryInfo->companyCity = "";
        $deliveryInfo->companyCountry = $address->country->iso_code;
        $deliveryInfo->companyPhone = "";

        return $deliveryInfo;
    }

    /**
     * @param Order $order
     * @return string
     */
    public static function getTerminalCode(Order $order)
    {
        if ($order->carrier->external_module_name == Carrier::DPD_EXTERNAL_MODULE_NAME) {
            self::validateDpd($order);
            if (is_object($order->dpdOrder->deliveryPoint)) {
                return $order->dpdOrder->deliveryPoint->identifier;
            }
            return null;
        } else {
            // for other carriers code here
            return null;
        }
    }

    public static function validateDpd(Order $order)
    {
        if (empty($order->dpdOrder)) {
            throw new MappingException('No dpd order for order ' . $order->reference . ' though module name is ' . $order->carrier->external_module_name);
        }
        if (empty($order->dpdOrder->deliveryPoint) && $order->dpdOrder->carrier_type != 'carrier') {
            throw new MappingException('No no delivery point found by id ' . $order->dpdOrder->id_dpd_delivery_point . ' Order number:' . $order->reference);
        }
    }

    public static function getTerminalAddress(Order $order): ?string
    {
        if ($order->carrier->external_module_name == Carrier::DPD_EXTERNAL_MODULE_NAME) {
            self::validateDpd($order);
            if (is_object($order->dpdOrder->deliveryPoint)) {
                return $order->dpdOrder->deliveryPoint->address;
            }
            return null;
        } else {
            // for other carriers code here
            return null;
        }
    }

    public static function getDeliveryCode(Order $order)
    {
        if ($order->carrier->external_module_name == Carrier::DPD_EXTERNAL_MODULE_NAME) {
            return self::DELIVERY_DPD;
        } elseif($order->carrier->external_module_name == 'OFFICE') {
            return 'OFFICE';
        }
        else {
            $name = $order->carrier->name;
            if ($name == 'OFFICE') {
                $name = 'OFICE'; // hardkodas, nes skėtyje pas mus yra OFICE, o ne OFFICE ( gal jau pataisyta skėčio pusėje? )
            }
            return $name;
        }
    }

    public static function getSpecificDeliveryCode(Order $order)
    {
        if ($order->carrier->external_module_name == Carrier::DPD_EXTERNAL_MODULE_NAME) {
            if ($order->dpdOrder->carrier_type == 'carrier') {
                return self::DELIVERY_DPD_COURIER;
            } else {
                if ($order->dpdOrder->carrier_type == 'delivery_point') {
                    return self::DELIVERY_DPD_TERMINAL;
                }
            }
        }

        // kitu atveju nieko neperduodam
        return '';
    }
}