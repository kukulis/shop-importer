<?php
/**
 * PrestaDatabaseService.php
 * Created by Giedrius Tumelis.
 * Date: 20.5.7
 * Time: 10.18
 */

namespace Kukulis\Presta\Services;


use Kukulis\Helpers\Utils;
use Kukulis\Presta\Data\Address;
use Kukulis\Presta\Data\Carrier;
use Kukulis\Presta\Data\Cart;
use Kukulis\Presta\Data\CategoryTreeItem;
use Kukulis\Presta\Data\Country;
use Kukulis\Presta\Data\Currency;
use Kukulis\Presta\Data\Customer;
use Kukulis\Presta\Data\CustomerMessage;
use Kukulis\Presta\Data\DpdDeliveryPoint;
use Kukulis\Presta\Data\DpdOrder;
use Kukulis\Presta\Data\ImageType;
use Kukulis\Presta\Data\Order;
use Kukulis\Presta\Data\OrderCarrier;
use Kukulis\Presta\Data\OrderDetail;
use Kukulis\Presta\Data\SketisOrderStatus;
use Kukulis\Presta\Data\TmpCategory;
use Kukulis\Presta\Data\TmpCategoryProduct;
use Kukulis\Presta\Data\TmpImage;
use Kukulis\Presta\Data\TmpProduct;
use Kukulis\Presta\Data\TmpProductTag;
use Kukulis\Presta\Data\TmpSketisPricesAmounts;
use Kukulis\Presta\Data\TmpSpecialPrice;
use Kukulis\Presta\Data\TmpTag;
use Kukulis\Presta\Exception\ErrorException;
use Kukulis\Presta\Exception\SystemException;
use Kukulis\Presta\Services\Mysql\Database;
use \PDO;
use \Closure;
use \PDOException;
use Psr\Log\LoggerInterface;

class PrestaDatabaseService
{
    /** @var LoggerInterface */
    private $logger;

    /** @var IDatabase */
    private $database;

    /**
     * @var string
     */
    private $prefix = "ps_";

    /** @var \Closure */
    private $_quoter;

    /**
     * PrestaDatabaseService constructor.
     * @param LoggerInterface $logger
     * @param Database $database
     */
    public function __construct(LoggerInterface $logger, IDatabase $database)
    {
        $this->logger = $logger;
        $this->database = $database;
    }

    /**
     * @return \Closure
     * @throws SystemException
     */
    private function getQuoter()
    {
        if ($this->_quoter == null) {
            $pdo = $this->database->getPdo();
            $this->_quoter = function ($str) use ($pdo) {
                return $pdo->quote($str);
            };
        }

        return $this->_quoter;
    }

    /**
     * @return IDatabase
     */
    public function getDatabase(): IDatabase
    {
        return $this->database;
    }

    /**
     * @param TmpSketisPricesAmounts[] $spas
     * @return int
     * @throws SystemException
     */
    public function insertTmpPricesAmounts($spas)
    {
        $pdo = $this->database->getPdo();
        $valuesArr = [];
        foreach ($spas as $spa) {
            $qNomnr = $pdo->quote($spa->nomnr);
            $valuesLine = "($qNomnr, {$spa->amount}, {$spa->price}, {$spa->wholesale_price}, {$spa->id_product_attribute})";
            $valuesArr[] = $valuesLine;
        }

        $valuesStr = join(",\n", $valuesArr);

        $sql = /** @lang MySQL */
            "insert into tmp_sketis_prices_amounts (nomnr, amount, price, wholesale_price, id_product_attribute)
        VALUES $valuesStr
        on duplicate key update  amount=values(amount), price=values(price), wholesale_price=values(price)";

        return $pdo->exec($sql);
    }

    /**
     * @return CategoryTreeItem[]
     */
    public function loadCategoryTreeItems()
    {
        $pdo = $this->getDatabase()->getPdo();

        $sql = /** @lang MySQL */
            "select
             id_category,
             id_parent,
             level_depth,
             nleft,
             nright,
             date_add,
             date_upd        
             from ps_category order by position
        ";

        /** @var CategoryTreeItem[] $treeItems */
        $treeItems = $pdo->query($sql)->fetchAll(PDO::FETCH_CLASS, CategoryTreeItem::class);

        return $treeItems;
    }

    /**
     * @param CategoryTreeItem[] $items
     * @return int
     * @throws SystemException
     */
    public function storeTreeItems($items)
    {
        $pdo = $this->database->getPdo();
        $valuesLines = [];
        foreach ($items as $item) {
            $lineArr = [
                $item->id_category,
                $item->level_depth,
                $item->nleft,
                $item->nright,
                $item->id_parent,
                $pdo->quote($item->date_add),
                $pdo->quote($item->date_upd)
            ];
            $line = '(' . join(',', $lineArr) . ')';
            $valuesLines[] = $line;
        }
        $valuesStr = join(",\n", $valuesLines);

        $sql = /** @lang MySQL */
            "insert into ps_category (id_category, level_depth, nleft, nright, id_parent, date_add, date_upd)
          values $valuesStr
         on duplicate key update level_depth=values(level_depth), nleft=values(nleft), nright=values(nright)";

        $this->logger->debug('storeTreeItems: sql=' . $sql);

        return $pdo->exec($sql);
    }

    /**
     * @param TmpProduct[] $tmpProducts
     * @param Closure $quoter
     * @return string
     */
    public function generateImportTmpProducts($tmpProducts, Closure $quoter)
    {
        $values = [];
        foreach ($tmpProducts as $tp) {
            $q_nomnr = Utils::nullValueOrQuoter($tp->nomnr, $quoter);
            $q_id_supplier = Utils::nullValueOrQuoter($tp->id_supplier, $quoter);
            $q_supplier = Utils::nullValueOrQuoter($tp->supplier, $quoter);
            $q_id_manufacturer = Utils::nullValueOrQuoter($tp->id_manufacturer, $quoter);
            $q_id_category_default = Utils::nullValueOrQuoter($tp->id_category_default, $quoter);
            $q_id_shop_default = Utils::nullValueOrQuoter($tp->id_shop_default, $quoter);
            $q_id_tax_rules_group = Utils::nullValueOrQuoter($tp->id_tax_rules_group, $quoter);
            $q_on_sale = Utils::nullValueOrQuoter($tp->on_sale, $quoter);
            $q_online_only = Utils::nullValueOrQuoter($tp->online_only, $quoter);
            $q_isbn = Utils::nullValueOrQuoter($tp->isbn, $quoter);
            $q_upc = Utils::nullValueOrQuoter($tp->upc, $quoter);
            $q_mpn = Utils::nullValueOrQuoter($tp->mpn, $quoter);
            $q_ecotax = Utils::nullValueOrQuoter($tp->ecotax, $quoter);
            $q_quantity = Utils::nullValueOrQuoter($tp->quantity, $quoter);
            $q_minimal_quantity = Utils::nullValueOrQuoter($tp->minimal_quantity, $quoter);
            $q_low_stock_threshold = Utils::nullValueOrQuoter($tp->low_stock_threshold, $quoter);
            $q_low_stock_alert = Utils::nullValueOrQuoter($tp->low_stock_alert, $quoter);
            $q_price = Utils::nullValueOrQuoter($tp->price, $quoter);
            $q_wholesale_price = Utils::nullValueOrQuoter($tp->wholesale_price, $quoter);
            $q_unity = Utils::nullValueOrQuoter($tp->unity, $quoter);
            $q_unit_price_ratio = Utils::nullValueOrQuoter($tp->unit_price_ratio, $quoter);
            $q_additional_shipping_cost = Utils::nullValueOrQuoter($tp->additional_shipping_cost, $quoter);
            $q_reference = Utils::nullValueOrQuoter($tp->reference, $quoter);
            $q_supplier_reference = Utils::nullValueOrQuoter($tp->supplier_reference, $quoter);
            $q_location = Utils::nullValueOrQuoter($tp->location, $quoter);
            $q_width = Utils::nullValueOrQuoter($tp->width, $quoter);
            $q_height = Utils::nullValueOrQuoter($tp->height, $quoter);
            $q_depth = Utils::nullValueOrQuoter($tp->depth, $quoter);
            $q_weight = Utils::nullValueOrQuoter($tp->weight, $quoter);
            $q_out_of_stock = Utils::nullValueOrQuoter($tp->out_of_stock, $quoter);
            $q_additional_delivery_times = Utils::nullValueOrQuoter($tp->additional_delivery_times, $quoter);
            $q_quantity_discount = Utils::nullValueOrQuoter($tp->quantity_discount, $quoter);
            $q_customizable = Utils::nullValueOrQuoter($tp->customizable, $quoter);
            $q_uploadable_files = Utils::nullValueOrQuoter($tp->uploadable_files, $quoter);
            $q_text_fields = Utils::nullValueOrQuoter($tp->text_fields, $quoter);
            $q_active = Utils::nullValueOrQuoter($tp->active, $quoter);
            $q_redirect_type = Utils::nullValueOrQuoter($tp->redirect_type, $quoter);
            $q_id_type_redirected = Utils::nullValueOrQuoter($tp->id_type_redirected, $quoter);
            $q_available_for_order = Utils::nullValueOrQuoter($tp->available_for_order, $quoter);
            $q_available_date = Utils::nullValueOrQuoter($tp->available_date, $quoter);
            $q_show_condition = Utils::nullValueOrQuoter($tp->show_condition, $quoter);
            $q_condition = Utils::nullValueOrQuoter($tp->condition, $quoter);
            $q_show_price = Utils::nullValueOrQuoter($tp->show_price, $quoter);
            $q_indexed = Utils::nullValueOrQuoter($tp->indexed, $quoter);
            $q_visibility = Utils::nullValueOrQuoter($tp->visibility, $quoter);
            $q_cache_is_pack = Utils::nullValueOrQuoter($tp->cache_is_pack, $quoter);
            $q_cache_has_attachments = Utils::nullValueOrQuoter($tp->cache_has_attachments, $quoter);
            $q_is_virtual = Utils::nullValueOrQuoter($tp->is_virtual, $quoter);
            $q_cache_default_attribute = Utils::nullValueOrQuoter($tp->cache_default_attribute, $quoter);
            $q_date_add = Utils::nullValueOrQuoter($tp->date_add, $quoter);
            $q_date_upd = Utils::nullValueOrQuoter($tp->date_upd, $quoter);
            $q_advanced_stock_management = Utils::nullValueOrQuoter($tp->advanced_stock_management, $quoter);
            $q_pack_stock_type = Utils::nullValueOrQuoter($tp->pack_stock_type, $quoter);
            $q_state = Utils::nullValueOrQuoter($tp->state, $quoter);
            $q_id_shop = Utils::nullValueOrQuoter($tp->id_shop, $quoter);
            $q_id_lang = Utils::nullValueOrQuoter($tp->id_lang, $quoter);
            $q_description = Utils::nullValueOrQuoter($tp->description, $quoter);
            $q_description_short = Utils::nullValueOrQuoter($tp->description_short, $quoter);
            $q_link_rewrite = Utils::nullValueOrQuoter($tp->link_rewrite, $quoter);
            $q_meta_description = Utils::nullValueOrQuoter($tp->meta_description, $quoter);
            $q_meta_keywords = Utils::nullValueOrQuoter($tp->meta_keywords, $quoter);
            $q_meta_title = Utils::nullValueOrQuoter($tp->meta_title, $quoter);
            $q_name = Utils::nullValueOrQuoter($tp->name, $quoter);
            $q_available_now = Utils::nullValueOrQuoter($tp->available_now, $quoter);
            $q_available_later = Utils::nullValueOrQuoter($tp->available_later, $quoter);
            $q_delivery_in_stock = Utils::nullValueOrQuoter($tp->delivery_in_stock, $quoter);
            $q_delivery_out_stock = Utils::nullValueOrQuoter($tp->delivery_out_stock, $quoter);
            $q_brand = Utils::nullValueOrQuoter($tp->brand, $quoter);
            $q_tipas = Utils::nullValueOrQuoter($tp->tipas, $quoter);
            $q_paskirtis = Utils::nullValueOrQuoter($tp->paskirtis, $quoter);
            $q_dydis = Utils::nullValueOrQuoter($tp->dydis, $quoter);
            $q_kiekis = Utils::nullValueOrQuoter($tp->kiekis, $quoter);
            $q_iso_code = Utils::nullValueOrQuoter($tp->iso_code, $quoter);
            $q_parent = Utils::nullValueOrQuoter($tp->parent, $quoter);
            $q_spalva = Utils::nullValueOrQuoter($tp->spalva, $quoter);
            $q_var_name = Utils::nullValueOrQuoter($tp->var_name, $quoter);
            $q_ean = Utils::nullValueOrQuoter($tp->ean13, $quoter);

            $line = "(
              $q_nomnr,                      
              $q_id_supplier,                
              $q_supplier,
              $q_id_manufacturer,            
              $q_id_category_default,        
              $q_id_shop_default,            
              $q_id_tax_rules_group,         
              $q_on_sale,                    
              $q_online_only,                
              $q_isbn,                       
              $q_upc,
              $q_mpn,                        
              $q_ecotax,                     
              $q_quantity,                   
              $q_minimal_quantity,           
              $q_low_stock_threshold,        
              $q_low_stock_alert,            
              $q_price,                      
              $q_wholesale_price,            
              $q_unity,                      
              $q_unit_price_ratio,           
              $q_additional_shipping_cost,   
              $q_reference,                  
              $q_supplier_reference,         
              $q_location,                   
              $q_width,                      
              $q_height,                     
              $q_depth,                      
              $q_weight,                     
              $q_out_of_stock,               
              $q_additional_delivery_times,  
              $q_quantity_discount,          
              $q_customizable,               
              $q_uploadable_files,           
              $q_text_fields,                
              $q_active,                     
              $q_redirect_type,              
              $q_id_type_redirected,         
              $q_available_for_order,        
              $q_available_date,             
              $q_show_condition,             
              $q_condition,                  
              $q_show_price,                 
              $q_indexed,                    
              $q_visibility,                 
              $q_cache_is_pack,              
              $q_cache_has_attachments,      
              $q_is_virtual,                 
              $q_cache_default_attribute,    
              $q_date_add,                   
              $q_date_upd,                   
              $q_advanced_stock_management,  
              $q_pack_stock_type,            
              $q_state,                      
              $q_id_shop,                    
              $q_id_lang,                    
              $q_description,                
              $q_description_short,          
              $q_link_rewrite,               
              $q_meta_description,           
              $q_meta_keywords,              
              $q_meta_title,                 
              $q_name,                       
              $q_available_now,              
              $q_available_later,            
              $q_delivery_in_stock,          
              $q_delivery_out_stock,
              $q_brand, 
              $q_tipas,       
              $q_paskirtis,    
              $q_dydis,        
              $q_kiekis,
              $q_iso_code,
              $q_parent,      
              $q_spalva,
              $q_var_name,
              $q_ean      
            )";

            $values[] = $line;
        }

        $valuesStr = join(",\n", $values);

        $sql = /** @lang MySQL */
            "
        insert into tmp_products (
            nomnr,
            id_supplier,
            supplier,
            id_manufacturer,
            id_category_default,
            id_shop_default,
            id_tax_rules_group,
            on_sale,
            online_only,
            isbn,
            upc,
            mpn,
            ecotax,
            quantity,
            minimal_quantity,
            low_stock_threshold,
            low_stock_alert,
            price,
            wholesale_price,
            unity,
            unit_price_ratio,
            additional_shipping_cost,
            reference,
            supplier_reference,
            location,
            width,
            height,
            depth,
            weight,
            out_of_stock,
            additional_delivery_times,
            quantity_discount,
            customizable,
            uploadable_files,
            text_fields,
            active,
            redirect_type,
            id_type_redirected,
            available_for_order,
            available_date,
            show_condition,
            `condition`,
            show_price,
            indexed,
            visibility,
            cache_is_pack,
            cache_has_attachments,
            is_virtual,
            cache_default_attribute,
            date_add,
            date_upd,
            advanced_stock_management,
            pack_stock_type,
            state,
            id_shop,
            id_lang,
            description,
            description_short,
            link_rewrite,
            meta_description,
            meta_keywords,
            meta_title,
            name,
            available_now,
            available_later,
            delivery_in_stock,
            delivery_out_stock,
            brand,
            tipas,
            paskirtis,
            dydis,
            kiekis,
            iso_code,
            parent,
            spalva,
            var_name,
            ean13
            ) values $valuesStr
            on duplicate key update 
            id_supplier               = values(id_supplier              ),
            id_manufacturer           = values(id_manufacturer          ),
            id_category_default       = values(id_category_default      ),
            id_shop_default           = values(id_shop_default          ),
            id_tax_rules_group        = values(id_tax_rules_group       ),
            on_sale                   = values(on_sale                  ),
            online_only               = values(online_only              ),
            isbn                      = values(isbn                     ),
            upc                       = values(upc                      ),
            mpn                       = values(mpn                      ),
            ecotax                    = values(ecotax                   ),
            quantity                  = values(quantity                 ),
            minimal_quantity          = values(minimal_quantity         ),
            low_stock_threshold       = values(low_stock_threshold      ),
            low_stock_alert           = values(low_stock_alert          ),
            unity                     = values(unity                    ),
            unit_price_ratio          = values(unit_price_ratio         ),
            additional_shipping_cost  = values(additional_shipping_cost ),
            reference                 = values(reference                ),
            supplier_reference        = values(supplier_reference       ),
            location                  = values(location                 ),
            width                     = values(width                    ),
            height                    = values(height                   ),
            depth                     = values(depth                    ),
            weight                    = values(weight                   ),
            out_of_stock              = values(out_of_stock             ),
            additional_delivery_times = values(additional_delivery_times),
            quantity_discount         = values(quantity_discount        ),
            customizable              = values(customizable             ),
            uploadable_files          = values(uploadable_files         ),
            text_fields               = values(text_fields              ),
            active                    = values(active                   ),
            redirect_type             = values(redirect_type            ),
            id_type_redirected        = values(id_type_redirected       ),
            available_for_order       = values(available_for_order      ),
            available_date            = values(available_date           ),
            show_condition            = values(show_condition           ),
            `condition`               = values(`condition`              ),
            show_price                = values(show_price               ),
            indexed                   = values(indexed                  ),
            visibility                = values(visibility               ),
            cache_is_pack             = values(cache_is_pack            ),
            cache_has_attachments     = values(cache_has_attachments    ),
            is_virtual                = values(is_virtual               ),
            cache_default_attribute   = values(cache_default_attribute  ),
            date_upd                  = values(date_upd                 ),
            advanced_stock_management = values(advanced_stock_management),
            pack_stock_type           = values(pack_stock_type          ),
            state                     = values(state                    ),
            id_shop                   = values(id_shop                  ),
            id_lang                   = values(id_lang                  ),
            description               = values(description              ),
            description_short         = values(description_short        ),
            link_rewrite              = values(link_rewrite             ),
            meta_description          = values(meta_description         ),
            meta_keywords             = values(meta_keywords            ),
            meta_title                = values(meta_title               ),
            name                      = values(name                     ),
            available_now             = values(available_now            ),
            available_later           = values(available_later          ),
            delivery_in_stock         = values(delivery_in_stock        ),
            delivery_out_stock        = values(delivery_out_stock       ),
            brand        = values(brand),
            tipas        = values(tipas),
            paskirtis    = values(paskirtis),
            dydis        = values(dydis),
            kiekis       = values(kiekis),
            iso_code     = values(iso_code),
            parent       = values(parent),
            spalva       = values(spalva),
            var_name     = values(var_name),
            ean13        = values(ean13),
            id_product=0;
            ";

        return $sql;
    }

    /**
     * @param TmpCategory[] $tmpCategories
     * @param Closure $quoter
     * @return string
     */
    public function generateImportTmpCategories($tmpCategories, Closure $quoter)
    {
        $values = [];

        foreach ($tmpCategories as $tc) {

            $q_sketis_code = Utils::nullValueOrQuoter($tc->sketis_code, $quoter);
            $q_parent_code = Utils::nullValueOrQuoter($tc->parent_code, $quoter);
            $q_id_category = Utils::nullValueOrQuoter($tc->id_category, $quoter);
            $q_id_parent = Utils::nullValueOrQuoter($tc->id_parent, $quoter);
            $q_id_shop_default = Utils::nullValueOrQuoter($tc->id_shop_default, $quoter);
            $q_level_depth = Utils::nullValueOrQuoter($tc->level_depth, $quoter);
            $q_nleft = Utils::nullValueOrQuoter($tc->nleft, $quoter);
            $q_nright = Utils::nullValueOrQuoter($tc->nright, $quoter);
            $q_active = Utils::nullValueOrQuoter($tc->active, $quoter);
            $q_date_add = Utils::nullValueOrQuoter($tc->date_add, $quoter);
            $q_date_upd = Utils::nullValueOrQuoter($tc->date_upd, $quoter);
            $q_position = Utils::nullValueOrQuoter($tc->position, $quoter);
            $q_is_root_category = Utils::nullValueOrQuoter($tc->is_root_category, $quoter);
            $q_id_shop = Utils::nullValueOrQuoter($tc->id_shop, $quoter);
            $q_id_lang = Utils::nullValueOrQuoter($tc->id_lang, $quoter);
            $q_name = Utils::nullValueOrQuoter($tc->name, $quoter);
            $q_description = Utils::nullValueOrQuoter($tc->description, $quoter);
            $q_link_rewrite = Utils::nullValueOrQuoter($tc->link_rewrite, $quoter);
            $q_meta_title = Utils::nullValueOrQuoter($tc->meta_title, $quoter);
            $q_meta_keywords = Utils::nullValueOrQuoter($tc->meta_keywords, $quoter);
            $q_meta_description = Utils::nullValueOrQuoter($tc->meta_description, $quoter);
            $q_iso_code = Utils::nullValueOrQuoter($tc->iso_code, $quoter);

            $line = "(
            $q_sketis_code,     
            $q_parent_code,     
            $q_id_category,     
            $q_id_parent,       
            $q_id_shop_default, 
            $q_level_depth,     
            $q_nleft,           
            $q_nright,          
            $q_active,          
            $q_date_add,        
            $q_date_upd,        
            $q_position,        
            $q_is_root_category,
            $q_id_shop,         
            $q_id_lang,         
            $q_name,            
            $q_description,     
            $q_link_rewrite,    
            $q_meta_title,      
            $q_meta_keywords,   
            $q_meta_description,
            $q_iso_code
            )";

            $values[] = $line;
        }

        $valuesStr = join(",\n", $values);

        $sql = /** @lang MySQL */
            "
        insert into tmp_categories (
            sketis_code,
            parent_code,
            id_category,
            id_parent,
            id_shop_default,
            level_depth,
            nleft,
            nright,
            active,
            date_add,
            date_upd,
            position,
            is_root_category,
            id_shop,
            id_lang,
            name,
            description,
            link_rewrite,
            meta_title,
            meta_keywords,
            meta_description,
            iso_code
        ) values $valuesStr 
        ON DUPLICATE KEY UPDATE
            parent_code      = values(parent_code),
            id_shop_default  = values(id_shop_default),  
            level_depth      = values(level_depth),      
            nleft            = values(nleft),            
            nright           = values(nright),           
            active           = values(active),           
            date_upd         = values(date_upd),         
            position         = values(position),         
            id_shop          = values(id_shop),          
            id_lang          = values(id_lang),          
            name             = values(name),             
            description      = values(description),      
            link_rewrite     = values(link_rewrite),     
            meta_title       = values(meta_title),       
            meta_keywords    = values(meta_keywords),    
            meta_description = values(meta_description);";

        return $sql;
    }

    /**
     * @param TmpCategoryProduct[] $tmpCategoriesProducts
     * @param Closure $quoter
     * @return $sql;
     */
    public function generateImportTmpCategoriesProducts($tmpCategoriesProducts, Closure $quoter)
    {
        $lines = [];
        foreach ($tmpCategoriesProducts as $tmpCategoryProduct) {
            $q_sketis_code = $quoter($tmpCategoryProduct->sketis_code);
            $q_nomnr = $quoter($tmpCategoryProduct->nomnr);

            $line = "($q_sketis_code, $q_nomnr, {$tmpCategoryProduct->position})";
            $lines[] = $line;
        }
        $linesStr = join(",\n", $lines);

        $sql = /** @lang MySQL */
            "INSERT IGNORE INTO tmp_categories_products (sketis_code, nomnr, position)
        values $linesStr;";
        return $sql;
    }

    /**
     * @param TmpProduct[] $products
     * @return int
     * @throws ErrorException
     * @throws SystemException
     */
    public function importTmpProducts($products)
    {
        if (count($products) == 0) {
            return 0;
        }
        $quoter = $this->getQuoter();
        $sql = $this->generateImportTmpProducts($products, $quoter);
        try {
            return $this->database->getPdo()->exec($sql);
        } catch (PDOException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param TmpCategory[] $categories
     * @return int
     * @throws ErrorException
     * @throws SystemException
     */
    public function importTmpCategories($categories)
    {
        if (count($categories) == 0) {
            return 0;
        }

        $quoter = $this->getQuoter();
        $sql = $this->generateImportTmpCategories($categories, $quoter);
        try {
            return $this->database->getPdo()->exec($sql);
        } catch (PDOException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param TmpCategoryProduct[] $categoriesProducts
     * @return int
     * @throws ErrorException
     * @throws SystemException
     */
    public function importTmpCategoriesProducts($categoriesProducts)
    {
        if (count($categoriesProducts) == 0) {
            return 0;
        }
        $quoter = $this->getQuoter();
        $sql = $this->generateImportTmpCategoriesProducts($categoriesProducts, $quoter);
        try {
            return $this->database->getPdo()->exec($sql);
        } catch (PDOException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param TmpImage[] $tmpImages
     * @param Closure $quoter
     * @return string
     */
    public function generateInsertTmpImages($tmpImages, Closure $quoter)
    {

        $values = [];
        foreach ($tmpImages as $tmpImage) {
            $q_sketis_image_id = Utils::nullValueOrQuoter($tmpImage->sketis_image_id, $quoter);
            $q_uri = Utils::nullValueOrQuoter($tmpImage->uri, $quoter);
            $q_nomnr = Utils::nullValueOrQuoter($tmpImage->nomnr, $quoter);
            $q_position = Utils::nullValueOrQuoter($tmpImage->position, $quoter);
            $q_version = Utils::nullValueOrQuoter($tmpImage->version, $quoter);
            $q_copied_to_live = Utils::nullValueOrQuoter($tmpImage->copied_to_live, $quoter);
            $q_id_shop = Utils::nullValueOrQuoter($tmpImage->id_shop, $quoter);
            $q_cover = Utils::nullValueOrQuoter($tmpImage->cover, $quoter);

            $line = "(
                $q_sketis_image_id,
                $q_uri,            
                $q_nomnr,          
                $q_position,       
                $q_version,        
                $q_copied_to_live, 
                $q_id_shop,
                $q_cover        
            )";

            $values[] = $line;
        }


        $valuesStr = join(",\n", $values);
        $sql = /** @lang MySQL */
            "
        INSERT INTO tmp_images (
            sketis_image_id, 
            uri, 
            nomnr, 
            position, 
            version, 
            copied_to_live, 
            id_shop,
            cover
            )
            VALUES $valuesStr
            ON DUPLICATE KEY UPDATE
            sketis_image_id = VALUES(sketis_image_id), 
            uri = VALUES(uri), 
            nomnr = VALUES(nomnr), 
            position = VALUES(position), 
            version = VALUES(version), 
            copied_to_live=VALUES(copied_to_live), 
            id_shop=VALUES(id_shop),
            cover=VALUES(cover)";

        return $sql;
    }

    /**
     * @param $tmpImages
     * @return int
     * @throws ErrorException
     * @throws SystemException
     */
    public function insertTmpImages($tmpImages)
    {
        if (count($tmpImages) == 0) {
            return 0;
        }
        $quoter = $this->getQuoter();
        $sql = $this->generateInsertTmpImages($tmpImages, $quoter);
        try {
            return $this->database->getPdo()->exec($sql);
        } catch (PDOException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * @param $offset
     * @param $limit
     * @return TmpImage[]
     */
    public function loadTmpImages($offset, $limit)
    {
        $sql = /** @lang MySQL */
            "SELECT * from tmp_images LIMIT $offset,$limit";

        /** @var TmpImage[] $images */
        $images = $this->getDatabase()->getPdo()->query($sql)->fetchAll(PDO::FETCH_CLASS, TmpImage::class);
        return $images;
    }

    /**
     * @param TmpImage $tmpImage
     * @return int
     * @throws SystemException
     */
    public function storeDestinationPath(TmpImage $tmpImage)
    {
        $pdo = $this->database->getPdo();
        $quoter = $this->getQuoter();
        $qDetinationPath = Utils::nullValueOrQuoter($tmpImage->destination_path, $quoter);
        $qSketisImageId = $pdo->quote($tmpImage->sketis_image_id);
        $sql = /** @lang MySQL */
            "UPDATE tmp_images set destination_path=$qDetinationPath where sketis_image_id=$qSketisImageId";
        return $pdo->exec($sql);
    }

    /**
     * @param TmpImage[] $tmpImages
     * @throws SystemException
     * @return int
     */
    public function storeDestinationPaths($tmpImages)
    {
        $lines = [];
        $quoter = $this->getQuoter();
        foreach ($tmpImages as $tmpImage) {
            $q_sketis_image_id = Utils::nullValueOrQuoter($tmpImage->sketis_image_id, $quoter);
            $q_destination_path = Utils::nullValueOrQuoter($tmpImage->destination_path, $quoter);
            $line = "($q_sketis_image_id, $q_destination_path)";
            $lines[] = $line;
        }
        $valuesStr = join(",\n", $lines);

        $sql = /** @lang MySQL */
            "insert into tmp_images (sketis_image_id, destination_path)
        values $valuesStr
        on duplicate key update destination_path=values(destination_path);";
        return $this->getDatabase()->getPdo()->exec($sql);
    }


    /**
     * @param $lastId
     * @param $limit
     * @return TmpImage[]
     */
    public function loadOldVersionsTmpImages($lastId, $limit)
    {
        $pdo = $this->getDatabase()->getPdo();

        $qLastId = $pdo->quote($lastId);
        $sql = /** @lang MySQL */
            "SELECT * FROM tmp_images 
              WHERE (version > uploaded_version OR uploaded_version IS NULL )
              AND sketis_image_id > $qLastId
             ORDER BY sketis_image_id
             LIMIT $limit";

        /** @var TmpImage[] $images */
        $images = $pdo->query($sql)->fetchAll(PDO::FETCH_CLASS, TmpImage::class);
        return $images;
    }

    /**
     * @param TmpImage[] $tmpImages
     * @return int
     * @throws SystemException
     */
    public function storeUploadedVersions($tmpImages)
    {
        $lines = [];
        $quoter = $this->getQuoter();
        foreach ($tmpImages as $tmpImage) {
            $q_sketis_image_id = Utils::nullValueOrQuoter($tmpImage->sketis_image_id, $quoter);
            $q_uploaded_version = Utils::nullValueOrQuoter($tmpImage->uploaded_version, $quoter);
            $line = "($q_sketis_image_id, $q_uploaded_version)";
            $lines[] = $line;
        }
        $valuesStr = join(",\n", $lines);

        $sql = /** @lang MySQL */
            "insert into tmp_images (sketis_image_id, uploaded_version)
        values $valuesStr
        on duplicate key update uploaded_version=values(uploaded_version);";
        return $this->getDatabase()->getPdo()->exec($sql);
    }

    /**
     * @param int $orderId
     * @return Order
     * @throws SystemException
     */
    public function loadOrder($orderId)
    {
        $sql = /** @lang MySQL */
            "select * from ps_orders where id_order=$orderId";
        /** @var Order $order */
        $order = $this->database->getPdo()->query($sql)->fetchObject(Order::class);
        return $order;
    }

    /**
     * @param string $reference
     * @return Order
     * @throws SystemException
     */
    public function loadOrderByReference($reference)
    {
        $pdo = $this->database->getPdo();
        $qReference = $pdo->quote($reference);

        $sql = /** @lang MySQL */
            "select * from ps_orders where reference=$qReference";
        /** @var Order $order */
        $order = $pdo->query($sql)->fetchObject(Order::class);
        return $order;
    }

    /**
     * @param string $fromDate
     * @return int[]
     * @throws SystemException
     */
    public function findOrders($fromDate)
    {
        $pdo = $this->database->getPdo();
        $qFromDate = $pdo->quote($fromDate);

        $sql =
            /** @lang MySQL */
            "SELECT o.id_order from ps_orders o
            left join sketis_order_status s on o.id_order=s.id_order
            where date_upd >= $qFromDate and ( s.id_order is null or s.transfered = 0 and s.error = 0 ) ";

        /** @var int[] $id_orders */
        $id_orders = $pdo->query($sql)->fetchAll(PDO::FETCH_COLUMN);

        return $id_orders;
    }

    /**
     * @param string $fromDate
     * @return int[]
     * @throws SystemException
     */
    public function findOrderNumber($fromDate)
    {
        $pdo = $this->database->getPdo();
        $qFromDate = $pdo->quote($fromDate);

        $sql =
            /** @lang MySQL */
            "SELECT o.reference from ps_orders o where date_add >= $qFromDate";

        /** @var int[] $order_numbers */
        $order_numbers = $pdo->query($sql)->fetchAll(PDO::FETCH_COLUMN);
        return $order_numbers;
    }

    /**
     * @param $orderId
     * @return OrderDetail[]
     * @throws SystemException
     */
    public function loadOrderDetails($orderId)
    {
        $sql =
            /** @lang MySQL */
            "SELECT * from ps_order_detail where id_order=$orderId";

        /** @var OrderDetail[] $orderDetails */
        $orderDetails = $this->database->getPdo()->query($sql)
            ->fetchAll(PDO::FETCH_CLASS, OrderDetail::class);
        return $orderDetails;
    }

    /**
     * @param int $addressId
     * @return Address
     * @throws SystemException
     */
    public function loadAddress($addressId)
    {
        $sql =
            /** @lang MySQL */
            "SELECT * from ps_address where id_address=$addressId";

        /** @var Address $address */
        $address = $this->database->getPdo()->query($sql)->fetchObject(Address::class);
        return $address;
    }

    /**
     * @param $addressId
     * @return Address
     * @throws SystemException
     */
    public function loadAddressWithCustomer($addressId)
    {
        $address = $this->loadAddress($addressId);

        $customer = $this->loadCustomer($address->id_customer);
        $address->customer = $customer;

        $country = $this->loadCountry($address->id_country);
        $address->country = $country;

        return $address;
    }

    /**
     * @param int $orderId
     * @return Order
     * @throws SystemException
     */
    public function loadOrderWithAllDetails($orderId)
    {
        $order = $this->loadOrder($orderId);
        $orderDetails = $this->loadOrderDetails($orderId);
        $addressDelivery = $this->loadAddressWithCustomer($order->id_address_delivery);
        $addressInvoice = $this->loadAddressWithCustomer($order->id_address_invoice);
        $carrier = $this->loadCarrier($order->id_carrier);

        $order->lines = $orderDetails;
        $order->addressDelivery = $addressDelivery;
        $order->addressInvoice = $addressInvoice;
        $order->carrier = $carrier;
        $order->orderMessages = $this->loadOrderMessages($orderId);
        $order->customer = $this->loadCustomer($order->id_customer);

        if ($order->carrier->external_module_name == Carrier::DPD_EXTERNAL_MODULE_NAME) {
            $order->dpdOrder = $this->findDpdOrder($order->id_order);
            if (is_object($order->dpdOrder)) {
                $order->dpdOrder->deliveryPoint = $this->loadDpdDeliveryPoint($order->dpdOrder->id_dpd_delivery_point);
            }
        }
        $order->cart = $this->loadCartWithRelations($order->id_cart);

        return $order;
    }

    /**
     * @param int $customerId
     * @return Customer
     * @throws SystemException
     */
    public function loadCustomer($customerId)
    {
        $sql = /** @lang MySQL */
            "SELECT * FROM ps_customer where id_customer=$customerId";
        /** @var Customer $customer */
        $customer = $this->database->getPdo()->query($sql)->fetchObject(Customer::class);
        return $customer;
    }

    /**
     * @param $carrierId
     * @return Carrier
     * @throws SystemException
     */
    public function loadCarrier($carrierId)
    {
        $sql =
            /** @lang MySQL */
            "select * from ps_carrier where id_carrier=$carrierId";

        /** @var Carrier $carrier */
        $carrier = $this->database->getPdo()->query($sql)->fetchObject(Carrier::class);
        return $carrier;
    }

    /**
     * @param int $orderId
     * @return CustomerMessage[]
     * @throws SystemException
     */
    public function loadOrderMessages($orderId)
    {
        $sql = /** @lang MySQL */
            "select cm.* from ps_customer_message cm
         join ps_customer_thread ct on cm.id_customer_thread=ct.id_customer_thread
         where ct.id_order=$orderId";

        /** @var CustomerMessage[] $customerMessages */
        $customerMessages = $this->database->getPdo()->query($sql)
            ->fetchAll(PDO::FETCH_CLASS, CustomerMessage::class);
        return $customerMessages;
    }


    /**
     * @param int $countryId
     * @return Country
     * @throws SystemException
     */
    public function loadCountry($countryId)
    {
        $sql = /** @lang MySQL */
            "SELECT * FROM ps_country where id_country=$countryId";
        /** @var Country $country */
        $country = $this->database->getPdo()->query($sql)->fetchObject(Country::class);
        return $country;
    }

    /**
     * @param SketisOrderStatus $s
     * @return int
     * @throws SystemException
     */
    public function upsertSketisOrderStatus(SketisOrderStatus $s)
    {
        $pdo = $this->database->getPdo();
        $qMsg = $pdo->quote($s->response_msg);
        $qMissings = $pdo->quote($s->missings);
        $sql =
            /** @lang MySQL */
            "INSERT INTO sketis_order_status (id_order, transfered, response_msg, error, missings, error_count )
        VALUES({$s->id_order}, {$s->transfered}, $qMsg, {$s->error}, $qMissings, {$s->error_count})
        ON DUPLICATE KEY UPDATE 
           transfered=values(transfered), response_msg=values(response_msg), 
                                error=values(error), missings=values(missings),
                                error_count=values(error_count)
        ";
        return $pdo->exec($sql);
    }

    /**
     * @param int $id_order
     * @return SketisOrderStatus|null
     * @throws SystemException
     */
    public function getSketisOrderStatus($id_order)
    {
        $sql = /** @lang MySQL */
            "SELECT * FROM sketis_order_status where id_order=$id_order";
        $pdo = $this->database->getPdo();

        /** @var SketisOrderStatus[] $sketisOrderStatuses */
        $sketisOrderStatuses = $pdo->query($sql)->fetchAll(PDO::FETCH_CLASS, SketisOrderStatus::class);

        if (count($sketisOrderStatuses) > 0) {
            return $sketisOrderStatuses[0];
        } else {
            return null;
        }
    }

    /**
     * @param int $id_order
     * @return SketisOrderStatus
     * @throws SystemException
     */
    public function getOrCreateSketisOrderStatus($id_order)
    {
        $sketisOrderStatus = $this->getSketisOrderStatus($id_order);
        if ($sketisOrderStatus == null) {
            $sketisOrderStatus = new SketisOrderStatus();
            $sketisOrderStatus->id_order = $id_order;
            $this->upsertSketisOrderStatus($sketisOrderStatus);
        }

        return $sketisOrderStatus;
    }


    /**
     * @param $reference
     * @param $statusId
     * @return int
     * @throws SystemException
     */
    public function updateOrderStatus($reference, $statusId)
    {
        $pdo = $this->database->getPdo();
        $qReference = $pdo->quote($reference);

        $sql = /** @lang MySQL */
            "UPDATE ps_orders set current_state=$statusId WHERE reference=$qReference";
        return $pdo->exec($sql);
    }

//    /**
//     * @param string $reference
//     * @return int
//     * @throws SystemException
//     *
//     * @deprecated nenaudojamas
//     */
//    public function getPreviousOrderStatus ( $reference ) {
//        $pdo = $this->database->getPdo();
//        $qReference = $pdo->quote($reference);
//
//        $sql = /** @lang MySQL */ "SELECT current_state from ps_orders WHERE reference=$qReference";
//
//        /** @var int $status */
//        $status = $pdo->query($sql)->fetchColumn();
//        return $status;
//    }

    /**
     * @param int $idOrder
     * @param int $statusId
     * @return false|int
     * @throws SystemException
     */
    public function insertOrderStatusHistory($idOrder, $statusId)
    {
        $pdo = $this->database->getPdo();
        $sql = /** @lang MySQL */
            "insert into ps_order_history ( id_employee, id_order, id_order_state, date_add)  values ( 0, $idOrder, $statusId, now() )";
        return $pdo->exec($sql);
    }

    /**
     * @param string[] $references
     * @return array
     * @throws SystemException
     */
    public function loadProductsReferences2IdsMap($references)
    {
        $pdo = $this->database->getPdo();
        $qReferences = array_map([$pdo, 'quote'], $references);
        $qReferencesStr = join(',', $qReferences);

        $sql = /** @lang MySQL */
            "select reference, id_product from ps_product where reference in ($qReferencesStr)";
        $data = $pdo->query($sql)->fetchAll(PDO::FETCH_NUM);
        $map = [];
        foreach ($data as list($reference, $id_product)) {
            $map[$reference] = $id_product;
        }
        return $map;
    }

    public function updateOrderLinesAmountsAndPrices($update)
    {
        // TODO
    }

    public function insertOrderLines($insert)
    {
        // TODO
    }

    public function deleteOrderLines($delete)
    {
        // TODO
    }


    /**
     * @param int $orderId
     * @return OrderCarrier
     * @throws SystemException
     */
    public function findOrderCarrier($orderId)
    {
        $sql = /** @lang MySQL */
            "SELECT * FROM ps_order_carrier WHERE id_order=$orderId";

        /** @var OrderCarrier $orderCarrier */
        $orderCarrier = $this->database->getPdo()->query($sql)->fetchObject(OrderCarrier::class);

        return $orderCarrier;
    }

    /**
     * @param int $prestaOrderId
     * @return DpdOrder
     * @throws SystemException
     */
    public function findDpdOrder($prestaOrderId)
    {

        $sql = /** @lang MySQL */
            "SELECT do.* 
            FROM ps_dpd_order do 
            JOIN ps_dpd_order_ps ds ON do.id_dpd_order = ds.id_dpd_order 
            WHERE ds.id_order=$prestaOrderId";

        /** @var DpdOrder $dpdOrder */
        $dpdOrder = $this->database->getPdo()->query($sql)->fetchObject(DpdOrder::class);
        return $dpdOrder;
    }

    /**
     * @param int $id
     * @return DpdDeliveryPoint
     * @throws SystemException
     */
    public function loadDpdDeliveryPoint($id)
    {
        $sql = /** @lang MySQL */
            "SELECT * from ps_dpd_delivery_point dp where dp.id_dpd_delivery_point=$id";
        /** @var DpdDeliveryPoint $point */
        $point = $this->database->getPdo()->query($sql)->fetchObject(DpdDeliveryPoint::class);
        return $point;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix(string $prefix): void
    {
        $this->prefix = $prefix;
    }

    /**
     * @param string $fromDate
     * @return Order[] only id and reference
     * @throws SystemException
     */
    public function findPaidUnnoticedPayseraOrders($fromDate)
    {
        $pdo = $this->database->getPdo();
        $qFromDate = $pdo->quote($fromDate);
        $payseraModule = Order::PAYSERA_MODULE_NAME;

        $sql = /** @lang MySQL */
            "select o.id_order, o.reference
            from ps_orders o 
            join sketis_order_status sko on o.id_order = sko.id_order
            join ps_order_state os on o.current_state = os.id_order_state
          where
            os.module_name='$payseraModule'
            and paid=1
            and sko.allow_execute_sent=0
            and sko.error=0
            and o.date_upd >= $qFromDate";

        /** @var Order[] $orders */
        $orders = $pdo->query($sql)->fetchAll(PDO::FETCH_CLASS, Order::class);
        return $orders;
    }

    /**
     * @param int $idOrder
     * @param int $sent
     * @return int
     * @throws SystemException
     */
    public function markAllowExecuteSent($idOrder, $sent)
    {
        $sql = /** @lang MySQL */
            "UPDATE sketis_order_status SET allow_execute_sent=$sent WHERE id_order=$idOrder";
        return $this->database->getPdo()->exec($sql);
    }

    /**
     * @param TmpSpecialPrice[] $tmpSpecialPrices
     * @return int
     * @throws SystemException
     */
    public function insertTmpSpecialPrices($tmpSpecialPrices)
    {
        $pdo = $this->database->getPdo();
        $lines = [];
        foreach ($tmpSpecialPrices as $tp) {
            $qNomnr = $pdo->quote($tp->nomnr);
            $qGroupName = $pdo->quote($tp->group_name);
            $line = "($qNomnr, $qGroupName, {$tp->price})";
            $lines[] = $line;
        }
        $values = join(",\n", $lines);
        $sql = /** @lang MySQL */
            "
        INSERT INTO tmp_special_price ( nomnr, group_name, price)
        VALUES $values
        ON DUPLICATE KEY update price=VALUES(price)";

        return $pdo->exec($sql);
    }

    /**
     * @param string $orderNumber
     * @param string $trackCode
     * @return int
     * @throws SystemException
     */
    public function updateDeliveryCode($orderNumber, $trackCode)
    {
        $pdo = $this->database->getPdo();
        $qOrderNumber = $pdo->quote($orderNumber);
        $qTrackCode = $pdo->quote($trackCode);

        $sql = /** @lang MySQL */
            "update ps_order_carrier c
            join ps_orders o on c.id_order = o.id_order
        set tracking_number=$qTrackCode
        where reference=$qOrderNumber";
        return $pdo->exec($sql);
    }

    /**
     * @param int $idCart
     * @return Cart
     * @throws SystemException
     */
    public function loadCart($idCart)
    {
        $sql = /** @lang MySQL */
            "select * from ps_cart where id_cart=$idCart";
        /** @var Cart $cart */
        $cart = $this->database->getPdo()->query($sql)->fetchObject(Cart::class);
        return $cart;
    }

    /**
     * @param int $idCurrency
     * @return Currency
     * @throws SystemException
     */
    public function loadCurrency($idCurrency)
    {
        $sql = /** @lang MySQL */
            "select * from ps_currency where id_currency=$idCurrency";
        /** @var Currency $currency */
        $currency = $this->database->getPdo()->query($sql)->fetchObject(Currency::class);
        return $currency;
    }

    /**
     * @param $idCart
     * @return Cart
     * @throws SystemException
     */
    public function loadCartWithRelations($idCart)
    {
        /** @var Cart $cart */
        $cart = $this->loadCart($idCart);
        $cart->currency = $this->loadCurrency($cart->id_currency);
        return $cart;
    }

    /**
     * @return ImageType[]
     * @throws SystemException
     */
    public function loadProductsImageTypes()
    {
        $sql = /** @lang MySQL */
            "SELECT * FROM ps_image_type WHERE products=1";

        /** @var ImageType[] $imageTypes */
        $imageTypes = $this->database->getPdo()->query($sql)->fetchAll(PDO::FETCH_CLASS, ImageType::class);
        return $imageTypes;
    }


    /**
     * @param $lastId
     * @param $limit
     * @return TmpImage[]
     */
    public function loadUnresizedTmpImages($lastId, $limit)
    {
        $pdo = $this->getDatabase()->getPdo();

        $qLastId = $pdo->quote($lastId);
        $sql = /** @lang MySQL */
            "SELECT * FROM tmp_images 
              WHERE (version > resized_version OR resized_version IS NULL )
              AND sketis_image_id > $qLastId
             ORDER BY sketis_image_id
             LIMIT $limit";

        /** @var TmpImage[] $images */
        $images = $pdo->query($sql)->fetchAll(PDO::FETCH_CLASS, TmpImage::class);
        return $images;
    }

    /**
     * @param $sketis_image_id
     * @param $version
     * @return false|int
     * @throws SystemException
     */
    public function updateImageResizeVersion($sketis_image_id, $version)
    {
        $pdo = $this->database->getPdo();
        $qSketisImageId = $pdo->quote($sketis_image_id);
        $qVersion = $pdo->quote($version);
        $sql = /** @lang MySQL */
            "UPDATE tmp_images set resized_version=$qVersion where sketis_image_id=$qSketisImageId";
        return $pdo->exec($sql);
    }

    /**
     * @param TmpTag[] $tmpTags
     * @throws SystemException
     */
    public function importTmpTags($tmpTags)
    {
        $pdo = $this->database->getPdo();

        $lines = [];
        foreach ($tmpTags as $tag) {
            $line = [
                $pdo->quote($tag->tag_name),
                $pdo->quote($tag->iso_code),
            ];

            $lineStr = "(" . join(',', $line) . ")";
            $lines[] = $lineStr;
        }
        $valuesStr = join(",\n", $lines);

        $sql = /** @lang MySQL */
            "INSERT IGNORE INTO tmp_tag(tag_name, iso_code) 
                VALUES
                $valuesStr
                ";

        $pdo->exec($sql);
    }

    /**
     * @param TmpProductTag[] $tmpProductsTags
     * @throws SystemException
     */
    public function importTmpProductsTags($tmpProductsTags)
    {
        $pdo = $this->database->getPdo();
        $lines = [];

        foreach ($tmpProductsTags as $productTag) {
            $line = [
                $pdo->quote($productTag->nomnr),
                $pdo->quote($productTag->tag_name),
                $pdo->quote($productTag->iso_code),
            ];

            $lineStr = "(" . join(',', $line) . ")";
            $lines[] = $lineStr;
        }

        $valuesStr = join(",\n", $lines);
        $sql = /** @lang MySQL */
            " INSERT IGNORE INTO tmp_product_tag ( nomnr, tag_name, iso_code )
                 VALUES
                 $valuesStr";

        $pdo->exec($sql);
    }
}