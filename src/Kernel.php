<?php

use Kukulis\Helpers\DotEnv;
use Kukulis\Presta\Exception\SystemException;
use Kukulis\Presta\Importer\Command\ImportProductsCommand;
use Kukulis\Presta\Services\Mysql\Database;
use Kukulis\Presta\Services\File\FileDatabase;

class Kernel
{
    /** @var Kernel */
    private static $me = null;

    /**
     * @return Kernel
     */
    public static function instance()
    {
        if (self::$me == null) {
            $baseDir = realpath(__DIR__ . '/..');
            self::$me = new Kernel($baseDir);
        }
        return self::$me;
    }

    /** @var array */
    private $env;

    /** @var array */
    private $parameters;

    /** @var string */
    private $baseDir;

    /** @var FileDatabase */
    private $fileDatabase = null;

    /** @var Database */
    private $database = null;

    /** @var ImportProductsCommand */
    private $importProductsCommand = null;

    /** @var \Psr\Log\LoggerInterface */
    private $logger;

    /** @var \Kukulis\Presta\Services\Unishop\ExportOrders3Service */
    private $exportOrders3Service = null;

    /** @var \Sketis\B2b\Client\SketisB2bOrdersClient */
    private $unishopOrdersClient;


    private function __construct($baseDir)
    {
        $this->baseDir = $baseDir;
        $this->exceptionsHandling();
        $this->loadEnv();
        $this->loadParameters();
    }

    private function exceptionsHandling()
    {
        set_error_handler(function ($severity, $message, $file, $line) {
            if (!(error_reporting() & $severity)) {
                // This error code is not included in error_reporting
                return;
            }
            throw new ErrorException($message, 0, $severity, $file, $line);
        });
    }

    private function loadEnv()
    {
        $dotEnv = new DotEnv($this->baseDir . '/.env');
        $dotEnv->load();
        $this->env = $dotEnv->getParameters();
    }

    private function loadParameters()
    {
        $this->parameters = require($this->baseDir . '/conf/parameters.php');
    }


    public function getEnvs()
    {
        return $this->env;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getEnv($name)
    {
        if (!array_key_exists($name, $this->env)) {
            throw new SystemException('There is no parameter ' . $name);
        }
        return $this->env[$name];
    }

    /**
     * @return FileDatabase|null
     */
    public function getFileDatabase()
    {
        if ($this->fileDatabase == null) {
            $this->fileDatabase = new FileDatabase($this->baseDir . '/var/storage/output.sql');
        }
        return $this->fileDatabase;
    }

    /**
     * @return Database
     */
    public function getPrestaDatabase()
    {
        if ($this->database == null) {
            $this->database = new Database(
                self::getEnv('DB_HOST'),
                self::getEnv('DB_NAME'),
                self::getEnv('DB_USER'),
                self::getEnv('DB_PASS')
            );
        }

        return $this->database;
    }

    public function getLogger()
    {
        if ($this->logger == null) {
            $this->logger = new \Monolog\Logger('app');
            $stderrHandler = new \Monolog\Handler\StreamHandler(STDERR, \Monolog\Logger::DEBUG);
            $defaultFileHandler = new \Monolog\Handler\StreamHandler($this->baseDir . '/var/logs/main.log',
                \Monolog\Logger::DEBUG);
            $this->logger->setHandlers([$stderrHandler, $defaultFileHandler]);
        }

        return $this->logger;
    }

    public function getSQLLogger()
    {
        if ($this->logger == null) {
            $this->logger = new \Monolog\Logger('app');
            $stderrHandler = new \Monolog\Handler\StreamHandler(STDERR, \Monolog\Logger::INFO);
            $defaultFileHandler = new \Monolog\Handler\StreamHandler($this->baseDir . '/var/logs/sql.log',
                \Monolog\Logger::DEBUG);
            $this->logger->setHandlers([$stderrHandler, $defaultFileHandler]);
        }

        return $this->logger;
    }


    public function getImportProductsCommand()
    {

        if ($this->importProductsCommand == null) {
            $this->importProductsCommand =
                new ImportProductsCommand(
                    $this->getLogger(),
                    $this->getImportElkoDataService()
                );
        }

        return $this->importProductsCommand;
    }

    public function getPrestaDatabaseService()
    {
        return new \Kukulis\Presta\Services\PrestaDatabaseService(
            $this->getLogger(),
            $this->getPrestaDatabase()
        );
    }

    public function getFilePrestaDatabaseService()
    {
        return new \Kukulis\Presta\Services\PrestaDatabaseService(
            $this->getLogger(),
            $this->getFileDatabase()
        );
    }

    public function getElkoClient()
    {
        $elkoClient = new \Kukulis\Providers\ElkoClient(
            $this->getLogger(),
            new \GuzzleHttp\Client(),
            $this->getEnv('ELKO_URL')
        );

        $elkoClient->setUser($this->getEnv('ELKO_USER'));
        $elkoClient->setPassword($this->getEnv('ELKO_PASSWORD'));

        return $elkoClient;
    }

    public function getImportElkoDataService()
    {
        $importElkoDataService = new \Kukulis\Presta\Services\ImportElkoDataService(
            $this->getLogger(),
            $this->getElkoClient(),
            $this->getFilePrestaDatabaseService()
        );

        $importElkoDataService->setLocalesMap($this->getParameters()['localesMap']);

        return $importElkoDataService;
    }

    public function getImportCategoriesCommand()
    {
        return new \Kukulis\Presta\Importer\Command\ImportCategoriesCommand(
            $this->getLogger(),
            $this->getImportElkoDataService()
        );
    }

    public function getBuildCategoriesTreeCommand()
    {
        return new \Kukulis\Presta\Importer\Command\BuildCategoriesTreeCommand(
            $this->getSQLLogger(),
            $this->getPrestaTreeService()
        );
    }

    public function getPrestaTreeService()
    {
        return new \Kukulis\Presta\Services\PrestaTreeService(
            $this->getLogger(),
            $this->getPrestaDatabaseService()
        );
    }

    public function getImportAmountsAndPricesCommand()
    {
        return new \Kukulis\Presta\Importer\Command\ImportAmountsAndPricesCommand(
            $this->getLogger(),
            $this->getImportElkoDataService()
        );
    }

    public function getImportImagesCommand()
    {
        return new \Kukulis\Presta\Importer\Command\ImportImagesCommand(
            $this->getLogger(),
            $this->getImportElkoDataService()
        );
    }

    public function getImportImagesService()
    {
        return new \Kukulis\Presta\Services\ImportImagesService(
            $this->getLogger(),
            $this->getPrestaDatabaseService()
        );
    }

    public function getCalculateImagesPathsCommand()
    {
        return new \Kukulis\Presta\Importer\Command\CalculateImagesPathsCommand(
            $this->getImportImagesService(),
            $this->getLogger()
        );
    }

    public function getDownloadImagesCommands()
    {
        return new \Kukulis\Presta\Importer\Command\DownloadImagesCommand(
            $this->getImportImagesService(),
            $this->getLogger()
        );
    }

    public function getResizeImagesCommands()
    {
        return new \Kukulis\Presta\Importer\Command\ResizeImagesCommand(
            $this->getImportImagesService(),
            $this->getLogger()
        );
    }

    public function getExportOrdersToUnishopCommand()
    {
        return new \Kukulis\Presta\Importer\Command\ExportOrdersToUnishopCommand(
            $this->getLogger(),
            $this->getExportOrders3Service()
        );
    }


    public function getExportOrders3Service()
    {
        if ($this->exportOrders3Service == null) {
            $this->exportOrders3Service = new \Kukulis\Presta\Services\Unishop\ExportOrders3Service(
                $this->getLogger(),
                $this->getPrestaDatabaseService(),
                $this->getUnishopOrdersClient(),
                $this->getEnv('UNISHOP_TOKEN')

            );
        }

        return $this->exportOrders3Service;
    }

    public function getUnishopOrdersClient()
    {
        if ($this->unishopOrdersClient == null) {
            $this->unishopOrdersClient = new \Sketis\B2b\Client\SketisB2bOrdersClient(
                $this->getLogger(),
                new \GuzzleHttp\Client(),
                $this->getEnv('UNISHOP_BASE_URL'),
                \JMS\Serializer\SerializerBuilder::create()->build());
        }

        return $this->unishopOrdersClient;

    }
}