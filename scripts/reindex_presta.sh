#!/usr/bin/env bash

set -e
. bin/settings.sh

# copy the php script into presta home dir
cp bin/commands/reindex_presta_caller.php $SHOP_BASE_PATH

# run the php script
#PRESTA_REINDEX_TOKEN=$PRESTA_REINDEX_TOKEN ADMIN_DIR=$ADMIN_DIR SHOP_BASE_PATH=$SHOP_BASE_PATH php $SHOP_BASE_PATH/reindex_presta_caller.php
# depending on the version of the reindex_presta_caller.php all these parameters are not needed

php $SHOP_BASE_PATH/reindex_presta_caller.php
