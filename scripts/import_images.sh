#!/usr/bin/env bash
# leidžiama iš ezplatform direktorijos
set -e

. bin/commands/settings.sh

# quiet opcija neleidžia warningams eiti į error stream'ą, bet warningas toliau eis į output streamą
bin/console --quiet  --env=$SYMFONY_ENV sketis:shopapi:import_tmp_images $TOKEN $LOCALE tmp_images.sql

mysql < tmp_images.sql

mysql < sql/img_00_update_tmp_images_products_ids.sql
mysql < sql/img_01_insert_images_records.sql
mysql < sql/img_02_update_tmp_images_ids.sql
mysql < sql/img_03_insert_images_langs_records.sql
mysql < sql/img_04_insert_images_shops_records.sql
mysql < sql/img_05_assign_variations_images.sql
mysql < sql/img_06_delete_old_images.sql

bin/console  --quiet  --env=$SYMFONY_ENV sketis:shopapi:prepare_images_paths

bin/console  --quiet  --env=$SYMFONY_ENV sketis:shopapi:upload_images $KATALOGAS_URL $SHOP_BASE_PATH