#!/usr/bin/env bash

echo "Preparing tmp images for downloading"
mysql < sql/scripts/img_00_update_tmp_images_products_ids.sql
mysql < sql/scripts/img_01_insert_images_records.sql
mysql < sql/scripts/img_02_update_tmp_images_ids.sql
mysql < sql/scripts/img_03_insert_images_langs_records.sql
mysql < sql/scripts/img_04_insert_images_shops_records.sql
mysql < sql/scripts/img_05_assign_variations_images.sql
mysql < sql/scripts/img_06_delete_old_images.sql
echo "Preparing tmp images for downloading FINISHED"
