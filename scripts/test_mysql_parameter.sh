#!/usr/bin/env bash

set -e

. bin/settings.sh

#echo "@ZERO_PRODUCTS_DISPLAY=$ZERO_PRODUCTS_DISPLAY"

#mysql -e "set @ZERO_PRODUCTS_DISPLAY='search'; `cat sql/test_parameter.sql`"

if [ -z ${ZERO_PRODUCTS_DISPLAY+x} ]; then export ZERO_PRODUCTS_DISPLAY='none'; fi

echo "set @ZERO_PRODUCTS_DISPLAY='$ZERO_PRODUCTS_DISPLAY';" | cat - sql/scripts/test_parameter.sql | mysql
