#!/usr/bin/env bash

# šitas skriptas kviečiamas iš import_products.sh
# visi šie skriptai skirti pervesti duomenis iš tmp_products į prestos lenteles
echo "Generating virtual parent products in tmp_products"
mysql < sql/scripts/00.05_insert_tmp_parent_products.sql
echo "Inserting unique tmp_ps_products"
mysql < sql/scripts/00.1_insert_tmp_products_unique.sql
echo "Updating products ids both for tmp_products and tmp_products_unique"
mysql < sql/scripts/00.2_update_tmp_products_ids.sql
echo "Inserting ps_products"
mysql < sql/scripts/01_insert_products_from_tmp.sql
echo "Updating products ids in both for tmp_products and tmp_products_unique the second time"
mysql < sql/scripts/00.2_update_tmp_products_ids.sql
echo "Inserting brands (manufacturers)"
mysql < sql/scripts/02.2_insert_brands.sql
echo "Updating manufacturer ids in  tmp_products"
mysql < sql/scripts/02.3_update_manufacturer_ids.sql
echo "Upserting manufacturers"
mysql < sql/scripts/02.4_upsert_manufecturer_lang_shop.sql
echo "Updating supplier ids in  tmp_products"
mysql < sql/scripts/02.5_update_supplier_ids.sql
echo "Inserting supplier to product relations"
mysql < sql/scripts/02.6_insert_products_suppliers.sql

echo "updating ps_products from tmp_products"
mysql < sql/scripts/03_update_products_from_tmp.sql

echo "updating id_lang"
mysql < sql/scripts/03.1_update_id_lang.sql

echo "inserting products_langs"
mysql < sql/scripts/04_insert_products_langs_from_tmp.sql
echo "updating products_langs"
mysql < sql/scripts/05_update_products_langs_from_tmp.sql
echo "inserting products shops"
mysql < sql/scripts/06.1_insert_products_shops_from_tmp.sql
echo "updating products shops"
mysql < sql/scripts/06.2_update_products_shops_from_tmp.sql


echo "inserting categories"
mysql < sql/scripts/07.1_tmp_categories_unique.sql
mysql < sql/scripts/07.2_insert_categories_from_tmp.sql
echo "updating categories ids in tmp_catgories"
mysql < sql/scripts/08.1_update_categories_ids_in_tmp.sql
echo "updating categories parents in tmp_categories"
mysql < sql/scripts/08.2_update_categories_parents_ids_in_tmp.sql
echo "updating categories langs ids"
mysql < sql/scripts/08.3_update_categories_langs_ids_in_tmp.sql

echo "updating categories"
mysql < sql/scripts/09_update_categories_from_tmp.sql
echo "inserting categories langs"
mysql < sql/scripts/10_insert_categories_langs_from_tmp.sql
echo "inserting categories shops"
mysql < sql/scripts/10.2_insert_categories_shops_from_tmp.sql
echo "updating categories langs"
mysql < sql/scripts/11_update_categories_langs_from_tmp.sql
echo "updating id_product into tmp_categories (preparing for relations with products)"
mysql < sql/scripts/12_update_tmp_categories_poducts_ids.sql
echo "inserting category to product relations"
mysql < sql/scripts/13_insert_categories_products_from_tmp.sql
echo "updating default category id in products"
mysql < sql/scripts/14_update_default_category_id.sql
echo "assingning categories groups (rights for users to see) to categories"
mysql < sql/scripts/15_categories_groups.sql


# features
####################################################
echo "Inserting tmp features"
mysql < sql/scripts/f01_insert_tmp_features.sql

echo "Updating feature (group) id in tmp_features"
mysql < sql/scripts/f02_update_tmp_feature_id.sql

echo "inserting values in to tmp_features_values A: from existing values B: from tmp_features table"
mysql < sql/scripts/f03_insert_tmp_features_values.sql

echo "updating id_product in to tmp_features"
mysql < sql/scripts/f04_update_tmp_feature_id_product.sql

echo "updating id_feature_value in to tmp_features"
mysql < sql/scripts/f05_update_tmp_feature_value_id.sql

echo "inserting new feature values in to ps_feature_value ant ps_feature_value_lang"
mysql < sql/scripts/f06_insert_new_feature_values.sql

echo "assigning inserted features values to products"
mysql < sql/scripts/f07_insert_feature_products.sql
######################################################

# tags
echo "Importing tags"
mysql < sql/scripts/t01_import_tmp_tags.sql

# variantų importas

echo "Importing attributes for variants"
echo "Importing tmp attributes"
mysql < sql/scripts/a00_insert_tmp_attributes.sql
echo "Updating id lang"
mysql < sql/scripts/a01_update_id_lang.sql
echo "Updating id attribute group"
mysql < sql/scripts/a02_update_id_attribute_group.sql
echo "Inserting auto attributes"
mysql < sql/scripts/a03_insert_auto_attributes.sql
echo "Inserting attributes"
mysql < sql/scripts/a05_insert_attributes.sql
echo "Updading attribute ids to tmp"
mysql < sql/scripts/a06_update_tmp_attributes_ids.sql
echo "Updating attribute langs"
mysql < sql/scripts/a07_insert_attribute_lang.sql

echo "Importing variants "
echo "tmp variations"
mysql < sql/scripts/v01_insert_tmp_variations.sql
echo "variations parent ids"
mysql < sql/scripts/v02_update_parent_ids.sql
echo "variations product attributes"
mysql < sql/scripts/v03_insert_product_attributes.sql
echo "variations product attributes id to tmp"
mysql < sql/scripts/v04_update_id_product_attribute_to_tmp.sql
echo "variations update product attributes fields"
mysql < sql/scripts/v04.5_update_product_attributes_fields.sql
echo "variations update combinations ids"
mysql < sql/scripts/v05_update_combinations_ids.sql
echo "variations insert product attributes combinations"
mysql < sql/scripts/v06_insert_product_attributes_combinations.sql
echo "variations insert product attribute shop"
mysql < sql/scripts/v07_insert_product_attribute_shop.sql
echo "variations main variant setting"
mysql < sql/scripts/v08_main_variant_setting.sql
echo "variations delete old nonvariations"
mysql < sql/scripts/v09_delete_old_nonvariations.sql
echo "variations delete old variations"
mysql < sql/scripts/v10_delete_old_variations.sql



