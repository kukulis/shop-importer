#!/usr/bin/env bash
set -e
. bin/commands/settings.sh

# 00 truncate previous tmp records
mysql < sql/sp_00_truncate_tmp_special_prices.sql


SQLFILE=tmp_special_prices.sql

# iš mysql nuskaitom tagus iš lentelės ps_group_lang stulpelio `name`
TAGS=()
while IFS= read -r line; do
        if [ "$line" != 'name' ]
        then
           TAGS+=("$line")
        fi
done < <(echo "select name from ps_group_lang where id_group > 3 and id_lang=1;" | mysql)


for TAG in "${TAGS[@]}"
do
echo TAG=$TAG

cat /dev/null > $SQLFILE

#1.1 the command
# quiet opcija neleidžia warningams eiti į error stream'ą, bet warningas toliau eis į output streamą
bin/console --quiet  --env=$SYMFONY_ENV sketis:shopapi:import_special_prices $TOKEN $TAG  $SQLFILE

#1.2 feeding to mysql
mysql < $SQLFILE

done



#2 ids updaters
## 2.1 id_product
mysql < sql/sp_02.1_update_id_product.sql
## 2.2 id_group
mysql < sql/sp_02.2_update_id_group.sql

## 2.3 id_specific_price
mysql < sql/sp_02.3_update_id_specific_price.sql

#3 update presta special prices table
mysql < sql/sp_03_update_specific_prices.sql


#4 insert into prest special prices table
mysql < sql/sp_04_insert_specific_prices.sql

