#!/usr/bin/env bash
# leidžiama iš ezplatform direktorijos
set -e

. bin/commands/settings.sh

# quiet opcija neleidžia warningams eiti į error stream'ą, bet warningas toliau eis į output streamą
bin/console  --quiet  --env=$SYMFONY_ENV sketis:shopapi:import_prices_amounts $TOKEN tmp_prices_amounts.sql

mysql < sql/00_zero_tmp_amounts.sql


mysql < tmp_prices_amounts.sql

if [ -z ${ZERO_PRODUCTS_DISPLAY+x} ]; then export ZERO_PRODUCTS_DISPLAY='none'; fi
echo "set @ZERO_PRODUCTS_DISPLAY='$ZERO_PRODUCTS_DISPLAY';" | cat - sql/tmp_to_prices_amounts.sql | mysql
