#!/usr/bin/env bash

set -e

. bin/commands/settings.sh

DAYS=20

# quiet opcija neleidžia warningams eiti į error stream'ą, bet warningas toliau eis į output streamą
bin/console --quiet sketis:shopapi:update_order_statuses $TOKEN $DAYS