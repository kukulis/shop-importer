#!/bin/bash
set -e

. bin/commands/settings.sh

DAYS=4

# quiet opcija neleidžia warningams eiti į error stream'ą, bet warningas toliau eis į output streamą
bin/console --quiet  --env=$SYMFONY_ENV sketis:shopapi:update_orders_delivery_codes $TOKEN 3
