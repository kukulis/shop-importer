#!/usr/bin/env bash
# leidžiama iš ezplatform direktorijos
set -e

. bin/commands/settings.sh

bin/console  --quiet  --env=$SYMFONY_ENV sketis:shopapi:resize_images $SHOP_BASE_PATH
