<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.8.15
 * Time: 19.53
 */

namespace Tests\Draft;


use PHPUnit\Framework\TestCase;
use Kernel;

class AvailabilityTest extends TestCase
{
    public function testGetAvailabilities() {
         $elkoClient = Kernel::instance()->getElkoClient();
         $ids = [ 1303388,1247557,1301633,1301630 ];
         $token = $elkoClient->getToken();
         $availabilities =  $elkoClient->getAvailabilityAndPrice( $token, $ids );

         $this->assertEquals(4, count($availabilities));
    }

}