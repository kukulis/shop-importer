<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.16
 * Time: 21.17
 */

namespace Tests\Draft;


use Kukulis\Presta\Helpers\CategoriesTreeParser;
use PHPUnit\Framework\TestCase;

class CategoriesTreeTest extends TestCase
{

    public function  testParse() {
          $tree = unserialize(file_get_contents('categories_tree.txt' ));
//          var_dump($tree);
          $categoriesMap = CategoriesTreeParser::treeToFlatMap($tree, null );
          $this->assertTrue(is_array($categoriesMap));
          foreach ( $categoriesMap as $key => $category ) {
              echo "$key => ".$category->code.', ['.$category->parentCode.']'."\n" ;
          }
    }

}