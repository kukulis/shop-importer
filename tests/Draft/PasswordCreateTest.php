<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.28
 * Time: 19.30
 */

namespace Tests\Draft;


use PHPUnit\Framework\TestCase;

class PasswordCreateTest extends TestCase
{
    public function testCreate() {
        $password = 'Labas123';

        $hash = password_hash($password,  PASSWORD_BCRYPT );
        $this->assertNotEmpty($hash);
        echo "hash=$hash\n";
    }

    public function testPassword() {
        $password = 'Labas123';
        $hash = '$2y$10$9/dR.iTlL5PEbpZIbR/8M.V53jMk686DAYRpnxqrbPYVrSdw5kGvO';

        $verifyResult = password_verify($password, $hash);
        echo "verifyResult=$verifyResult\n";
    }

}