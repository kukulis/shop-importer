<?php

namespace Tests\Unit;

use Kukulis\Helpers\Utils;
use PHPUnit\Framework\TestCase;


class PutInColumnsArraysTest extends TestCase
{

    public function testEquals()
    {
        $arrayOfArrays1 = [
            ['A', 1],
            ['B', 2],
            ['C', 3],
        ];
        $arrayOfArrays2 = [
            ['X', 10],
            ['Y', 20],
            ['Z', 30],
        ];

        $expected = [
            ['A', 1, 'X', 10],
            ['B', 2, 'Y', 20],
            ['C', 3, 'Z', 30],
        ];

        $this->assertEquals($expected, Utils::putInColumnsArrays([$arrayOfArrays1, $arrayOfArrays2]));
    }

    public function testThree() {
        $arrayOfArrays1 = [
            ['A', 1],
            ['B', 2],
            ['C', 3],
        ];
        $arrayOfArrays2 = [
            ['X', 10],
            ['Y', 20],
            ['Z', 30],
        ];

        $arrayOfArrays3 = [
            ['W', 100],
            ['U', 200],
            ['V', 300],
        ];

        $expected = [
            ['A', 1, 'X', 10, 'W', 100],
            ['B', 2, 'Y', 20, 'U', 200],
            ['C', 3, 'Z', 30, 'V', 300],
        ];

        $this->assertEquals($expected, Utils::putInColumnsArrays([$arrayOfArrays1, $arrayOfArrays2, $arrayOfArrays3]));
    }


    public function testNonEquals() {
        $arrayOfArrays1 = [
            ['A', 1],
            ['B', 2],
            ['C', 3],
        ];
        $arrayOfArrays2 = [
            ['X', 10],
            ['Y', 20],
        ];

        $arrayOfArrays3 = [
            ['W', 100],
        ];

        $expected = [
            ['A', 1, 'X', 10, 'W', 100],
            ['B', 2, 'Y', 20, '', ''],
            ['C', 3, '', '', '', ''],
        ];

        $this->assertEquals($expected, Utils::putInColumnsArrays([$arrayOfArrays1, $arrayOfArrays2, $arrayOfArrays3]));

    }

    public function testLongerNonEquals() {
        $arrayOfArrays1 = [
            ['A', 1],
            ['B', 2],
            ['C', 3],
        ];
        $arrayOfArrays2 = [
            ['X'],
            ['Y'],
        ];

        $arrayOfArrays3 = [
            ['W', 100, 'abc'],
            ['U', 200, 'def'],
            ['V', 300, 'ghi'],
            ['VV', 400, 'jkl'],
        ];

        $expected = [
            ['A', 1, 'X',  'W',  100, 'abc'],
            ['B', 2, 'Y',  'U',  200, 'def'],
            ['C', 3,  '',  'V',  300, 'ghi'],
            [ '','',  '',  'VV', 400, 'jkl'],
        ];

        $this->assertEquals($expected, Utils::putInColumnsArrays([$arrayOfArrays1, $arrayOfArrays2, $arrayOfArrays3]));

    }
}