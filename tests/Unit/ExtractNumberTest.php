<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.8.15
 * Time: 23.43
 */

namespace Tests\Unit;


use Kukulis\Helpers\Utils;
use PHPUnit\Framework\TestCase;

class ExtractNumberTest extends TestCase
{
    public function testExtract() {
        $this->assertEquals(50, Utils::extractNumber('> 50'));
    }

}