<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.6.26
 * Time: 12.12
 */

namespace Tests\Unit;


use GuzzleHttp\Utils;
use Kukulis\Elko\Data\DescriptionObject;
use Kukulis\Elko\ElkoDescriptionsHolder;
use Kukulis\Providers\ElkoDescriptionHandler;
use PHPUnit\Framework\TestCase;

class ElkoDescriptionHandlerTest extends TestCase
{
    public function testHandleDescription() {
        $dataJson = '
        [
  {
    "productId": 1301633,
    "description": [
      {
        "criteria": "3.5\" SAS Hot-swap",
        "measurement": null,
        "comlexName": "Drive Bays",
        "value": "106",
        "orderNo": "3",
        "criteriaId": 2034,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Description",
        "measurement": null,
        "comlexName": null,
        "value": "SEAGATE Exos E 4U106 JBOD system. Total available RAW capacity 1696TB with 106 of 106 (3.5&quot;) drive bays populated.&nbsp;<br />\n<br />\nDrives: 106x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos (ST16000NM002G)<br />\n4x System Fans<br />\n2x IOM Cooling FAN Modules<br />\n8x Hot-Swap Expanders<br />\n2x High-Efficiency 2kW Redundant Power Supplies<br />\nRail Kit and Cable Management Arm<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; Mini-SAS HD connectors (12Gb/s)<br />\n- 2 RJ45 port per IOM (for management)<br />\n- 1 Serial Ports<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
        "orderNo": null,
        "criteriaId": 419,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Full Description Line",
        "measurement": null,
        "comlexName": null,
        "value": "106x3.5\" SAS Hot-swap|Supported drives SAS HDD|4xRJ45|8xMiniSAS, ext|Rack 4U",
        "orderNo": "999",
        "criteriaId": 999,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "HDD configuration",
        "measurement": null,
        "comlexName": null,
        "value": "84x 16TB, SAS 7.2k",
        "orderNo": "2",
        "criteriaId": 2157,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "MiniSAS, ext",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "8",
        "orderNo": "6",
        "criteriaId": 1827,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Rack 4U",
        "measurement": null,
        "comlexName": "Mounting Type",
        "value": "Yes",
        "orderNo": "7",
        "criteriaId": 1224,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "RJ45",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "4",
        "orderNo": "6",
        "criteriaId": 1309,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Supported drives",
        "measurement": null,
        "comlexName": null,
        "value": "SAS HDD",
        "orderNo": "4",
        "criteriaId": 1854,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Brutto Volume",
        "measurement": "cubm",
        "comlexName": null,
        "value": "0.1591",
        "orderNo": "13",
        "criteriaId": 1449,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Gross Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "155",
        "orderNo": "15",
        "criteriaId": 790,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Net Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "140.6",
        "orderNo": "14",
        "criteriaId": 791,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Vendor Homepage",
        "measurement": null,
        "comlexName": null,
        "value": "www.seagate.com/files/www-content/datasheets/pdfs/exos-e-4u106-DS1980-3-1812US-en_US.pdf",
        "orderNo": null,
        "criteriaId": 426,
        "lastUpdateDate": "0001-01-01T00:00:00"
      }
    ]
  },
  {
    "productId": 1301630,
    "description": [
      {
        "criteria": "3.5\" SAS Hot-swap",
        "measurement": null,
        "comlexName": "Drive Bays",
        "value": "84",
        "orderNo": "3",
        "criteriaId": 2034,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Description",
        "measurement": null,
        "comlexName": null,
        "value": "SEAGATE Exos X 5U84 RAID system. Total available RAW capacity 1344TB with 84 of 84 (3.5&quot;) drive bays populated.&nbsp;<br />\n320k IOPS<br />\n7GB/s sequential read troughput<br />\n5.5G/b sequential write troughput<br />\nThin Provisioning,&nbsp;SSD Read Cashe,&nbsp;Real-time Auto Tiering, Snapshots, Asynchronous Replications,&nbsp;ADAPT<br />\n<br />\nDrives: 84x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos<br />\n2x SFP+ 4-pack for 10Gb iSCSI<br />\n5x Hot-Swap Fan Cooling Modules<br />\n2x High-Efficiency 2.2kW Redundant Power Supplies<br />\nRail Kit<br />\n2x Bezels for each Drawer<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; FC or iSCSI (SFP+) (FC - 16Gb/s, 8Gb/s; iSCSI - 10Gb/s, 1Gb/s)<br />\n- 1 x4 mini-SAS HD connector for expansion (up to 10 2U (total 240 drives) or up to 4 5U enclousures (336 drives)<br />\n- 1 RJ45 port per IOM<br />\n- 4 Serial Ports<br />\n- 1 Mini-USB<br />\n- 16GB cache<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
        "orderNo": null,
        "criteriaId": 419,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Full Description Line",
        "measurement": null,
        "comlexName": null,
        "value": "84x3.5\" SAS Hot-swap|Supported drives SAS HDD|2xMini-USB|8xSFP+|2xRJ45|2xMiniSAS, ext|Rack 5U",
        "orderNo": "999",
        "criteriaId": 999,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "HDD configuration",
        "measurement": null,
        "comlexName": null,
        "value": "84x 16TB, SAS 7.2k",
        "orderNo": "2",
        "criteriaId": 2157,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "MiniSAS, ext",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "2",
        "orderNo": "6",
        "criteriaId": 1827,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Mini-USB",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "2",
        "orderNo": "6",
        "criteriaId": 1799,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Rack 5U",
        "measurement": null,
        "comlexName": "Mounting Type",
        "value": "Yes",
        "orderNo": "7",
        "criteriaId": 1225,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "RJ45",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "2",
        "orderNo": "6",
        "criteriaId": 1309,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "SFP+",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "8",
        "orderNo": "6",
        "criteriaId": 1832,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Supported drives",
        "measurement": null,
        "comlexName": null,
        "value": "SAS HDD",
        "orderNo": "4",
        "criteriaId": 1854,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Brutto Volume",
        "measurement": "cubm",
        "comlexName": null,
        "value": "0.1591",
        "orderNo": "13",
        "criteriaId": 1449,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Gross Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "145",
        "orderNo": "15",
        "criteriaId": 790,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Net Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "130",
        "orderNo": "14",
        "criteriaId": 791,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Vendor Homepage",
        "measurement": null,
        "comlexName": null,
        "value": "www.seagate.com/files/www-content/datasheets/pdfs/exos-x-5u84DS1982-5-2003US-en_US.pdf",
        "orderNo": null,
        "criteriaId": 426,
        "lastUpdateDate": "0001-01-01T00:00:00"
      }
    ]
  }
]
        ';

        /** @var DescriptionObject[]  $elkoDescriptions */
        $elkoDescriptions = Utils::jsonDecode($dataJson);

        $holders = ElkoDescriptionHandler::makeElkoDescriptionsHolders($elkoDescriptions);

        $this->assertEquals(2, count($holders));

        $holder1 = $holders[1301633];

        // assert all fields
        $this->assertEquals( 1301633, $holder1->getProductId());
        $this->assertEquals( "SEAGATE Exos E 4U106 JBOD system. Total available RAW capacity 1696TB with 106 of 106 (3.5&quot;) drive bays populated.&nbsp;<br />\n<br />\nDrives: 106x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos (ST16000NM002G)<br />\n4x System Fans<br />\n2x IOM Cooling FAN Modules<br />\n8x Hot-Swap Expanders<br />\n2x High-Efficiency 2kW Redundant Power Supplies<br />\nRail Kit and Cable Management Arm<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; Mini-SAS HD connectors (12Gb/s)<br />\n- 2 RJ45 port per IOM (for management)<br />\n- 1 Serial Ports<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
            $holder1->getDESCRIPTION()->value );

        $this->assertEquals(  "106x3.5\" SAS Hot-swap|Supported drives SAS HDD|4xRJ45|8xMiniSAS, ext|Rack 4U",
            $holder1->getFULL_DESCRIPTION_LINE()->value );

        $this->assertEquals(  "84x 16TB, SAS 7.2k",
            $holder1->getHDD_CONFIGURATION()->value );

        $this->assertEquals(  106,
            $holder1->getHOT_SWAP()->value );

        $this->assertEquals(  "8",
            $holder1->getMINISAS_EXT()->value );

        $this->assertEquals(  "Yes",
            $holder1->getRACK_4U()->value );

        $this->assertEquals(  "4",
            $holder1->getRJ45()->value );

        $this->assertEquals(  "SAS HDD",
            $holder1->getSUPPORTED_DRIVES()->value );

        $this->assertEquals(  "0.1591",
            $holder1->getUNIT_BRUTTO_VOLUME()->value );

        $this->assertEquals(  "140.6",
            $holder1->getUNIT_NET_WEIGHT()->value );

        $this->assertEquals(  "155",
            $holder1->getUNIT_GROS_WIGHT()->value );

        $this->assertEquals(  "www.seagate.com/files/www-content/datasheets/pdfs/exos-e-4u106-DS1980-3-1812US-en_US.pdf",
            $holder1->getVENDOR_HOME_PAGE()->value );


        $holder2 = $holders[1301630];


        $this->assertEquals( 1301630, $holder2->getProductId());
        $this->assertEquals( "SEAGATE Exos X 5U84 RAID system. Total available RAW capacity 1344TB with 84 of 84 (3.5&quot;) drive bays populated.&nbsp;<br />\n320k IOPS<br />\n7GB/s sequential read troughput<br />\n5.5G/b sequential write troughput<br />\nThin Provisioning,&nbsp;SSD Read Cashe,&nbsp;Real-time Auto Tiering, Snapshots, Asynchronous Replications,&nbsp;ADAPT<br />\n<br />\nDrives: 84x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos<br />\n2x SFP+ 4-pack for 10Gb iSCSI<br />\n5x Hot-Swap Fan Cooling Modules<br />\n2x High-Efficiency 2.2kW Redundant Power Supplies<br />\nRail Kit<br />\n2x Bezels for each Drawer<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; FC or iSCSI (SFP+) (FC - 16Gb/s, 8Gb/s; iSCSI - 10Gb/s, 1Gb/s)<br />\n- 1 x4 mini-SAS HD connector for expansion (up to 10 2U (total 240 drives) or up to 4 5U enclousures (336 drives)<br />\n- 1 RJ45 port per IOM<br />\n- 4 Serial Ports<br />\n- 1 Mini-USB<br />\n- 16GB cache<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
            $holder2->getDESCRIPTION()->value );

        $this->assertEquals(  "84x3.5\" SAS Hot-swap|Supported drives SAS HDD|2xMini-USB|8xSFP+|2xRJ45|2xMiniSAS, ext|Rack 5U",
            $holder2->getFULL_DESCRIPTION_LINE()->value );

        $this->assertEquals(  "84x 16TB, SAS 7.2k",
            $holder2->getHDD_CONFIGURATION()->value );

        $this->assertEquals(  84,
            $holder2->getHOT_SWAP()->value );

        $this->assertEquals(  "2",
            $holder2->getMINISAS_EXT()->value );

        $this->assertNull($holder2->getRACK_4U()->value );

        $this->assertEquals(  "2",
            $holder2->getRJ45()->value );

        $this->assertEquals(  "SAS HDD",
            $holder2->getSUPPORTED_DRIVES()->value );

        $this->assertEquals(  "0.1591",
            $holder2->getUNIT_BRUTTO_VOLUME()->value );

        $this->assertEquals(  "130",
            $holder2->getUNIT_NET_WEIGHT()->value );

        $this->assertEquals(  "145",
            $holder2->getUNIT_GROS_WIGHT()->value );

        $this->assertEquals(  "www.seagate.com/files/www-content/datasheets/pdfs/exos-x-5u84DS1982-5-2003US-en_US.pdf",
            $holder2->getVENDOR_HOME_PAGE()->value );


        //
        $storedCriterias = $holder1->getAllCriterias();
        $configuredCriterias = ElkoDescriptionsHolder::ALL_CRITERIAS;
        $extraCriterias = array_diff($storedCriterias, $configuredCriterias );
        $missingCriterias = array_diff($configuredCriterias, $storedCriterias );


        $storedCriterias2 = $holder2->getAllCriterias();
        $configuredCriterias = ElkoDescriptionsHolder::ALL_CRITERIAS;
        $extraCriterias2 = array_diff($storedCriterias2, $configuredCriterias );
        $missingCriterias2 = array_diff($configuredCriterias, $storedCriterias2 );


        echo "Extra criterias = ".join( ',' , $extraCriterias)."\n";
        echo "Missing criterias = ".join( ',' , $missingCriterias)."\n";

        echo "Extra criterias2 = ".join( ',' , $extraCriterias2)."\n";
        echo "Missing criterias2 = ".join( ',' , $missingCriterias2)."\n";
    }

}