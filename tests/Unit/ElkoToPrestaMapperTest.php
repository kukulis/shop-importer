<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.6
 * Time: 14.47
 */

namespace Tests\Unit;


use Kukulis\Presta\Helpers\ElkoToPrestaMapper;
use Kukulis\Providers\ElkoDescriptionHandler;
use PHPUnit\Framework\TestCase;

class ElkoToPrestaMapperTest extends TestCase
{
    public function testMap() {


        $descriptionsJson = '
        [
  {
    "productId": 1301633,
    "description": [
      {
        "criteria": "3.5\" SAS Hot-swap",
        "measurement": null,
        "comlexName": "Drive Bays",
        "value": "106",
        "orderNo": "3",
        "criteriaId": 2034,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Description",
        "measurement": null,
        "comlexName": null,
        "value": "SEAGATE Exos E 4U106 JBOD system. Total available RAW capacity 1696TB with 106 of 106 (3.5&quot;) drive bays populated.&nbsp;<br />\n<br />\nDrives: 106x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos (ST16000NM002G)<br />\n4x System Fans<br />\n2x IOM Cooling FAN Modules<br />\n8x Hot-Swap Expanders<br />\n2x High-Efficiency 2kW Redundant Power Supplies<br />\nRail Kit and Cable Management Arm<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; Mini-SAS HD connectors (12Gb/s)<br />\n- 2 RJ45 port per IOM (for management)<br />\n- 1 Serial Ports<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
        "orderNo": null,
        "criteriaId": 419,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Full Description Line",
        "measurement": null,
        "comlexName": null,
        "value": "106x3.5\" SAS Hot-swap|Supported drives SAS HDD|4xRJ45|8xMiniSAS, ext|Rack 4U",
        "orderNo": "999",
        "criteriaId": 999,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "HDD configuration",
        "measurement": null,
        "comlexName": null,
        "value": "84x 16TB, SAS 7.2k",
        "orderNo": "2",
        "criteriaId": 2157,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "MiniSAS, ext",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "8",
        "orderNo": "6",
        "criteriaId": 1827,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Rack 4U",
        "measurement": null,
        "comlexName": "Mounting Type",
        "value": "Yes",
        "orderNo": "7",
        "criteriaId": 1224,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "RJ45",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "4",
        "orderNo": "6",
        "criteriaId": 1309,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Supported drives",
        "measurement": null,
        "comlexName": null,
        "value": "SAS HDD",
        "orderNo": "4",
        "criteriaId": 1854,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Brutto Volume",
        "measurement": "cubm",
        "comlexName": null,
        "value": "0.1591",
        "orderNo": "13",
        "criteriaId": 1449,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Gross Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "155",
        "orderNo": "15",
        "criteriaId": 790,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Net Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "140.6",
        "orderNo": "14",
        "criteriaId": 791,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Vendor Homepage",
        "measurement": null,
        "comlexName": null,
        "value": "www.seagate.com/files/www-content/datasheets/pdfs/exos-e-4u106-DS1980-3-1812US-en_US.pdf",
        "orderNo": null,
        "criteriaId": 426,
        "lastUpdateDate": "0001-01-01T00:00:00"
      }
    ]
  },
  {
    "productId": 1301630,
    "description": [
      {
        "criteria": "3.5\" SAS Hot-swap",
        "measurement": null,
        "comlexName": "Drive Bays",
        "value": "84",
        "orderNo": "3",
        "criteriaId": 2034,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Description",
        "measurement": null,
        "comlexName": null,
        "value": "SEAGATE Exos X 5U84 RAID system. Total available RAW capacity 1344TB with 84 of 84 (3.5&quot;) drive bays populated.&nbsp;<br />\n320k IOPS<br />\n7GB/s sequential read troughput<br />\n5.5G/b sequential write troughput<br />\nThin Provisioning,&nbsp;SSD Read Cashe,&nbsp;Real-time Auto Tiering, Snapshots, Asynchronous Replications,&nbsp;ADAPT<br />\n<br />\nDrives: 84x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos<br />\n2x SFP+ 4-pack for 10Gb iSCSI<br />\n5x Hot-Swap Fan Cooling Modules<br />\n2x High-Efficiency 2.2kW Redundant Power Supplies<br />\nRail Kit<br />\n2x Bezels for each Drawer<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; FC or iSCSI (SFP+) (FC - 16Gb/s, 8Gb/s; iSCSI - 10Gb/s, 1Gb/s)<br />\n- 1 x4 mini-SAS HD connector for expansion (up to 10 2U (total 240 drives) or up to 4 5U enclousures (336 drives)<br />\n- 1 RJ45 port per IOM<br />\n- 4 Serial Ports<br />\n- 1 Mini-USB<br />\n- 16GB cache<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
        "orderNo": null,
        "criteriaId": 419,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Full Description Line",
        "measurement": null,
        "comlexName": null,
        "value": "84x3.5\" SAS Hot-swap|Supported drives SAS HDD|2xMini-USB|8xSFP+|2xRJ45|2xMiniSAS, ext|Rack 5U",
        "orderNo": "999",
        "criteriaId": 999,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "HDD configuration",
        "measurement": null,
        "comlexName": null,
        "value": "84x 16TB, SAS 7.2k",
        "orderNo": "2",
        "criteriaId": 2157,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "MiniSAS, ext",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "2",
        "orderNo": "6",
        "criteriaId": 1827,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Mini-USB",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "2",
        "orderNo": "6",
        "criteriaId": 1799,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Rack 5U",
        "measurement": null,
        "comlexName": "Mounting Type",
        "value": "Yes",
        "orderNo": "7",
        "criteriaId": 1225,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "RJ45",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "2",
        "orderNo": "6",
        "criteriaId": 1309,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "SFP+",
        "measurement": null,
        "comlexName": "Input/Output connectors",
        "value": "8",
        "orderNo": "6",
        "criteriaId": 1832,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Supported drives",
        "measurement": null,
        "comlexName": null,
        "value": "SAS HDD",
        "orderNo": "4",
        "criteriaId": 1854,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Brutto Volume",
        "measurement": "cubm",
        "comlexName": null,
        "value": "0.1591",
        "orderNo": "13",
        "criteriaId": 1449,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Gross Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "145",
        "orderNo": "15",
        "criteriaId": 790,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Unit Net Weight",
        "measurement": "kg",
        "comlexName": null,
        "value": "130",
        "orderNo": "14",
        "criteriaId": 791,
        "lastUpdateDate": "0001-01-01T00:00:00"
      },
      {
        "criteria": "Vendor Homepage",
        "measurement": null,
        "comlexName": null,
        "value": "www.seagate.com/files/www-content/datasheets/pdfs/exos-x-5u84DS1982-5-2003US-en_US.pdf",
        "orderNo": null,
        "criteriaId": 426,
        "lastUpdateDate": "0001-01-01T00:00:00"
      }
    ]
  }
]
        ';

        $productsJson = '[
         {
    "id": 1301630,
    "elkoCode": 1301630,
    "name": "RBOD 5U84 1344TB 7.2K SAS HDD/DUAL.C. 84X16TB EXOS X SEAGATE",
    "manufacturerCode": "EXOS-X-5U84_84X16T_3Y",
    "vendorName": "SEAGATE",
    "vendorCode": "SE",
    "catalog": "JBO",
    "catalogName": "JBOD",
    "quantity": "0",
    "price": 60672.51,
    "discountPrice": 60672.51,
    "imagePath": "https://static.elkogroup.com/ProductImages/73b9a296-73af-4e1c-8814-8fb452555acb.Jpeg",
    "thumbnailImagePath": "https://static.elkogroup.com/ProductImagesThumbs/73b9a296-73af-4e1c-8814-8fb452555acb.Jpeg",
    "fullDsc": "84x3.5\" SAS Hot-swap | Supported drives SAS HDD | 2xMini-USB | 8xSFP+ | 2xRJ45 | 2xMiniSAS, ext | Rack 5U",
    "currency": "EUR",
    "httpDescription": "https://ecom.elkogroup.com/Catalog/Product/1301630",
    "packagingQuantity": 1,
    "warranty": "36",
    "eanCode": "",
    "obligatoryKit": 0,
    "reservedQuantity": 0,
    "promDate": 0,
    "promQuant": 0,
    "quantityForPrice2": "0",
    "price2": 0.0,
    "lotNumber": "",
    "copyrightTax": 0.0,
    "incomingQuantity": 0
  },
  {
    "id": 1301633,
    "elkoCode": 1301633,
    "name": "JBOD 4U106 1696TB 7.2K SAS HDD/DUAL.C.106X16TB EXOS E SEAGATE",
    "manufacturerCode": "EXOS-E-4U106_106X16T_3Y",
    "vendorName": "SEAGATE",
    "vendorCode": "SE",
    "catalog": "JBO",
    "catalogName": "JBOD",
    "quantity": "0",
    "price": 63539.53,
    "discountPrice": 63539.53,
    "imagePath": "https://static.elkogroup.com/ProductImages/9f15d6f6-38ff-4124-b6fe-2dee185abc36.Jpeg",
    "thumbnailImagePath": "https://static.elkogroup.com/ProductImagesThumbs/9f15d6f6-38ff-4124-b6fe-2dee185abc36.Jpeg",
    "fullDsc": "106x3.5\" SAS Hot-swap | Supported drives SAS HDD | 4xRJ45 | 8xMiniSAS, ext | Rack 4U",
    "currency": "EUR",
    "httpDescription": "https://ecom.elkogroup.com/Catalog/Product/1301633",
    "packagingQuantity": 1,
    "warranty": "36",
    "eanCode": "",
    "obligatoryKit": 0,
    "reservedQuantity": 0,
    "promDate": 0,
    "promQuant": 0,
    "quantityForPrice2": "0",
    "price2": 0.0,
    "lotNumber": "",
    "copyrightTax": 0.0,
    "incomingQuantity": 0
  }]';

        $descriptions = json_decode($descriptionsJson);
        $products = json_decode($productsJson);


        $elkoCatalogProduct = $products[0];
        $defaultDate = '2021-01-01';
        $isoCode = 'en';
        $descriptionsHolders = ElkoDescriptionHandler::makeElkoDescriptionsHolders($descriptions);


        $this->assertTrue ( array_key_exists($elkoCatalogProduct->id, $descriptionsHolders ));

        echo "id={$elkoCatalogProduct->id}\n";
        $descriptionHolder = $descriptionsHolders[$elkoCatalogProduct->id];

        $product = ElkoToPrestaMapper::mapProduct($elkoCatalogProduct, $defaultDate, $isoCode, $descriptionHolder );
        $this->assertNotNull($product);

        $this->assertEquals('E_1301630', $product->nomnr);
        $this->assertEquals(0, $product->id_product);
        $this->assertEquals(0, $product->id_supplier);
        $this->assertEquals(0, $product->id_manufacturer);
        $this->assertEquals(1, $product->id_category_default);
        $this->assertEquals(1, $product->id_shop_default);
        $this->assertEquals(1, $product->id_tax_rules_group);
        $this->assertEquals(0, $product->on_sale);
        $this->assertEquals(0, $product->online_only);
        $this->assertEquals('', $product->isbn);
        $this->assertEquals('', $product->upc);
        $this->assertEquals(0, $product->ecotax);
        $this->assertEquals(1, $product->quantity);
        $this->assertEquals(1, $product->minimal_quantity);
        $this->assertEquals(1, $product->low_stock_threshold);
        $this->assertEquals(0, $product->low_stock_alert);
        $this->assertEquals(99999, $product->price);
        $this->assertEquals(99999, $product->wholesale_price);
        $this->assertEquals('', $product->unity);
        $this->assertEquals(0, $product->unit_price_ratio);
        $this->assertEquals(0, $product->additional_shipping_cost);
        $this->assertEquals('E_1301630', $product->reference);
        $this->assertEquals('', $product->supplier_reference);
        $this->assertEquals('', $product->location);
        $this->assertEquals(0, $product->width);
        $this->assertEquals(0, $product->height);
        $this->assertEquals(0, $product->depth);
        $this->assertEquals(0, $product->weight); // todo map
        $this->assertEquals(2, $product->out_of_stock);
        $this->assertEquals(1, $product->additional_delivery_times);
        $this->assertEquals(0, $product->quantity_discount);
        $this->assertEquals(0, $product->customizable);
        $this->assertEquals(0, $product->uploadable_files);
        $this->assertEquals(0, $product->text_fields);
        $this->assertEquals(1, $product->active);
        $this->assertEquals('301-product', $product->redirect_type);
        $this->assertEquals(0, $product->id_type_redirected);
        $this->assertEquals(1, $product->available_for_order);
        $this->assertEquals('2020-01-01', $product->available_date);
        $this->assertEquals(0, $product->show_condition);
        $this->assertEquals('new', $product->condition);
        $this->assertEquals(1, $product->show_price);
        $this->assertEquals(0, $product->indexed);
        $this->assertEquals('both', $product->visibility);
        $this->assertEquals(0, $product->cache_is_pack);
        $this->assertEquals(0, $product->cache_has_attachments);
        $this->assertEquals(0, $product->is_virtual);
        $this->assertEquals('', $product->cache_default_attribute);
        $this->assertEquals('2021-01-01', $product->date_add);
        $this->assertEquals('2021-01-01', $product->date_upd);
        $this->assertEquals(0, $product->advanced_stock_management);
        $this->assertEquals(3, $product->pack_stock_type);
        $this->assertEquals(1, $product->state);
        $this->assertEquals(1, $product->id_shop);
        $this->assertEquals('', $product->id_lang);
        $this->assertEquals("SEAGATE Exos X 5U84 RAID system. Total available RAW capacity 1344TB with 84 of 84 (3.5&quot;) drive bays populated.&nbsp;<br />\n320k IOPS<br />\n7GB/s sequential read troughput<br />\n5.5G/b sequential write troughput<br />\nThin Provisioning,&nbsp;SSD Read Cashe,&nbsp;Real-time Auto Tiering, Snapshots, Asynchronous Replications,&nbsp;ADAPT<br />\n<br />\nDrives: 84x 16TB&nbsp;3.5&quot; 7.2K SAS&nbsp;12G&nbsp;HDD Exos<br />\n2x SFP+ 4-pack for 10Gb iSCSI<br />\n5x Hot-Swap Fan Cooling Modules<br />\n2x High-Efficiency 2.2kW Redundant Power Supplies<br />\nRail Kit<br />\n2x Bezels for each Drawer<br />\n2x IOM controllers (working in active/active mode), each have:<br />\n- 4&nbsp; FC or iSCSI (SFP+) (FC - 16Gb/s, 8Gb/s; iSCSI - 10Gb/s, 1Gb/s)<br />\n- 1 x4 mini-SAS HD connector for expansion (up to 10 2U (total 240 drives) or up to 4 5U enclousures (336 drives)<br />\n- 1 RJ45 port per IOM<br />\n- 4 Serial Ports<br />\n- 1 Mini-USB<br />\n- 16GB cache<br />\n<br />\nWarranty - On Site, 8x5xNBD, 3 Year<br />\n<br />\n<em>* The price is for reference only. The actual price is available after deal registration and product quotation *</em>",
            $product->description);
        $this->assertEquals("84x3.5\" SAS Hot-swap|Supported drives SAS HDD|2xMini-USB|8xSFP+|2xRJ45|2xMiniSAS, ext|Rack 5U", $product->description_short);
        $this->assertEquals('rbod-5u84-1344tb-72k-sas-hdd-dualc-84x16tb-exos-x-seagate', $product->link_rewrite);
        $this->assertEquals('', $product->meta_description);
        $this->assertEquals('', $product->meta_keywords);
        $this->assertEquals('', $product->meta_title);
        $this->assertEquals("RBOD 5U84 1344TB 7.2K SAS HDD/DUAL.C. 84X16TB EXOS X SEAGATE", $product->name);
        $this->assertEquals('', $product->available_now);
        $this->assertEquals('', $product->available_later);
        $this->assertEquals('', $product->delivery_in_stock);
        $this->assertEquals('', $product->delivery_out_stock);
        $this->assertEquals('', $product->brand);
        $this->assertEquals('', $product->tipas);
        $this->assertEquals('', $product->paskirtis);
        $this->assertEquals('', $product->dydis);
        $this->assertEquals('', $product->kiekis);
        $this->assertEquals('en', $product->iso_code);
        $this->assertEquals('', $product->parent);
        $this->assertEquals('', $product->spalva);
        $this->assertEquals('', $product->var_name);
        $this->assertEquals('', $product->ean13);
        $this->assertEquals('SE', $product->supplier);
    }

}