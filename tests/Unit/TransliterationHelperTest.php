<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.17
 * Time: 09.43
 */

namespace Tests\Unit;


use Kukulis\Helpers\TransliterationHelper;
use PHPUnit\Framework\TestCase;

class TransliterationHelperTest extends TestCase
{
    public function testNonAlphaNumericToConnector()
    {
        $this->assertEquals('asdf_asdf', TransliterationHelper::nonAlphaNumericToConnectors('asdf(#*$&(#asdf'));
    }

    public function testToCode()
    {
        $this->assertEquals('zukles-priemones', TransliterationHelper::toCode('Žūklės   Priemonės'));
    }

}