<?php

namespace Tests\Unit;


use Kukulis\Elko\TitleTransformer;
use PHPUnit\Framework\TestCase;

class TestTitleTransformer extends TestCase
{
    public function testTutorial()
    {
        $this->assertEquals(1, preg_match('/^\w+\s+\w+\s+\w+\s+\w+$/', 'vienas du trys keturi'));
        $this->assertEquals(0, preg_match('/^\w+\s+\w+\s+\w+\s+\w+$/', 'vienas du trys'));

        $this->assertEquals(1, preg_match('/^\w+\s+\w+\s+\w+\s+\w+$/', 'vienas du trys keturi', $matches));
        $this->assertCount(1, $matches);
        $this->assertEquals(1, preg_match('/^(\w+)\s+\w+\s+\w+\s+\w+$/', 'vienas du trys keturi', $matches));
        $this->assertCount(2, $matches);
        $this->assertEquals( 'vienas', $matches[1]);

        $this->assertEquals(1, preg_match('/^(\w+)\s+(\w+)\s+\w+\s+\w+$/', 'vienas du trys keturi', $matches));
        $this->assertCount(3, $matches);
        $this->assertEquals( 'vienas', $matches[1]);
        $this->assertEquals( 'du', $matches[2]);


        $this->assertEquals(1, preg_match('/^(\w+)\s+(\w+)\s+(\w+)\s+(\w+)$/', 'vienas du trys keturi', $matches));
        $this->assertCount(5, $matches);
        $this->assertEquals( 'vienas', $matches[1]);
        $this->assertEquals( 'du', $matches[2]);
        $this->assertEquals( 'trys', $matches[3]);
        $this->assertEquals( 'keturi', $matches[4]);

    }

    public function testTransform()
    {


        //PRINTER ACC PRINTHEAD CYAN/NO.11 C4811A HP
//PRINTER ACC PRINTHEAD CYAN/NO.11 C4811A HP
//PRINTER ACC PRINTHEAD MAGENTA/NO.11 C4812A HP
//PRINTER ACC PRINTHEAD MAGENTA/NO.11 C4812A HP
//PRINTER ACC PRINTHEAD YELLOW/NO.11 C4813A HP
//PRINTER ACC PRINTHEAD YELLOW/NO.11 C4813A HP
//INK CARTRIDGE CYAN NO 82/C4911A HP
//INK CARTRIDGE CYAN NO 82/C4911A HP
//INK CARTRIDGE MAGENTA NO 82/C4912A HP
//INK CARTRIDGE MAGENTA NO 82/C4912A HP
//INK CARTRIDGE YELLOW NO 82/C4913A HP
//INK CARTRIDGE YELLOW NO 82/C4913A HP
//PRINTER ACC PRINTHEAD BLACK/NO.11 C4810A HP
//PRINTER ACC PRINTHEAD BLACK/NO.11 C4810A HP
//INK CARTRIDGE BLACK NO.10//DESIGN JET LARGE C4844AE HP
//INK CARTRIDGE BLACK NO.10//DESIGN JET LARGE C4844AE HP
//TONER BLACK 27X /LJ4000/4050/10K C4127X HP
        $titleTransformer = new TitleTransformer();
        $this->assertEquals('ACC PRINTHEAD CYAN/NO.11 C4811A PRINTER',
            $titleTransformer->transform('PRINTER ACC PRINTHEAD CYAN/NO.11 C4811A HP'));
        $this->assertEquals('CARTRIDGE CYAN NO 82/C4911A INK',
            $titleTransformer->transform('INK CARTRIDGE CYAN NO 82/C4911A HP'));
        $this->assertEquals('BLACK 27X /LJ4000/4050/10K C4127X TONER',
            $titleTransformer->transform('TONER BLACK 27X /LJ4000/4050/10K C4127X HP'));


        // jeigu čia yra ne tai ko nori, daryti reikia taip:
        // 1) darai assertą, kur surašai ko tu iš tikrųjų tikiesi:
        $this->assertEquals('/LJ4000/4050/10K C4127X',
            $titleTransformer->transform('TONER BLACK 27X /LJ4000/4050/10K C4127X HP'));

        //2) šitas testas duoda klaidą, tada koreguoji asertą, tol kol testas nebeduos klaidos.

    }

}