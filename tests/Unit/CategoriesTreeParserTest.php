<?php
/**
 * Created by PhpStorm.
 * User: giedrius
 * Date: 21.7.17
 * Time: 10.17
 */

namespace Tests\Unit;


use Kukulis\Data\Category;
use Kukulis\Elko\Data\CategoryTreeNode;
use Kukulis\Presta\Helpers\CategoriesTreeParser;
use PHPUnit\Framework\TestCase;

class CategoriesTreeParserTest extends TestCase
{
    public function testTreeToFlat() {

        $child1 = new CategoryTreeNode();
        $child1->name ='Vaikas1';

        $child2 = new CategoryTreeNode();
        $child2->name ='Vaikas2';

        $child11 = new CategoryTreeNode();
        $child11->name ='Vaikas11';

        $child12 = new CategoryTreeNode();
        $child12->name ='Vaikas12';


        $child21 = new CategoryTreeNode();
        $child21->name ='Vaikas21';

        $child22 = new CategoryTreeNode();
        $child22->name ='Vaikas22';

        $child1->childs[] = $child11;
        $child1->childs[] = $child12;

        $child2->childs[] = $child21;
        $child2->childs[] = $child22;


        $expected = [
            'vaikas2' => Category::create('vaikas2', 'Vaikas2', 'root' ),
            'vaikas1' => Category::create('vaikas1', 'Vaikas1', 'root' ),
            'vaikas11' => Category::create('vaikas11', 'Vaikas11', 'vaikas1' ),
            'vaikas12' => Category::create('vaikas12', 'Vaikas12', 'vaikas1' ),
            'vaikas21' => Category::create('vaikas21', 'Vaikas21', 'vaikas2' ),
            'vaikas22' => Category::create('vaikas22', 'Vaikas22', 'vaikas2' ),
        ];

        $this->assertEquals($expected, CategoriesTreeParser::treeToFlatMap([$child1, $child2], 'root'));
    }
}