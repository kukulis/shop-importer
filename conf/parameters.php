<?php
// global parameters config

return [
    'aaa' => 'bbb',

    'localesMap' => [
        'lit-lt' => 'lt',
        'lt-lt' => 'lt',
        'en-us' => 'en',
        'eng-gb' => 'en',
        'en-gb' => 'en'
    ],
];